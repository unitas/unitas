#!/usr/bin/env bash

runtests="false"
gendocs="false"

function show_opts {
	echo "=== A script to clear the Unitas repository. ==="
	echo "This script clears log files, old migration folders, drops and "
	echo "recreates the database. Runs bootstrap scripts to create system "
	echo "folders and users. It also adds data like mimetypes to the system."
	echo 
	echo "clear-repo.sh [-t|--test] [-h|--help]"
	echo "    -t or --test : run unit tests"
	echo "    -h or --help : displays this help message"
	echo "    -d or --gendocs: generates updated documentation"
	echo
	exit 0
}

while [ $# -gt 0 ]
do
    case "$1" in
        -t|--test)  runtests="true";;
		-h|--help) show_opts;;
		-d|--gendocs) gendocs="true";;
    esac
    shift
done

# Set database, user
db=unitas
dbuser=admin
unitas_admin_email="admin@localhost"

# Clears and resets bash shell
clear; reset;

# Clear content files
echo "Clearing content and temp files ..."
rm -rf content
rm -rf tmp/*

# Clear logs
echo "Clearing logs ..."
rm -rf logs/*

# Delete all past migrations
echo "Clearing migrations ..."
rm -rf apps/repository/migrations/*
rm -rf apps/services/migrations/*
rm -rf apps/api/migrations/*
rm -rf apps/web/migrations/*

# Restarting database
echo "Restarting database ..."
sudo service postgresql restart

# Recreate database
echo "Dropping database $db ..."
sudo -iu postgres dropdb $db

echo "Creating new database $db ..."
sudo -iu postgres createdb -O $dbuser $db

# Perform migrations to build database based on unitas models
# Create general migrations
echo "Making migrations ..."
scripts/makemigrations.sh

# Create migrations for repository app
echo "Making migrations for repository ..."
scripts/makemigrations.sh repository

# Create migrations for services app
echo "Making migrations for services ..."
scripts/makemigrations.sh services

# Create migrations for api app
echo "Making migrations for api ..."
scripts/makemigrations.sh api

# Create migrations for api app
echo "Making migrations for web ..."
scripts/makemigrations.sh web

# Perform migrations
echo "Performing migrations ..."
scripts/migrate.sh

# Add system settings
echo "Creating system settings ..."
scripts/setrepoid.py

# Create the Unitas and Django admin user
echo "Creating superuser ..."
./manage.py createsuperuser --noinput --username "admin" --email "$unitas_admin_email"

echo "Setting superuser password ..."
scripts/setadminpw.py

# Create additional bootstrap users
echo "Creating other users ..."
scripts/data/create_users.py

# Add bootstrap data
# Create model types like document, folder
echo "Creating Model Types ..."
scripts/data/create_model_types.py

# Create necessary system folders for Unitas
echo "Creating Folders ..."
scripts/data/create_system_folders.py

# Create necessary mimetypes for Unitas
echo "Creating MimeTypes ..."
scripts/data/create_mimetypes.py

if [ "$runtests" == "true" ]
then
	# Run unit tests
	echo "Running tests ..."
	./runtests.sh
fi

if [ "$gendocs" == "true" ]
then
	# Run gendocs.sh
	echo "Generating documentation ..."
	./gendocs.sh
fi

echo 
echo "Done."
echo