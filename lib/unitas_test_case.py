"""UnitasTestCase class.
"""

import shutil
from django.test import TestCase

from lib.run_bootstrap_scripts import BootstrapScriptsRunner, BootstrapScriptsRunnerNoMimeTypes
from apps.repository.lib.service.user import UserService
import logging

logger = logging.getLogger(__name__)


class BaseUnitasTestCase(TestCase):
	"""BaseUnitasTestCase class. Contains some very common methods for tests.
	"""
	
	def get_non_priv_user(self, username='nonprivuser'):
		"""Return a non privileged user.
		"""
		non_priv_user = UserService().create(
			username,
			'secret',
			'{}@localhost'.format(username),
		)
		return non_priv_user

	def get_super_user(self, username='superuser'):
		"""Return a super user.
		"""
		user = self.get_non_priv_user(username=username)
		user.is_superuser = True
		user.save()
		return user

class UnitasTestCase(BaseUnitasTestCase):
	"""UnitasTestCase class. Runs the BootstrapScriptsTest with mimetype generation.
	"""
	@classmethod
	def setUpTestData(BootstrapScriptsTest):
		bootstrap_scripts_runner = BootstrapScriptsRunner()
		bootstrap_scripts_runner.run()

	def tearDown(self):
		try:
			shutil.rmtree('content')
		except OSError:
			pass

class UnitasTestCaseNoMimeTypes(BaseUnitasTestCase):
	"""UnitasTestCaseNoMimeTypes class. Runs the BootstrapScriptsTest without mimetype generation. Was created to save time on test running.
	"""
	@classmethod
	def setUpTestData(BootstrapScriptsTest):
		bootstrap_scripts_runner = BootstrapScriptsRunnerNoMimeTypes()
		bootstrap_scripts_runner.run()

	def tearDown(self):
		try:
			shutil.rmtree('content')
		except OSError:
			pass

			