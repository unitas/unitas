"""Bootstrap Scripts runner class.
"""

from django.contrib.auth.models import User
from scripts.data.create_users import UserCreater, system_users
from scripts.data.create_system_folders import SystemFolderCreater
from scripts.data.create_model_types import ModelTypeCreater
from scripts.data.create_mimetypes import MimeTypeCreater
import logging

logger = logging.getLogger(__name__)


class BootstrapScriptsRunner(object):
	"""BootstrapScript Runner with Mimetype generation.
	"""
	def run(self):

		admin_user = User.objects.create(
			username='admin',
			email='admin@localhost',
			is_superuser=True,
			is_staff=True,
		)
		admin_user.set_password('admin')
		admin_user.save()

		mime_type_creater = MimeTypeCreater()
		mime_type_creater.run()

		user_creater = UserCreater()
		user_creater.run()

		model_type_creater = ModelTypeCreater()
		model_type_creater.run()

		system_folder_creater = SystemFolderCreater()
		system_folder_creater.run()

class BootstrapScriptsRunnerNoMimeTypes(object):
	"""BootstrapScript Runner without Mimetype generation.
	"""
	def run(self):

		admin_user = User.objects.create(
			username='admin',
			email='admin@localhost',
			is_superuser=True,
			is_staff=True,
		)
		admin_user.set_password('admin')
		admin_user.save()

		user_creater = UserCreater()
		user_creater.run()

		model_type_creater = ModelTypeCreater()
		model_type_creater.run()

		system_folder_creater = SystemFolderCreater()
		system_folder_creater.run()