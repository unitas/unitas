#!/usr/bin/env bash

export DJANGO_SETTINGS_MODULE='unitas.settings'
epydoc --html --parse-only --name Unitas --inheritance grouped apps lib unitas web -o docs/
cp web/static/admin/css/base.css docs/epydoc.css