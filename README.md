# Unitas

## What is it?

Open source document management software written in Python making use of Django platform.

## How is it licensed?

GNU General Public License v3.0

## What is document management software?

Have a look here:

This seems to very accurately describe it: https://en.wikipedia.org/wiki/Document_management_system

## What are some other examples of document management software:

* Alfresco
* Documentum
* IBM FileNet
* Microsoft Sharepoint
* OpenText's Vignette or eDOCS

and really many, many others. There's a lot of this software type out there but hardly any of it is open source.

## What sets this apart from the others? What are the goals of this project?

* The project will be open source. There should not ever be a fee to use it.
* The UI should be very simple to use.
* Documentation should contain reference and example instructions with each concept.
* The software should be supported on as many different platforms as possible.
* The software should be very simple to customize and extend.

## Supported Platforms goals:

* QA for Python 2.7, 3.x
* QA for Ubuntu (14.04), CentOS/RHEL (6 & 7), Windows 2008 R2 & 2012
* QA for Postgresql 9.3, MySQL 5.5.x

Should work with most any Linux distro that can install and run Python 2.7.x and/or 3.x. Should work with any Postgresql 9.x or MySQL 5.x.

## Current detailed goals

* The project will release a weekly message (see 'What phase of development is this in at the moment?') about progress.

* There are some outstanding issue cases. You can see them by clicking on Issues to the left of clicking directly: https://bitbucket.org/unitas/unitas/issues?status=new&status=open If you are interested in tackling these issues or volunteering to help this project in general, please see "Interested in Volunteering" section below.

## What phase of development is this in at the moment?

As of 27 Jan, 2016, Unitas' repository core as a concept is still in progress meaning that code is being written and that code is being unit tested. Design is also still being worked out for the repository models. Also, at the moment, a web-client ui is in the design phase.

Just over the last few days (As of 27 Jan, 2016) we have succeeded in putting in an API application complete with authorization (ACL) and auditing features. The next layer for this will be a REST API. Extending a Django project is a lot simpler than extending a Java Spring project but, the REST API will make it even easier for users in the future who want to write their own custom modules against Unitas. One could even then write their own web frontend with very little difficulty.

03 Feb, 2016 - We have decided to not use Solr as a supported search index engine for Unitas at this time. Solr is pretty heavyweight and will keep us from our goal of putting out an MVP of Unitas in the very near future. We definitely will consider it as we have time in the future. We've decided to go with full text search with Postgresql. Postgresql has its own license but it's very similar to BSD or MIT. There won't be any restrictions on distribution if needed. It's easy to use. Django has support for it already built in. As far as performance testing goes, we want to make sure Unitas MVP will be able to handle a million documents with relative ease. We are certain Postgresql can help us with that.

The API for the web client is now preliminarily complete. We still expect to add a few pieces to this before the MVP launch but most of the work for it is done. We are now moving into the View (of MVC) phase.

16 Feb, 2016 - Still in the middle of creating the web client. Some expectations of use: create folders and documents, edit folder and document properties, share documents, create projects, transform documents on demand, create and modify users, delete and recycle documents and folders, etc.

21 Feb, 2016 - Just finished with a very basic transformation and preview action. Most office documents can/will be transformed to PDF for previews. Image files and PDFs will be previewable out of the box as well. For now, this requires the user having LibreOffice installed.

19 Mar 2016 - Still fleshing out the gui for the web client. Have had to work on other things (including my job) lately.

## Interested in volunteering?

If you are interested in helping with the project, please contact Jacob Ritenour at unitasrepo@gmail.com. At this point, nothing we are doing is extremely complex. While we are looking for volunteer contributors, this will be a very good open source project to point to when sending CV's for jobs in the technology field. Since this is more of a business/enterprise related type of software, volunteering here will look good on your behalf ;-) We are primarily interested in the following skills/competencies:

* Some understanding of content/document management -- at the very least have used one of the example software listed above or at the least have an interest in knowing more. Even if you're not a coder, we can always use testers.
* Python/Django
* Java
* Postgresql
* Documentation writing
* Testing especially if you are familiar with various Linux distros