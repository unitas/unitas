"""Web app views.
"""

from __future__ import absolute_import

from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from unitas.settings import LOGIN_URL
from apps.repository.lib.service.folder import FolderService
from apps.web.lib.view.folder_views import *
from apps.web.lib.view.document_views import *
from apps.web.lib.view.version_views import *
from apps.web.lib.view.project_views import *

import logging

logger = logging.getLogger(__name__)

@login_required(login_url=LOGIN_URL)
def index(request):
	"""Redirect user to user's own home folder.
	"""
	
	folder_service = FolderService()
	folder_service.set_user(request.user)
	user_home_folder = folder_service.get_user_home_folder()
	user_home_folder_uuid = user_home_folder.uuid

	return HttpResponseRedirect(
		reverse(
			'folder_view',
			kwargs={
				'folder_id': user_home_folder_uuid,
			}
		)
	)


@login_required(login_url=LOGIN_URL)
def redirect_to_user_home_folder(request):
	"""Redirect user to user's own home folder.
	"""

	folder_service = FolderService()
	folder_service.set_user(request.user)
	user_home_folder = folder_service.get_user_home_folder()
	user_home_folder_uuid = user_home_folder.uuid

	return HttpResponseRedirect(
		reverse(
			'folder_view',
			kwargs={
				'folder_id': user_home_folder_uuid,
			}
		)
	)