"""Project model related configs.
"""

project_types = (
	'Open',
	'Public',
	'Private',
)

project_update_excluded_fields = (
	'owner',
	'key',
	'created',
)