""" Preview configuration settings.
"""

from __future__ import absolute_import

from apps.services.modules.transformation.config.transformation import allowable_xform_2pdf

previewable_documents = [
	'.jpg', '.jpeg', '.gif', '.png', '.tiff', '.svg', '.xbm', '.bmp', '.ico', 
	'.pdf',
]

exportable_as_pdf = allowable_xform_2pdf

previewable_documents.extend(exportable_as_pdf)