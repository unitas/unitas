
from __future__ import absolute_import

from django.forms import ModelForm
from django import forms
from django.contrib.auth.models import User
from apps.repository.models import Folder, Document, DocumentVersion
from apps.web.models import Project
from apps.web.config.project import project_types

class CreateFolderForm(ModelForm):

	def __init__(self, *args, **kwargs):
		self.parent_folder_id=kwargs.pop('parent_folder_id')
		super(CreateFolderForm, self).__init__(*args,**kwargs)

	def clean_name(self):
		
		folder_name = self.cleaned_data['name']

		try:
			parent_folder = Folder.objects.get(pk=self.parent_folder_id)
			folder_exists = Folder.objects.get(
				name=folder_name,
				parent_folder=parent_folder)
		except Folder.DoesNotExist:
			folder_exists = None

		if folder_exists:
			raise forms.ValidationError(
				"Folder with name '{}' already exists in this space.".format(
					folder_name
				)
			)
		return folder_name

	class Meta:
		model = Folder
		fields = [
			'name', 'title', 'description',
		]

class EditFolderForm(CreateFolderForm):
	pass

class CreateDocumentForm(ModelForm):

	def __init__(self, *args, **kwargs):
		self.parent_folder_id=kwargs.pop('parent_folder_id')
		super(CreateDocumentForm, self).__init__(*args,**kwargs)

	def clean_name(self):
		
		document_name = self.cleaned_data['name']

		try:
			parent_folder = Folder.objects.get(pk=self.parent_folder_id)
			document_exists = Document.objects.get(
				name=document_name,
				parent_folder=parent_folder)
		except Document.DoesNotExist:
			document_exists = None

		if document_exists:
			raise forms.ValidationError(
				"Document with name '{}' already exists in this space.".format(
					document_name
				)
			)
		return document_name

	name = forms.CharField(
		label='Name', 
		max_length=255
	)

	title = forms.CharField(
		label='Title', 
		max_length=255, 
		required=False
	)

	description = forms.CharField(
		label='Description', 
		max_length=255, 
		required=False,
		widget=forms.Textarea(
			attrs={
				'style': 'resize:none;'
			}
		),
	)

	class Meta:
		model = DocumentVersion
		fields = [
			'name', 'title', 'description', 'content_file',
		]

class CreateVersionForm(ModelForm):

	mode = forms.ChoiceField(
		label='Version Change Type',
		choices=(
			('major', 'major'),
			('minor', 'minor'),
		)
	)

	class Meta:
		model = DocumentVersion
		fields = [
			'comment', 'mode', 'content_file',
		]

class CreateProjectForm(ModelForm):

	class Meta:
		model = Project
		fields = [
			'name', 'key', 'ptype', 'title', 'description',
		]

class EditDocumentForm(ModelForm):
	
	class Meta:
		model = Document
		fields = [
			'name', 'title', 'description',
		]

class AddNewMemberForm(forms.Form):

	user = forms.ModelChoiceField(
		queryset=User.objects.all().exclude(is_active=False).order_by('username')
	)

	group = forms.ChoiceField(
		choices=(
			('Manager', 'Manager'),
			('Collaborator', 'Collaborator'),
			('User', 'User'),
		),
	)
