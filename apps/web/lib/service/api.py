"""Api module for web client.
"""

from apps.web.lib.service import ProjectAuthorization, ProjectFolderAuthorization, ProjectDocumentAuthorization, ProjectService, auditing_enabled, Auditor, Project, Folder, Document, logging

logger = logging.getLogger(__name__)


class UnauthorizedActionException(Exception):

	def __init__(self, message):
		self.message = message

	def __str__(self):
		return self.message

class ProjectApi(object):

	def __init__(self, user, project=None, parent_folder=None, folder=None, document=None):
		self.set_user(user)
		self.set_project(project)
		self.set_folder(folder)
		self.set_document(document)

		if not parent_folder and project:
			self.set_parent_folder(self.project.folder)
		elif not parent_folder and not project:
			self.set_parent_folder(None)
		else:
			self.set_parent_folder(parent_folder)

	def set_user(self, user):
		"""Set the default user.
		"""
		self.user = user

	def get_user(self):
		"""Return the default user.
		"""
		return self.user

	def set_project(self, project):
		"""Set the default project.
		"""
		self.project = project

	def get_project(self):
		"""Return the default project.
		"""
		project_authorization = ProjectAuthorization(self.user, self.project)
		if project_authorization.is_authorized_read():
			if self.project:
				if auditing_enabled:
					auditor = Auditor(self.user, self.project)
					auditor.create_entry('read')
				return self.project
			else:
				logger.error(
					'Authorized to read project but unable to do so.')
				return False

		else:
			if auditing_enabled:
				auditor = Auditor(self.user, Project)
				message = "Unauthorized project creation attempt by '{}' to read project '{}'".format(
					self.user,
					self.project.name,
				)
				auditor.create_entry('security', message)
			raise UnauthorizedActionException(
				"User '{}' is not authorized to read project '{}'".format(
					self.user, self.project.name,
				)
			)
		return None

	def set_parent_folder(self, folder):
		"""Set the project folder.
		"""
		self.parent_folder = folder

	def get_parent_folder(self):
		"""Return the default project folder.
		"""
		return self.parent_folder

	def set_folder(self, folder):
		"""Set the current folder.
		"""
		self.folder = folder

	def get_folder(self, folder):
		"""Return the current folder.
		"""
		folder = Folder.objects.get(uuid=folder.uuid)
		return folder

	def set_document(self, document):
		"""Set the current document.
		"""
		self.document = document

	def get_document(self):
		"""Return the current document.
		"""
		return self.document

	def create_project(self, name, key, ptype, owner, **kwargs):
		project_authorization = ProjectAuthorization(self.user)
		if project_authorization.is_authorized_creation():
			new_project = ProjectService().create_project(
				name, key, ptype, owner, **kwargs
			)
			self.set_project(new_project)
			if new_project:
				if auditing_enabled:
					auditor = Auditor(self.user, new_project)
					auditor.create_entry('create')
				return True
			else:
				logger.error(
					'Authorized to create project but unable to do so.')
				return False

		else:
			if auditing_enabled:
				auditor = Auditor(self.user, Project)
				message = "Unauthorized project creation attempt by '{}' to create project '{}'".format(
					self.user,
					name,
				)
				auditor.create_entry('security', message)
			raise UnauthorizedActionException(
				"User '{}' is not authorized to create project '{}'".format(
					self.user, name,
				)
			)

	def update_project(self, **kwargs):
		project_authorization = ProjectAuthorization(self.user, self.project)
		if project_authorization.is_authorized_update():
			project_service = ProjectService(self.project)
			updated_project = project_service.update_project(
				**kwargs
			)
			self.set_project(updated_project)
			if updated_project:
				if auditing_enabled:
					auditor = Auditor(self.user, updated_project)
					auditor.create_entry('update')
				return True
			else:
				logger.error(
					'Authorized to update project but unable to do so.')
				return False

		else:
			if auditing_enabled:
				auditor = Auditor(self.user, Project)
				message = "Unauthorized project update attempt by '{}' to update project '{}'".format(
					self.user,
					self.project.name,
				)
				auditor.create_entry('security', message)
			raise UnauthorizedActionException(
				"User '{}' is not authorized to update project '{}'".format(
					self.user, self.project.name,
				)
			)

	def delete_project(self):
		project_authorization = ProjectAuthorization(self.user, self.project)
		if project_authorization.is_authorized_delete():
			project_service = ProjectService(self.project)
			project_service.delete_project()
			
			if auditing_enabled:
				auditor = Auditor(self.user, self.project)
				auditor.create_entry('delete')

			self.set_project(None)
			return True

		else:
			if auditing_enabled:
				auditor = Auditor(self.user, Project)
				message = "Unauthorized project delete attempt by '{}' to delete project '{}'".format(
					self.user,
					self.project.name,
				)
				auditor.create_entry('security', message)
			raise UnauthorizedActionException(
				"User '{}' is not authorized to delete project '{}'".format(
					self.user, self.project.name,
				)
			)

	def create_project_folder(self, name, parent_folder, owner, **kwargs):
		project_folder_authorization = ProjectFolderAuthorization(
			self.user,
			self.project,
		)
		if project_folder_authorization.is_authorized_creation():
			project_service = ProjectService(self.project)
			new_folder = project_service.create_folder(
				name, parent_folder, owner, **kwargs
			)

			if auditing_enabled:
				auditor = Auditor(self.user, new_folder)
				auditor.create_entry('create')

			self.set_project(self.project)
			return new_folder
		else:
			if auditing_enabled:
				auditor = Auditor(self.user, Folder)
				message = "Unauthorized project folder create attempt by '{}' to create folder '{}'".format(
					self.user,
					name,
				)
				auditor.create_entry('security', message)
			raise UnauthorizedActionException(
				"User '{}' is not authorized to create folder in project '{}'".format(
					self.user, self.project.name,
				)
			)

	def get_project_folder(self, folder):
		project_folder_authorization = ProjectFolderAuthorization(
			self.user,
			self.project,
		)
		if project_folder_authorization.is_authorized_read():
			project_service = ProjectService(self.project)
			retFolder = project_service.get_folder(
				folder
			)

			if auditing_enabled:
				auditor = Auditor(self.user, folder)
				auditor.create_entry('read')

			self.set_project(self.project)
			return retFolder
		else:
			if auditing_enabled:
				auditor = Auditor(self.user, Folder)
				message = "Unauthorized attempt by '{}' to read folder '{}'".format(
					self.user,
					folder.name,
				)
				auditor.create_entry('security', message)
			raise UnauthorizedActionException(
				"User '{}' is not authorized to read folder in project '{}'".format(
					self.user, self.project.name,
				)
			)

	def update_project_folder(self, folder, **kwargs):
		project_folder_authorization = ProjectFolderAuthorization(
			self.user,
			self.project,
			folder,
		)
		if project_folder_authorization.is_authorized_update():
			project_service = ProjectService(self.project)
			folder = project_service.update_folder(
				folder, **kwargs
			)

			if auditing_enabled:
				auditor = Auditor(self.user, folder)
				auditor.create_entry('update')

			self.set_project(self.project)
			return folder
		else:
			if auditing_enabled:
				auditor = Auditor(self.user, Folder)
				message = "Unauthorized attempt by '{}' to update folder '{}'".format(
					self.user,
					folder.name,
				)
				auditor.create_entry('security', message)
			raise UnauthorizedActionException(
				"User '{}' is not authorized to update folder in project '{}'".format(
					self.user, self.project.name,
				)
			)

	def delete_project_folder(self, folder):
		project_folder_authorization = ProjectFolderAuthorization(
			self.user,
			self.project,
			folder,
		)

		if project_folder_authorization.is_authorized_delete():
			project_service = ProjectService(self.project)
			project_service.delete_folder(folder)

			if auditing_enabled:
				auditor = Auditor(self.user, folder)
				auditor.create_entry('delete')

			self.set_project(self.project)
			return True
		else:
			if auditing_enabled:
				auditor = Auditor(self.user, folder)
				message = "Unauthorized attempt by '{}' to delete folder '{}'".format(
					self.user,
					folder.name,
				)
				auditor.create_entry('security', message)
			raise UnauthorizedActionException(
				"User '{}' is not authorized to delete folder {} in project '{}'".format(
					self.user, folder.name, self.project.name,
				)
			)

	def create_project_document(self, name, parent_folder, content_file, owner, **kwargs):
		project_document_authorization = ProjectDocumentAuthorization(
			self.user,
			self.project,
		)
		if project_document_authorization.is_authorized_creation():
			project_service = ProjectService(self.project)
			new_document = project_service.create_document(
				name, parent_folder, content_file, owner, **kwargs
			)

			if auditing_enabled:
				auditor = Auditor(self.user, new_document)
				auditor.create_entry('create')

			self.set_project(self.project)
			return new_document
		else:
			if auditing_enabled:
				auditor = Auditor(self.user, Document)
				message = "Unauthorized project document create attempt by '{}' to create document '{}'".format(
					self.user,
					name,
				)
				auditor.create_entry('security', message)
			raise UnauthorizedActionException(
				"User '{}' is not authorized to create document in project '{}'".format(
					self.user, self.project.name,
				)
			)

	def get_project_document(self, document):
		project_document_authorization = ProjectDocumentAuthorization(
			self.user,
			self.project,
			document
		)
		if project_document_authorization.is_authorized_read():
			project_service = ProjectService(self.project)
			ret_document = project_service.get_document(
				document
			)

			if auditing_enabled:
				auditor = Auditor(self.user, document)
				auditor.create_entry('read')

			self.set_project(self.project)
			return ret_document
		else:
			if auditing_enabled:
				auditor = Auditor(self.user, Document)
				message = "Unauthorized attempt by '{}' to read document '{}'".format(
					self.user,
					document.name,
				)
				auditor.create_entry('security', message)
			raise UnauthorizedActionException(
				"User '{}' is not authorized to read document in project '{}'".format(
					self.user, self.project.name,
				)
			)

	def update_project_document(self, document, **kwargs):
		project_document_authorization = ProjectDocumentAuthorization(
			self.user,
			self.project,
			document,
		)
		if project_document_authorization.is_authorized_update():
			project_service = ProjectService(self.project)
			document = project_service.update_document(
				document, **kwargs
			)

			if auditing_enabled:
				auditor = Auditor(self.user, document)
				auditor.create_entry('update')

			self.set_project(self.project)
			return document
		else:
			if auditing_enabled:
				auditor = Auditor(self.user, Document)
				message = "Unauthorized attempt by '{}' to update document '{}'".format(
					self.user,
					document.name,
				)
				auditor.create_entry('security', message)
			raise UnauthorizedActionException(
				"User '{}' is not authorized to update document in project '{}'".format(
					self.user, self.project.name,
				)
			)

	def delete_project_document(self, document):
		project_document_authorization = ProjectDocumentAuthorization(
			self.user,
			self.project,
			document,
		)

		if project_document_authorization.is_authorized_delete():
			project_service = ProjectService(self.project)
			project_service.delete_document(document)

			if auditing_enabled:
				auditor = Auditor(self.user, document)
				auditor.create_entry('delete')

			self.set_project(self.project)
			return True
		else:
			if auditing_enabled:
				auditor = Auditor(self.user, document)
				message = "Unauthorized attempt by '{}' to delete document '{}'".format(
					self.user,
					document.name,
				)
				auditor.create_entry('security', message)
			raise UnauthorizedActionException(
				"User '{}' is not authorized to delete document {} in project '{}'".format(
					self.user, document.name, self.project.name,
				)
			)

