"""Contains ProjectService class.
"""

from django.contrib.auth.models import Group, User
from apps.repository.lib.service.repository import FDService
from apps.web.config.project import project_types, project_update_excluded_fields
from apps.web.models import Project
from apps.repository.models import Folder, Document
from apps.repository.lib.service.folder import FolderService
from apps.repository.lib.service.document import DocumentService
import logging

logger = logging.getLogger(__name__)

class InvalidProjectTypeException(Exception):
	"""Exception raised when an invalid project type is input.
	"""
	def __init__(self, message):
		self.message = message

	def __str__(self):
		return self.message


class ProjectService(FDService):
	"""Api for interfacing with apps.web.models.Project.
	"""
	def __init__(self, project=None):
		"""Set the default project.
		"""
		self.project = project

	def create_project(self, name, key, ptype, owner, **kwargs):
		"""Create a project.
		"""
		project = Project()
		project.name = name
		project.owner = owner
		project.key = key

		for k,v in kwargs.items():
			setattr(project, k, v)

		if ptype not in project_types:
			raise InvalidProjectTypeException(
				"'{}' is an invalid project type.".format(
					ptype,
				)
			)
		else:
			project.ptype = ptype

		folder_service = FolderService()
		projects_folder = folder_service.get_projects_folder()
		project_folder = folder_service.create(
			project.name,
			projects_folder,
			owner,
			title="Project folder for {} Project".format(name.title()),
			description="Project folder for {} Project".format(name.title()),
		)
		project.folder = project_folder
		project.save()


		for g in ('manager', 'contributor', 'user',):
			group = Group.objects.create(name='{}_{}'.format(project.key, g,))
			if g == 'manager':
				group.user_set.add(owner)

		self.project = project
		return self.project

	def get_project_home_folder(self):
		"""Returns the default folder for the project."""
		return self.project.folder

	def set_project(self, project):
		self.project = project

	def get_project(self, project):
		"""Return project.
		"""
		project = Project.objects.get(key=project.key)
		return project

	def update_project(self, **kwargs):
		"""Update Project properties.
		"""
		if self.project:
			for k,v in kwargs.items():
				if k not in project_update_excluded_fields:
					setattr(self.project, k, v)
			self.project.save()
			return True
		else:
			return False

	def delete_project(self):
		""" Delete Project. Also, deletes associated project groups and project folder.
		"""
		if self.project:
			for g in ('manager', 'contributor', 'user',):
				group = Group.objects.get(
					name='{}_{}'.format(self.project.key, g,))
				group.delete()
			folder = self.get_project_home_folder()
			folder_service = FolderService(folder)
			folder_service.delete()
			self.project.delete()

	def add_member(self, user, group_type):
		"""Add a member to the project by adding them to project groups.
		"""
		group = Group.objects.get(name='{}_{}'.format(self.project.key, group_type))
		group.user_set.add(user)

	def get_users(self):
		"""Return members of the Project 'user' group.
		"""
		return User.objects.filter(groups__name='{}_user'.format(self.project.key))

	def get_contributors(self):
		"""Return members of the Project 'contributor' group.
		"""
		return User.objects.filter(groups__name='{}_contributor'.format(self.project.key))

	def get_managers(self):
		"""Return members of the Project 'manager' group.
		"""
		return User.objects.filter(groups__name='{}_manager'.format(self.project.key))

	def get_members(self):
		"""Return all members of the Project in various groups as a queryset.
		"""
		from itertools import chain
		members = list(
			chain(
				self.get_users(), 
				self.get_managers(), 
				self.get_contributors()
			)
		)
		return members

	def create_folder(self, name, parent_folder, owner, **kwargs):
		folder_service = FolderService()
		folder = folder_service.create(
			name,
			parent_folder,
			owner,
			**kwargs
		)
		return folder

	def get_folder(self, folder):
		return Folder.objects.get(uuid=folder.uuid)

	def update_folder(self, folder, **kwargs):
		folder_service = FolderService(folder)
		folder_service.update(**kwargs)
		return folder

	def delete_folder(self, folder):
		folder_service = FolderService(folder)
		folder_service.delete()

	def create_document(self, name, parent_folder, content_file, owner, **kwargs):
		document_service = DocumentService()
		document = document_service.create(
			name,
			parent_folder,
			content_file,
			owner,
			**kwargs
		)
		return document

	def get_document(self, document):
		return Document.objects.get(uuid=document.uuid)

	def update_document(self, document, **kwargs):
		document_service = DocumentService(document)
		document_service.update(**kwargs)
		return document

	def delete_document(self, document):
		document_service = DocumentService(document)
		document_service.delete()

	def is_in_project_path(self, folder):

		if Project.objects.filter(folder=folder):
			return True

		if hasattr(folder, 'parent_folder'):
			return self.is_in_project_path(folder.parent_folder)

		return False