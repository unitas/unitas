"""Package de-noter for apps.web.lib.service.
"""

from apps.web.modules.authorization.lib.project import ProjectAuthorization, ProjectFolderAuthorization, ProjectDocumentAuthorization
from apps.web.lib.service.project import ProjectService
from apps.services.config import auditing_enabled
from apps.services.modules.auditing.lib.auditor import Auditor
from apps.web.models import Project
from apps.repository.models import Folder, Document

import logging
from apps.repository.lib.service.repository import FDService
from apps.web.config.project import project_types, project_update_excluded_fields
from apps.repository.lib.service.folder import FolderService
from apps.repository.lib.service.document import DocumentService
from django.contrib.auth.models import Group, User
