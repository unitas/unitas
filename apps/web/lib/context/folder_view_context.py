"""Contains the FolderViewContext class.
"""
from apps.repository.lib.service.folder import FolderService
from apps.repository.lib.service.repository import RepoService
from apps.repository.models import Folder, Document

class FolderViewContext(object):
	"""Generates folder view context.
	"""
	folder_service = FolderService()

	def __init__(self, user, folder_id, session, is_in_shared_docs_folder=False, request_method=None):
		"""Initializes the FolderViewContext class.
		args	user (User object) the request user object
				folder_id (UUID) for getting the current folder.
		returns None
		"""
		self.user = user
		self.folder_id = folder_id
		self.current_folder = Folder.objects.get(pk=folder_id)
		self.root_folder = self.folder_service.get_root_folder()
		self.query_children()
		self.session = session
		self.is_in_shared_docs_folder = is_in_shared_docs_folder
		self.request_method = request_method

	def get_user_home_folder_uuid(self):
		"""Return the current user's home folder uuid.
		"""
		self.folder_service.set_user(self.user)
		user_home_folder_uuid = self.folder_service.get_user_home_folder().uuid
		return user_home_folder_uuid

	def get_nav_folders(self):
		"""Return the child folders in the root folder.
		"""
		projects_folder = Folder.objects.get(name='Projects', parent_folder=self.root_folder)
		shared_docs_folder = Folder.objects.get(name='Shared Documents', parent_folder=self.root_folder)

		nav_folders = (projects_folder, shared_docs_folder)

		return nav_folders

	def get_current_folder(self):
		"""Return the current folder object.
		"""
		return self.current_folder

	def query_children(self):
		"""Query for the child objects of the current folder (folders and documents).
		"""
		self.folder_service.set_folder(self.current_folder)
		self.children = self.folder_service.get_children(
			sticky_system_folders=True
		)
		
	def get_child_documents(self):
		"""Return child documents for the current folder.
		"""
		return self.children['documents']

	def get_child_folders(self):
		"""Return child folders for the current folder.
		"""
		return self.children['folders']

	def get_user_trashcan_folder_uuid(self):
		self.folder_service.set_user(self.user)
		folder = self.folder_service.get_user_trashcan_folder()
		return folder.uuid

	def is_in_trashcan_path(self):
		"""Returns boolean if current_folder is in the path of the user's trashcan.
		"""
		return self.folder_service.is_in_trashcan_path(self.get_current_folder())

	def is_pastable(self):
		"""Returns boolean if clipboard document/folder can be pasted in the current directory. Prevents duplicately named documents and folders in the same parent folder. Also prevents recursive copies and moves which will cause race conditions.
		"""
		if self.session.get('action'):

			if self.session.get('action') and self.session.get('object') and self.session.get('uuid'):

				if self.session.get('object') == 'document':
					obj = Document
				
				elif self.session.get('object') == 'folder':
					obj = Folder

				o = obj.objects.get(pk=self.session.get('uuid'))
				
				repo_service = RepoService()
				repo_service.set_object(o)

				document = Document.objects.filter(name=o.name, parent_folder=self.get_current_folder())
				if document.count() > 0:
					same_name_document_exists = True
				else:
					same_name_document_exists = False

				folder = Folder.objects.filter(name=o.name, parent_folder=self.get_current_folder())
				if folder.count() > 0:
					same_name_folder_exists = True
				else:
					same_name_folder_exists = False
					
				if repo_service.is_in_path_of_self(self.get_current_folder()):
					return False
				
				elif o.parent_folder == self.get_current_folder():
					return False

				elif same_name_document_exists:
					return False

				elif same_name_folder_exists:
					return False
				
				else:
					return True
		
		return False

	def get_context(self, inc_folder_list=True):
		"""Build and return the context.
		"""
		folder_context = {
			'auth_user': self.user,
			'nav_folders': self.get_nav_folders(),
			'user_home_folder_uuid': self.get_user_home_folder_uuid(),
			'parent_folder_uuid': self.folder_id,
			'current_folder': self.get_current_folder(),
			'user_trashcan_folder_uuid': self.get_user_trashcan_folder_uuid(),
			'current_folder_in_trashcan_path': self.is_in_trashcan_path(),
			'is_pastable': self.is_pastable(),
			'is_shared_docs_view': self.is_in_shared_docs_folder,
		}

		from itertools import chain
		from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

		if self.is_in_shared_docs_folder:

			node_list = list(
				chain(
					[], Document.objects.filter(is_shared=True)
				)
			)

		else:

			if not inc_folder_list:
				node_list = list(
					chain(
						self.get_child_documents(),
					)
				)

			else:

				node_list = list(
					chain(
						self.get_child_folders(),
						self.get_child_documents(),
					)
				)

		paginator = Paginator(node_list, 25)

		try:
			page = self.request_method.get('page')
		except AttributeError:
			page = 0

		try:
			nodes = paginator.page(page)
		except PageNotAnInteger:
			nodes = paginator.page(1)
		except EmptyPage:
			nodes = paginator.page(paginator.num_pages)

		document_list = [
			node for node in nodes if node.model_type.name == 'document'
		]

		folder_list = [
			node for node in nodes if node.model_type.name == 'folder'
		]

		folder_context.update(
			{
				'document_list': document_list,
				'folder_list': folder_list,
				'nodes': nodes,
				'node_count': len(node_list),
			}
		)

		return folder_context


