"""Contains the FolderViewContext class.
"""
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from apps.repository.lib.service.folder import FolderService
from apps.repository.lib.service.repository import RepoService
from apps.web.lib.service.project import ProjectService
from apps.web.models import Project
from apps.repository.models import Folder, Document


class ProjectViewContext(object):
	"""Generates folder view context.
	"""
	folder_service = FolderService()

	def __init__(self, user, session, request=None, is_in_shared_docs_folder=False, request_method=None):
		"""Initializes the FolderViewContext class.
		args	user (User object) the request user object
				folder_id (UUID) for getting the current folder.
		returns None
		"""
		self.request = request
		self.user = user
		self.root_folder = self.folder_service.get_root_folder()
		self.is_in_shared_docs_folder = is_in_shared_docs_folder
		self.request_method = request_method

	def get_user_home_folder_uuid(self):
		"""Return the current user's home folder uuid.
		"""
		self.folder_service.set_user(self.user)
		user_home_folder_uuid = self.folder_service.get_user_home_folder().uuid
		return user_home_folder_uuid

	def get_nav_folders(self):
		"""Return the child folders in the root folder.
		"""
		projects_folder = Folder.objects.get(name='Projects', parent_folder=self.root_folder)
		shared_docs_folder = Folder.objects.get(name='Shared Documents', parent_folder=self.root_folder)

		nav_folders = (projects_folder, shared_docs_folder)

		return nav_folders

	def get_child_folders(self):
		"""Return child folders for the current folder.
		"""
		return self.children['folders']

	def get_user_trashcan_folder_uuid(self):
		self.folder_service.set_user(self.user)
		folder = self.folder_service.get_user_trashcan_folder()
		return folder.uuid

	def get_parent_folder(self):
		folder_service = FolderService()
		folder_service.set_user(self.user)
		return folder_service.get_user_home_folder()

	def get_projects_folder(self):
		folder_service = FolderService()
		return folder_service.get_projects_folder()

	def get_projects(self):

		all_projects = Project.objects.all()
		
		if self.user.username == 'admin':
			project_list = all_projects.order_by('name')

		elif self.user.is_superuser == True:
			project_list = list(all_projects.filter(
				Q(owner=self.user) | Q(ptype='Open') | Q(ptype='Public')
			).order_by('name'))

			for project in all_projects:
				
				project_service = ProjectService(project)

				if self.user in project_service.get_members():
					if project not in all_projects:
						project_list.append(project)

		else:
			project_list = list(all_projects.filter(
				Q(owner=self.user) | Q(ptype='Open') | Q(ptype='Public')
			).order_by('name'))

			for project in all_projects:
				
				project_service = ProjectService(project)

				if self.user in project_service.get_members():
					if project not in all_projects:
						project_list.append(project)

		return project_list

	def get_context(self, inc_folder_list=True):
		"""Build and return the context.
		"""
		folder_context = {
			'auth_user': self.user,
			'nav_folders': self.get_nav_folders(),
			'user_home_folder_uuid': self.get_user_home_folder_uuid(),
			'user_trashcan_folder_uuid': self.get_user_trashcan_folder_uuid(),
			'is_shared_docs_view': self.is_in_shared_docs_folder,
			'parent_folder_uuid': self.get_parent_folder().uuid,
			'project_home_folder': self.get_projects_folder(),
		}

		if self.request:

			paginator = Paginator(self.get_projects(), 25)
			page = self.request.GET.get('page')

			try:
				projects = paginator.page(page)
			except PageNotAnInteger:
				projects = paginator.page(1)
			except EmptyPage:
				projects = paginator.page(paginator.num_pages)

			folder_context.update(
				{
					'projects': projects,
				}
			)

		return folder_context


