
from apps.web.config.preview import exportable_as_pdf, previewable_documents
from apps.repository.lib.service.folder import DocumentService
from apps.repository.models import DocumentVersion

class DocumentPropertiesContext(object):

	def __init__(self, document):
		self.document = document

	def is_document_exportable_as_pdf(self):

		doc_extension = '.{}'.format(self.document.name.split('.')[-1])

		if doc_extension in exportable_as_pdf:
			return True
		else:
			return False

	def is_document_previewable(self):
		doc_extension = '.{}'.format(self.document.name.split('.')[-1])

		if doc_extension in previewable_documents:
			return True
		else:
			return False

	def get_latest_version(self):
		document_service = DocumentService()
		document_service.set_document(self.document)
		document_version = document_service.get_latest_version()
		return document_version.content_file

	def get_version_list(self):

		version_list = DocumentVersion.objects.filter(
			parent_document=self.document
		)

		return version_list

	def get_context(self, inc_version_list=False):

		context = {
			'document_exportable_as_pdf': self.is_document_exportable_as_pdf(),
			'document_is_previewable': self.is_document_previewable(),
			'content_file': self.get_latest_version(),
		}

		if inc_version_list:
			context.update(
				{
					'version_list': self.get_version_list(),
				}
			)

		return context