from apps.web.lib.view import login_required, LOGIN_URL, CreateProjectForm, ProjectViewContext, render, ProjectApi, HttpResponseRedirect, reverse, Folder, FolderService, FolderAuthorization, Project, ProjectService, AddNewMemberForm


@login_required(login_url=LOGIN_URL)
def projects_home_view(request):

	root_folder = Folder.objects.get(name='Repository', parent_folder=None, is_system=True)

	is_in_shared_docs_folder = False
	
	folder = FolderService().get_projects_folder();

	if folder == FolderService().get_shared_documents_folder():
		is_in_shared_docs_folder = True

	#folder_auth = FolderAuthorization(request.user, folder)

	#if folder_auth.is_authorized_read():

	view_context = {}
	project_view_context = ProjectViewContext(
		request.user, 
		request.session,
		request=request
	)
	
	view_context.update(project_view_context.get_context())
	view_context.update(
		{
			'is_projects_view': True,
		}
	)
	
	return render(
		request,
		'web/projects.html',
		view_context,
	)


@login_required(login_url=LOGIN_URL)
def create_project_view(request):

	"""Create a Project.
	"""

	root_folder = Folder.objects.get(name='Repository', parent_folder=None, is_system=True)

	if request.method == "POST":
		
		create_project_form = CreateProjectForm(
			request.POST
		)
		
		if create_project_form.is_valid():
			
			project_api = ProjectApi(request.user)
			project_api.create_project(
				create_project_form.cleaned_data['name'],
				create_project_form.cleaned_data['key'],
				create_project_form.cleaned_data['ptype'],
				request.user,
				title=create_project_form.cleaned_data['title'],
				description=create_project_form.cleaned_data['description'],
			)

			return HttpResponseRedirect(
				reverse(
					'folder_view',
					kwargs={
						'folder_id': Folder.objects.get(name='Projects', parent_folder=root_folder).uuid,
					}
				)
			)

	elif request.method == "GET":
		
		create_project_form = CreateProjectForm()

	view_context = {}
	
	project_view_context = ProjectViewContext(
		request.user,
		request.session,
	)
	
	view_context.update(
		project_view_context.get_context(inc_folder_list=False)
	)
	
	view_context.update(
		{'create_project_form': create_project_form},
	)
	
	return render(
		request,
		'web/create_project.html',
		view_context,
	)


@login_required(login_url=LOGIN_URL)
def manage_project_view(request, project_id):

	project = Project.objects.get(pk=project_id)
	project_service = ProjectService(project)

	view_context = {}
	
	project_view_context = ProjectViewContext(
		request.user,
		request.session
	)
	
	view_context.update(
		project_view_context.get_context(inc_folder_list=False)
	)

	view_context.update(
		{
			'project': project,
			'project_managers': project_service.get_managers(),
			'project_contributors': project_service.get_contributors(),
			'project_users': project_service.get_users(),
		}
	)
	
	return render(
		request,
		'web/manage_project.html',
		view_context,
	)

@login_required(login_url=LOGIN_URL)
def add_members_view(request, project_id):

	project = Project.objects.get(pk=project_id)

	if request.method == 'POST':
		add_member_form = AddNewMemberForm(request.POST)
		if add_member_form.is_valid():
			user = add_member_form.cleaned_data['user']
			group = add_member_form.cleaned_data['group']
			project_service = ProjectService(project)
			project_service.add_member(user, group.lower())
			return HttpResponseRedirect(
				reverse(
					'manage_project_view',
					kwargs={
						'project_id': project.uuid 
					}
				)
			)

	elif request.method == 'GET':
		add_member_form = AddNewMemberForm()

	project_view_context = ProjectViewContext(
		request.user,
		request.session
	)

	view_context = {}

	view_context.update(
		project_view_context.get_context(inc_folder_list=False)
	)

	view_context.update(
		{
			'add_member_form': add_member_form,
			'project': project,
		}
	)

	return render(
		request,
		'web/add_member.html',
		view_context,
	)
