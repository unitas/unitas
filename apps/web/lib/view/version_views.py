from apps.web.lib.view import login_required, LOGIN_URL, Document, CreateVersionForm, FolderViewContext, render, HttpResponseRedirect, DocumentAuthorization, DocumentService, reverse, DocumentPropertiesContext, DocumentApi, DocumentVersion, HttpResponse

@login_required(login_url=LOGIN_URL)
def upload_new_version_view(request, document_id):
	"""Upload a new version for current document.
	"""

	document = Document.objects.get(pk=document_id)

	if request.method == "POST":
		
		create_version_form = CreateVersionForm(
			request.POST,
			request.FILES,
		)
		
		if create_version_form.is_valid():

			document_api = DocumentApi(request.user)
			document_api.add_new_version(
				document,
				create_version_form.cleaned_data['content_file'],
				create_version_form.cleaned_data['mode'],
				comment=create_version_form.cleaned_data['comment']
			)

			return HttpResponseRedirect(
				reverse(
					'document_properties_view',
					args=(document.uuid,),
				)
			)

	elif request.method == "GET":
		
		create_version_form = CreateVersionForm()

	view_context = {}
	
	folder_view_context = FolderViewContext(
		request.user,
		document.parent_folder.uuid,
		request.session,
	)
	
	view_context.update(
		folder_view_context.get_context(inc_folder_list=False)
	)
	
	view_context.update(
		{
			'create_version_form': create_version_form,
			'current_document': document
		},
	)
	
	return render(
		request,
		'web/create_document_version.html',
		view_context,
	)

@login_required(login_url=LOGIN_URL)
def versions_view(request, document_id):

	current_document = Document.objects.get(pk=document_id)

	view_context = {}

	folder_view_context = FolderViewContext(
		request.user,
		current_document.parent_folder.uuid,
		request.session,
	)

	document_properties_context = DocumentPropertiesContext(
		current_document
	)
	
	view_context.update(
		folder_view_context.get_context(inc_folder_list=False)
	)

	view_context.update(
		{
			'current_document': current_document,
		},
	)

	view_context.update(
		document_properties_context.get_context(inc_version_list=True)
	)
	
	return render(
		request,
		'web/all_versions.html',
		view_context,
	)

def get_versioned_file(document_version):

	document_name = document_version.parent_document.name
	name, extension = document_name.split('.')[:-1][0], document_name.split('.')[-1]
	return '{}-{}.{}'.format(
		name, 
		document_version.version, 
		extension
	)


@login_required(login_url=LOGIN_URL)
def download_version_view(request, version_id):

	document_version = DocumentVersion.objects.get(pk=version_id)

	response = HttpResponse(
		document_version.content_file.file, 
		content_type='text/plain'
	)
	
	response['Content-Disposition'] = 'attachment; filename={}'.format(
		get_versioned_file(document_version)
	)

	return response