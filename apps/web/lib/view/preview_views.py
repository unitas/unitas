
from __future__ import absolute_import
from apps.web.models import PreviewDocument
from django.http import HttpResponse

class PreviewView(object):
	"""Preview views - (PDF, Transformed to PDF view and view for image files)
	"""
	
	def __init__(self, document_version, current_document):
		
		self.document_version = document_version
		
		self.current_document = current_document

	def pdf_preview_view(self):
		"""Shows when the current document is a PDF file.
		"""

		with open(self.document_version.content_file.path, 'rb') as pdf:
			
			response = HttpResponse(
				pdf.read(),
				content_type='application/pdf'
			)
	        
	        response['Content-Disposition'] = 'filename={}'.format(
	        	self.current_document.name
	        )
	        
	        return response

	def preview_document_view(self):
		"""Shows the preview PDF file generated for the document.
		"""

		preview_document = PreviewDocument.objects.get(
			document_version=self.document_version
		)

		with open(preview_document.content_file.path, 'rb') as pdf:
			
			response = HttpResponse(
				pdf.read(),
				content_type='application/pdf'
			)
	        
	        response['Content-Disposition'] = 'filename={}'.format(
	        	self.current_document.name
	        )
	        
	        return response

	def image_preview_view(self):
		"""Shows for documents that are images by nature that are not PDF files.
		"""

		with open(self.document_version.content_file.path, 'rb') as image:
			
			response = HttpResponse(
				image.read(), 
				content_type=self.current_document.mime_type.designation
			)
			
			response['Content-Disposition'] = 'inline;filename={}'.format(
				self.current_document.name
			)
			
			return response