"""Imports for apps.web.lib.view package.
"""

from __future__ import absolute_import

from django.contrib.auth.decorators import login_required
from unitas.settings import LOGIN_URL
from apps.web.lib.context.folder_view_context import FolderViewContext
from apps.web.lib.context.project_view_context import ProjectViewContext
from apps.web.lib.context.document_properties_context import DocumentPropertiesContext
from django.shortcuts import render
from apps.web.forms import CreateFolderForm, CreateDocumentForm, EditFolderForm, EditDocumentForm, CreateVersionForm, CreateProjectForm, AddNewMemberForm
from apps.web.models import PreviewDocument, Project
from apps.web.config.preview import previewable_documents, exportable_as_pdf
from apps.web.lib.view.preview_views import PreviewView
from apps.web.lib.service.api import ProjectApi
from apps.web.lib.service.project import ProjectService
from apps.api.lib.folder import FolderApi
from apps.api.lib.document import DocumentApi
from apps.repository.models import Folder, Document, DocumentVersion
from apps.repository.lib.service.folder import FolderService
from apps.repository.lib.service.document import DocumentService
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from apps.services.modules.authorization.lib.folder import FolderAuthorization
from apps.services.modules.authorization.lib.document import DocumentAuthorization