
from apps.web.lib.view import login_required, LOGIN_URL, FolderViewContext, render, CreateDocumentForm, FolderApi, DocumentApi, Folder, HttpResponseRedirect, Document, reverse, FolderService, EditDocumentForm, DocumentService, HttpResponse, DocumentAuthorization, DocumentPropertiesContext, PreviewDocument, previewable_documents, exportable_as_pdf, PreviewView


@login_required(login_url=LOGIN_URL)
def document_properties_view(request, document_id):
	"""View document's properties and details.
	"""

	current_document = Document.objects.get(pk=document_id)

	view_context = {}

	folder_view_context = FolderViewContext(
		request.user,
		current_document.parent_folder.uuid,
		request.session,
	)

	document_properties_context = DocumentPropertiesContext(
		current_document
	)
	
	view_context.update(
		folder_view_context.get_context(inc_folder_list=False)
	)

	view_context.update(
		{
			'current_document': current_document,
		},
	)

	view_context.update(
		document_properties_context.get_context()
	)
	
	return render(
		request,
		'web/document_properties.html',
		view_context,
	)


@login_required(login_url=LOGIN_URL)
def preview_document_view(request, document_id):
	"""Preview document's content.
	"""
	current_document = Document.objects.get(pk=document_id)
	doc_extension = '.' + current_document.name.split('.')[-1]

	document_service = DocumentService()
	document_service.set_document(current_document)
	document_version = document_service.get_latest_version()

	preview_view = PreviewView(document_version, current_document)

	if doc_extension == '.pdf':
	  	return preview_view.pdf_preview_view()

	elif doc_extension in exportable_as_pdf:
		return preview_view.preview_document_view()

	else:
		return preview_view.image_preview_view()


@login_required(login_url=LOGIN_URL)
def download_document_view(request, document_id):
	"""Download document.
	"""
	
	document = Document.objects.get(pk=document_id)
	document_service = DocumentService()
	document_service.set_document(document)
	latest_doc = document_service.get_latest_version()
	
	response = HttpResponse(
		latest_doc.content_file.file, 
		content_type='text/plain'
	)
	
	response['Content-Disposition'] = 'attachment; filename={}'.format(
		document.name
	)

	return response


@login_required(login_url=LOGIN_URL)
def export_document_as_pdf_view(request, document_id):
	"""Download PDF version of document.
	"""
	
	document = Document.objects.get(pk=document_id)
	document_service = DocumentService()
	document_service.set_document(document)
	document_version = document_service.get_latest_version()

	preview_document = PreviewDocument.objects.get(
		document_version=document_version,
	)

	response = HttpResponse(
		preview_document.content_file.file,
		content_type='text/plain'
	)
	response['Content-Disposition'] = 'attachment; filename={}.pdf'.format(document.name
	)

	return response


@login_required(login_url=LOGIN_URL)
def share_document_view(request, document_id):
	"""Share document in Shared Documents folder.
	"""
	
	document = Document.objects.get(pk=document_id)
	document.is_shared = True
	document.save()

	return HttpResponseRedirect(
		reverse(
			'folder_view',
			args=(document.parent_folder.uuid,),
		)
	)


@login_required(login_url=LOGIN_URL)
def unshare_document_view(request, document_id):
	"""Unshare document in Shared Documents folder.
	"""

	document = Document.objects.get(pk=document_id)
	document.is_shared = False
	document.save()

	return HttpResponseRedirect(
		reverse(
			'folder_view',
			args=(document.parent_folder.uuid,),
		)
	)


@login_required(login_url=LOGIN_URL)
def create_document_view(request, folder_id):
	"""Create a document in a folder.
	"""
	
	if request.method == "POST":
		
		create_document_form = CreateDocumentForm(
			request.POST, 
			request.FILES, 
			parent_folder_id=folder_id
		)
		
		if create_document_form.is_valid():
			
			document_api = DocumentApi(request.user)
			document_api.create_document(
				create_document_form.cleaned_data['name'],
				Folder.objects.get(pk=folder_id),
				create_document_form.cleaned_data['content_file'],
				request.user,
				title=create_document_form.cleaned_data['title'],
				description=create_document_form.cleaned_data['description'],
			)
			
			return HttpResponseRedirect(
				reverse(
					'folder_view',
					kwargs={
						'folder_id': folder_id,
					}
				)
			)

	elif request.method == "GET":
		create_document_form = CreateDocumentForm(parent_folder_id=folder_id)

	view_context = {}
	
	folder_view_context = FolderViewContext(
		request.user,
		folder_id,
		request.session,
	)
	
	view_context.update(
		folder_view_context.get_context(inc_folder_list=False)
	)
	
	view_context.update(
		{'create_document_form': create_document_form},
	)
	
	return render(
		request,
		'web/create_document.html',
		view_context,
	)

@login_required(login_url=LOGIN_URL)
def copy_document_view(request, document_id):
	"""Copy document and place in another folder.
	"""
	
	document = Document.objects.get(pk=document_id)
	
	request.session['uuid'] = document_id
	request.session['action'] = 'copy'
	request.session['object'] = 'document'

	return HttpResponseRedirect(
		reverse(
			'folder_view',
			args=(document.parent_folder.uuid,),
		)
	)


@login_required(login_url=LOGIN_URL)
def edit_document_view(request, document_id):
	"""View to edit document properties.
	"""
	
	document = Document.objects.get(pk=document_id)

	if request.method == "POST":

		edit_document_form = EditDocumentForm(
			request.POST
		)
		
		if edit_document_form.is_valid():
			
			document_api = DocumentApi(request.user)
			document_api.update_document(
				document,
				name=edit_document_form.cleaned_data['name'],
				title=edit_document_form.cleaned_data['title'],
				description=edit_document_form.cleaned_data['description'],
			)

			return HttpResponseRedirect(
				reverse(
					'folder_view',
					kwargs={
						'folder_id': document.parent_folder.uuid,
					}
				)
			)

	elif request.method == "GET":
		
		edit_document_form = EditDocumentForm(
			instance=document,
		)

	view_context = {}
	
	folder_view_context = FolderViewContext(
		request.user,
		document.parent_folder.uuid,
		request.session,
	)
	
	view_context.update(
		folder_view_context.get_context(inc_folder_list=False)
	)

	view_context.update(
		{
			'edit_document_form': edit_document_form,
			'document': document,
		},
	)

	return render(
		request,
		'web/edit_document.html',
		view_context,
	)


@login_required(login_url=LOGIN_URL)
def move_document_view(request, document_id):
	"""Move document to other folder.
	"""
	
	document = Document.objects.get(pk=document_id)
	
	request.session['uuid'] = document_id
	request.session['action'] = 'move'
	request.session['object'] = 'document'

	return HttpResponseRedirect(
		reverse(
			'folder_view',
			args=(document.parent_folder.uuid,),
		)
	)


@login_required(login_url=LOGIN_URL)
def restore_document_view(request, document_id):
	"""Restore document from trashcan. Works essentially the same as the move_document_view.
	"""
	
	document = Document.objects.get(pk=document_id)
	
	request.session['uuid'] = document_id
	request.session['action'] = 'move'
	request.session['object'] = 'document'

	return HttpResponseRedirect(
		reverse(
			'folder_view',
			args=(document.parent_folder.uuid,),
		)
	)


@login_required(login_url=LOGIN_URL)
def delete_document_view(request, document_id):
	"""Places document in the user's trashcan folder but does not permanently delete it.
	"""

	document = Document.objects.get(pk=document_id)
	parent_folder_id = document.parent_folder.uuid
	document_api = DocumentApi(request.user)
	document_api.delete_document(document)
	
	return HttpResponseRedirect(
		reverse(
			'folder_view',
			kwargs={
				'folder_id': parent_folder_id,
			}
		)
	)


@login_required(login_url=LOGIN_URL)
def force_delete_document_view(request, document_id):
	"""Permanently delete a document.
	"""

	document = Document.objects.get(pk=document_id)
	
	document_auth = DocumentAuthorization(
		request.user,
		document.parent_folder,
		document,
	)

	if document_auth.is_authorized_delete():
		document.delete()

	return HttpResponseRedirect(
		reverse(
			'folder_view',
			args=(document.parent_folder.uuid,),
		)
	)
