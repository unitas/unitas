"""Web views that are folder-related.
"""

from apps.web.lib.view import login_required, LOGIN_URL, FolderViewContext, render, CreateFolderForm, FolderApi, Folder, HttpResponseRedirect, FolderService, reverse, EditFolderForm, FolderAuthorization, Document, DocumentAuthorization, DocumentService, DocumentApi, Project


@login_required(login_url=LOGIN_URL)
def folder_view(request, folder_id):
	"""Folder and document listing view.
	"""

	is_in_shared_docs_folder = False
	folder = Folder.objects.get(pk=folder_id)

	if folder == FolderService().get_projects_folder():
		return HttpResponseRedirect(
			reverse(
				'projects_home_view',
			)
		)

	if folder == FolderService().get_shared_documents_folder():
		is_in_shared_docs_folder = True

	folder_auth = FolderAuthorization(request.user, folder)

	if folder_auth.is_authorized_read() :

		view_context = {}
		folder_view_context = FolderViewContext(
			request.user, 
			folder_id,
			request.session,
			is_in_shared_docs_folder,
			request.GET
		)
		view_context.update(folder_view_context.get_context())

		return render(
			request,
			'web/folder.html',
			view_context,
		)

	else:

		return HttpResponseRedirect(
			reverse(
				'redirect_to_user_home_folder',
			)
		)


@login_required(login_url=LOGIN_URL)
def folder_properties_view(request, folder_id):
	"""View folder's properties and details.
	"""

	view_context = {}
	
	folder_view_context = FolderViewContext(
		request.user,
		folder_id,
		request.session
	)
	
	view_context.update(
		folder_view_context.get_context(inc_folder_list=False)
	)
	
	return render(
		request,
		'web/folder_properties.html',
		view_context,
	)


@login_required(login_url=LOGIN_URL)
def trashcan_folder_view(request):
	"""View for the user's trashcan folder.
	"""

	folder_service = FolderService()
	folder_service.set_user(request.user)
	user_trashcan_folder = folder_service.get_user_trashcan_folder()

	view_context = {}
	
	folder_view_context = FolderViewContext(
		request.user, 
		user_trashcan_folder.uuid,
		request.session,
	)
	
	view_context.update(folder_view_context.get_context())

	return render(
		request,
		'web/trashcan.html',
		view_context,
	)


@login_required(login_url=LOGIN_URL)
def create_folder_view(request, folder_id):
	"""Create a folder in a user's home folder.
	"""
	if request.method == "POST":
		
		create_folder_form = CreateFolderForm(
			request.POST, parent_folder_id=folder_id
		)
		
		if create_folder_form.is_valid():
			
			folder_api = FolderApi(request.user)
			folder_api.create_folder(
				create_folder_form.cleaned_data['name'],
				Folder.objects.get(pk=folder_id),
				request.user,
				title=create_folder_form.cleaned_data['title'],
				description=create_folder_form.cleaned_data['description'],
			)

			return HttpResponseRedirect(
				reverse(
					'folder_view',
					kwargs={
						'folder_id': folder_id,
					}
				)
			)

	elif request.method == "GET":
		
		create_folder_form = CreateFolderForm(parent_folder_id=folder_id)

	view_context = {}
	
	folder_view_context = FolderViewContext(
		request.user,
		folder_id,
		request.session,
	)
	
	view_context.update(
		folder_view_context.get_context(inc_folder_list=False)
	)
	
	view_context.update(
		{'create_folder_form': create_folder_form},
	)
	
	return render(
		request,
		'web/create_folder.html',
		view_context,
	)


@login_required(login_url=LOGIN_URL)
def copy_folder_view(request, folder_id):
	"""Copy folder and contents to another parent folder.
	"""

	folder = Folder.objects.get(pk=folder_id)
	
	parent_folder = folder.parent_folder
	
	request.session['uuid'] = folder_id
	request.session['action'] = 'copy'
	request.session['object'] = 'folder'

	return HttpResponseRedirect(
		reverse(
			'folder_view',
			args=(parent_folder.uuid,),
		)
	)

@login_required(login_url=LOGIN_URL)
def edit_folder_view(request, folder_id):
	"""Edit folder's properties.
	"""

	folder = Folder.objects.get(pk=folder_id)

	folder_auth = FolderAuthorization(request.user, folder)

	if not folder_auth.is_authorized_update() or folder.is_system:
		
		return HttpResponseRedirect(
			reverse(
				'folder_view',
				kwargs={
					'folder_id': folder.parent_folder.uuid,
				}
			)
		)

	if request.method == "POST":

		edit_folder_form = EditFolderForm(
			request.POST, parent_folder_id=folder_id
		)
		
		if edit_folder_form.is_valid():
			
			folder_api = FolderApi(request.user)
			folder_api.update_folder(
				folder,
				name=edit_folder_form.cleaned_data['name'],
				title=edit_folder_form.cleaned_data['title'],
				description=edit_folder_form.cleaned_data['description'],
			)

			return HttpResponseRedirect(
				reverse(
					'folder_view',
					kwargs={
						'folder_id': folder.parent_folder.uuid,
					}
				)
			)

	elif request.method == "GET":
		
		edit_folder_form = EditFolderForm(
			instance=folder,
			parent_folder_id=folder_id,
		)

	view_context = {}
	
	folder_view_context = FolderViewContext(
		request.user,
		folder_id,
		request.session,
	)
	
	view_context.update(
		folder_view_context.get_context(inc_folder_list=False)
	)
	
	view_context.update(
		{'edit_folder_form': edit_folder_form},
	)
	
	return render(
		request,
		'web/edit_folder.html',
		view_context,
	)

@login_required(login_url=LOGIN_URL)
def move_folder_view(request, folder_id):
	"""Move folder to another parent folder.
	"""

	folder = Folder.objects.get(pk=folder_id)
	
	parent_folder = folder.parent_folder
	
	request.session['uuid'] = folder_id
	request.session['action'] = 'move'
	request.session['object'] = 'folder'

	return HttpResponseRedirect(
		reverse(
			'folder_view',
			args=(parent_folder.uuid,),
		)
	)


@login_required(login_url=LOGIN_URL)
def restore_folder_view(request, folder_id):
	"""Restore folder from user trashcan.
	"""

	folder = Folder.objects.get(pk=folder_id)
	
	parent_folder = folder.parent_folder
	
	request.session['uuid'] = folder_id
	request.session['action'] = 'move'
	request.session['object'] = 'folder'

	return HttpResponseRedirect(
		reverse(
			'folder_view',
			args=(parent_folder.uuid,),
		)
	)

@login_required(login_url=LOGIN_URL)
def paste_folder_view(request, folder_id):
	"""Place folder/document in clipboard to another parent folder.
	"""

	parent_folder = Folder.objects.get(pk=folder_id)

	action = request.session.get('action')
	obj = request.session.get('object')
	uuid = request.session.get('uuid')

	if obj == 'document':
		
		document = Document.objects.get(pk=uuid)
		
		document_auth = DocumentAuthorization(
			request.user,
			parent_folder,
		)
		
		if document_auth.is_authorized_creation():
			
			if action == 'move':
				
				document.parent_folder = parent_folder
				document.save()
			
			elif action == 'copy':
				
				'''
				document_service = DocumentService()
				document_service.set_document(document)
				document_service.copy(
					parent_folder
				)
				'''
				document_api = DocumentApi(request.user)
				document_api.copy_document(document, parent_folder)
		
		else:
			
			print 'Not authorized'

	elif obj == 'folder':
		
		folder = Folder.objects.get(pk=uuid)
		
		folder_auth = FolderAuthorization(
			request.user,
			parent_folder,
		)
		
		if folder_auth.is_authorized_creation():
			
			if action == 'move':
				
				folder.parent_folder = parent_folder
				folder.save()
			
			elif action == 'copy':
				
				'''
				folder_service = FolderService()
				folder_service.set_folder(folder)
				folder_service.copy(
					parent_folder
				)
				'''
				folder_api = FolderApi(request.user)
				folder_api.copy_folder(folder, parent_folder)
		
		else:
			
			print 'Not authorized'

	if request.session.get('action'):
		
		del request.session['action']
		del request.session['object']
		del request.session['uuid']

	return HttpResponseRedirect(
		reverse(
			'folder_view',
			args=(folder_id,),
		)
	)


@login_required(login_url=LOGIN_URL)
def delete_folder_view(request, folder_id):
	"""Places folder in the user trashcan folder.
	"""

	folder = Folder.objects.get(pk=folder_id)

	if folder.is_system:
		
		return HttpResponseRedirect(
			reverse('redirect_to_user_home_folder')
		)

	parent_folder_id = folder.parent_folder.uuid
	
	folder_api = FolderApi(request.user)
	folder_api.delete_folder(folder)
	
	return HttpResponseRedirect(
		reverse(
			'folder_view',
			kwargs={
				'folder_id': parent_folder_id,
			}
		)
	)


@login_required(login_url=LOGIN_URL)
def force_delete_folder_view(request, folder_id):
	"""Permanently deletes a folder and its content.
	"""

	folder = Folder.objects.get(pk=folder_id)

	if folder.is_system:
		
		return HttpResponseRedirect(
			reverse('redirect_to_user_home_folder')
		)
	
	folder_auth = FolderAuthorization(
		request.user,
		folder,
	)

	if folder_auth.is_authorized_delete():
		folder.delete()

	return HttpResponseRedirect(
		reverse(
			'folder_view',
			args=(folder.parent_folder.uuid,),
		)
	)


@login_required(login_url=LOGIN_URL)
def empty_trashcan_folder_view(request):

	folder_service = FolderService()
	folder_service.set_user(request.user)
	trashcan_folder = folder_service.get_user_trashcan_folder()
	folder_service.set_folder(trashcan_folder)

	folder_children = folder_service.get_children()

	for document in folder_children['documents']:
		document.delete()

	for folder in folder_children['folders']:
		folder.delete()

	return HttpResponseRedirect(
		reverse(
			'folder_view',
			args=(trashcan_folder.uuid,),
		)
	)
