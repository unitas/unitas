"""Unit tests for ProjectApi class.
"""

from apps.web.tests import UnitasTestCase, ProjectApi, UnauthorizedActionException, ProjectService, Project, UserService

sample_file = 'scripts/data/samples/Test Document.txt'

class ProjectApiTest(UnitasTestCase):

	def setUp(self):
		self.admin_user = UserService().get_admin_user()

	def get_project(self, name='Project1', key='PROJ1', ptype='Public', owner=None):
		if not owner:
			owner = self.admin_user

		""" name, key, ptype, owner """
		project_service = ProjectService()
		project = project_service.create_project(
			name=name, 
			key=key, 
			ptype=ptype, 
			owner=owner,
		)
		return project

	def testInit(self):
		project = self.get_project()

		project_api = ProjectApi(self.admin_user, project)

		self.assertTrue(project_api)
		self.assertEqual(project_api.get_project(), project)
		self.assertEqual(project_api.get_user(), self.admin_user)
		self.assertEqual(project_api.get_parent_folder(), project.folder)

	def testCreateProject(self):
		"""Test create_project().

		All users except system users (nobody, system, everybody) should be able to create projects.
		"""

		# Admin user, super user, non priv user
		i = 1
		for user in (self.admin_user, self.get_super_user(), self.get_non_priv_user()):
			project_api = ProjectApi(user)
			project_api.create_project(
				name='Project{}'.format(i),
				key='PROJ{}'.format(i),
				ptype='Public',
				owner=user,
				title='Project #{}'.format(i),
				description='This is the space for Project #{}'.format(i),
			)
			self.assertTrue(
				Project.objects.get(
					name='Project{}'.format(i), key='PROJ{}'.format(i)))
			i += 1

		# nobody, system, everybody
		i = 1
		for user in (
			UserService().get_nobody_user(), UserService().get_system_user(), UserService().get_everybody_user()):
			project_api = ProjectApi(user)
			with self.assertRaises(UnauthorizedActionException):
				project_api.create_project(
					name='Project{}'.format(i),
					key='PROJ{}'.format(i),
					ptype='Public',
					owner=user,
					title='Project #{}'.format(i),
					description='This is the space for Project #{}'.format(i),
				)
			i += 1

			
	def testGetProject(self):
		"""Test get_project().

		Admin user should be able to read any project (public, private or open).
		Super user should be able to read any project (public, private or open).
		Project owner should be able to read project.
		Project members should be able to read project.
		Open projects should be read by non-members.
		Public projects should be read by non-members.
		"""

		non_priv_user = self.get_non_priv_user(username='testuser1')
		non_priv_user2 = self.get_non_priv_user(username='testuser2')
		non_memberUser = self.get_non_priv_user(username='nonmember1')
		super_user = self.get_super_user()

		project_service = ProjectService()

		public_project = ProjectService().create_project(
			'PublicProject1',
			'PUBPROJ1',
			'Public',
			non_priv_user,
		)
		project_service.set_project(public_project)
		project_service.add_member(non_priv_user2, 'user')

		private_project = ProjectService().create_project(
			'PrivateProject1',
			'PRVPROJ1',
			'Private',
			non_priv_user,
		)
		project_service.set_project(private_project)
		project_service.add_member(non_priv_user2, 'user')

		open_project = ProjectService().create_project(
			'OpenProject1',
			'OPNPROJ1',
			'Open',
			non_priv_user,
		)
		project_service.set_project(open_project)
		project_service.add_member(non_priv_user2, 'user')

		allProjects = (public_project, private_project, open_project,)

		# Admin user
		for project in allProjects:
			project_api = ProjectApi(self.admin_user, project)
			self.assertEqual(project_api.get_project(), project)

		# Super user
		for project in allProjects:
			project_api = ProjectApi(super_user, project)
			self.assertEqual(project_api.get_project(), project)

		# Project owner
		for project in allProjects:
			project_api = ProjectApi(non_priv_user, project)
			self.assertEqual(project_api.get_project(), project)

		# Project members
		for project in allProjects:
			project_service = ProjectService(project)
			members = project_service.get_members()
			for member in members:
				project_api = ProjectApi(member, project)
				self.assertEqual(project_api.get_project(), project)

		# Non-members open projects
		project_api = ProjectApi(non_memberUser, open_project)
		self.assertEqual(project_api.get_project(), open_project)

		# Non-members public projects
		project_api = ProjectApi(non_memberUser, public_project)
		self.assertEqual(project_api.get_project(), public_project)

		# Non-members private projects
		project_api = ProjectApi(non_memberUser, private_project)
		with self.assertRaises(UnauthorizedActionException):
			project_api.get_project()

	def testUpdateProject(self):
		"""Test update_project().

		Admin user can update any project.
		Project owner can update own project.
		Project managers can update project.
		"""
		project_owner = self.get_non_priv_user('projectowner1')
		project_manager1 = self.get_non_priv_user('projectmgr1')
		super_user = self.get_super_user()
		non_priv_user1 = self.get_non_priv_user('nonprivuser1')
		contributor_user = self.get_non_priv_user('contributor1')
		non_priv_user2 = self.get_non_priv_user('nonprivuser2')

		project_service = ProjectService()
		project = project_service.create_project(
			'PublicProject1',
			'PUBPROJ1',
			'Public',
			project_owner,
		)
		project_service.add_member(project_manager1, 'manager')
		project_service.add_member(contributor_user, 'contributor')
		project_service.add_member(non_priv_user2, 'user')

		# Admin user
		project_api = ProjectApi(self.admin_user, project)
		self.assertTrue(project_api.update_project(title='Public Project #1'))

		# Project owner
		project_api = ProjectApi(project_owner, project)
		self.assertTrue(project_api.update_project(title='Public Project #1'))

		# Project manager members
		project_api = ProjectApi(project_manager1, project)
		self.assertTrue(project_api.update_project(title='Public Project #1'))
		
		# Super user
		project_api = ProjectApi(super_user, project)
		with self.assertRaises(UnauthorizedActionException):
			project_api.update_project(title='Public Project #1')

		# Non priv user
		project_api = ProjectApi(non_priv_user1, project)
		with self.assertRaises(UnauthorizedActionException):
			project_api.update_project(title='Public Project #1')

		# Project contributor members
		project_api = ProjectApi(contributor_user, project)
		with self.assertRaises(UnauthorizedActionException):
			project_api.update_project(title='Public Project #1')

		# Project user members
		project_api = ProjectApi(non_priv_user2, project)
		with self.assertRaises(UnauthorizedActionException):
			project_api.update_project(title='Public Project #1')

	def create_project(self, project_owner, project_manager1, contributor_user, non_priv_user2):
		project_service = ProjectService()
		project = project_service.create_project(
			'PublicProject1',
			'PUBPROJ1',
			'Public',
			project_owner,
		)
		project_service.add_member(project_manager1, 'manager')
		project_service.add_member(contributor_user, 'contributor')
		project_service.add_member(non_priv_user2, 'user')
		return project


	def testDeleteProject(self):
		"""Test delete_project().

		Only project owners and admin user can delete a project.
		"""

		project_owner = self.get_non_priv_user('projectowner1')
		project_manager1 = self.get_non_priv_user('projectmgr1')
		super_user = self.get_super_user()
		non_priv_user1 = self.get_non_priv_user('nonprivuser1')
		contributor_user = self.get_non_priv_user('contributor1')
		non_priv_user2 = self.get_non_priv_user('nonprivuser2')

		project_service = ProjectService()
		project = project_service.create_project(
			'PublicProject1',
			'PUBPROJ1',
			'Public',
			project_owner,
		)
		project_service.add_member(project_manager1, 'manager')
		project_service.add_member(contributor_user, 'contributor')
		project_service.add_member(non_priv_user2, 'user')

		# Admin user
		project_api = ProjectApi(self.admin_user, project)
		self.assertTrue(project_api.delete_project())

		project = self.create_project(project_owner, project_manager1, contributor_user, non_priv_user2)
		# Project owner
		project_api = ProjectApi(project_owner, project)
		self.assertTrue(project_api.delete_project())


		project = self.create_project(project_owner, project_manager1, contributor_user, non_priv_user2)
		# Project manager members
		project_api = ProjectApi(project_manager1, project)
		with self.assertRaises(UnauthorizedActionException):
			project_api.delete_project()

		# Super user
		project_api = ProjectApi(super_user, project)
		with self.assertRaises(UnauthorizedActionException):
			project_api.delete_project()

		# Non priv user
		project_api = ProjectApi(non_priv_user1, project)
		with self.assertRaises(UnauthorizedActionException):
			project_api.delete_project()

		# Project contributor members
		project_api = ProjectApi(contributor_user, project)
		with self.assertRaises(UnauthorizedActionException):
			project_api.delete_project()

		# Project user members
		project_api = ProjectApi(non_priv_user2, project)
		with self.assertRaises(UnauthorizedActionException):
			project_api.delete_project()

	def testCreateProjectFolder(self):
		"""Test create_project_folder().

		System admin user can create a folder in any project.
		Project owner can create a folder in the project.
		Project manager can create a folder in the project.
		Project contributor can create a folder in the project.

		Super user cannot create a folder in projects where he is not a member.
		Non priv users cannot create a folder in projects where he is not a member.
		Project users cannot create a folder in projects where he is a member.
		"""

		project_owner = self.get_non_priv_user(username='projectowner1')
		project_manager = self.get_non_priv_user(username='projectmgr1')
		project_contrib = self.get_non_priv_user(username='projcontrib1')
		super_user = self.get_super_user(username='superuser1')
		non_priv_user = self.get_non_priv_user(username='nonmember1')
		project_user = self.get_non_priv_user(username='projectuser1')

		project_service = ProjectService()
		project1 = project_service.create_project(
			'Project1',
			'PROJ1',
			'Public',
			project_owner,
		)

		project_service.add_member(project_manager, 'manager')
		project_service.add_member(project_contrib, 'contributor')
		project_service.add_member(project_user, 'user')

		project_api = ProjectApi(self.admin_user, project1)
		project_folder = project_service.get_project_home_folder()

		# Admin user
		self.assertTrue(
			project_api.create_project_folder(
				'Folder1',
				project_folder,
				self.admin_user
			)
		)
		# Project owner
		project_api.set_user(project_owner)
		self.assertTrue(
			project_api.create_project_folder(
				'Folder2',
				project_folder,
				project_owner
			)
		)
		# Project manager
		project_api.set_user(project_manager)
		self.assertTrue(
			project_api.create_project_folder(
				'Folder3',
				project_folder,
				project_manager
			)
		)

		# Project contributor
		project_api.set_user(project_contrib)
		self.assertTrue(
			project_api.create_project_folder(
				'Folder4',
				project_folder,
				project_contrib
			)
		)

		# Super user
		project_api.set_user(super_user)
		with self.assertRaises(UnauthorizedActionException):
			project_api.create_project_folder(
				'Folder5',
				project_folder,
				super_user
			)
		
		# Non priv user
		project_api.set_user(non_priv_user)
		with self.assertRaises(UnauthorizedActionException):
			project_api.create_project_folder(
				'Folder6',
				project_folder,
				non_priv_user
			)

		# Project users
		project_api.set_user(project_user)
		with self.assertRaises(UnauthorizedActionException):
			project_api.create_project_folder(
				'Folder7',
				project_folder,
				project_user
			)

	def testReadProjectFolder(self):
		"""Test create_project_folder().

		Admin user can read any folder details.
		A folder owner cannot read his own folder details in a project where he is not a member.
		Any project member can his own folder details.
		Non members (inc. super user) cannot read project folder details.
		"""

		project_owner = self.get_non_priv_user(username='projectowner')
		folder_owner = self.get_non_priv_user(username='folderowner')
		project_member = self.get_non_priv_user(username='projectmember')
		non_project_member = self.get_non_priv_user(username='nonmember')

		project_service = ProjectService()

		project = project_service.create_project(
			'Project1',
			'PROJ1',
			'Public',
			project_owner,
		)

		project_service.add_member(project_member, 'user')

		# Admin
		project_api = ProjectApi(self.admin_user, project)

		project_folder = project_api.create_project_folder(
			'Folder1',
			project_service.get_project_home_folder(),
			folder_owner,
		)

		self.assertTrue(project_api.get_project_folder(project_folder))
		
		# Folder owner
		project_api.set_user(folder_owner)
		with self.assertRaises(UnauthorizedActionException):
			project_api.get_project_folder(project_folder)

		# Project member
		project_api.set_user(project_member)
		self.assertTrue(project_api.get_project_folder(project_folder))

		# Non members
		project_api.set_user(non_project_member)
		with self.assertRaises(UnauthorizedActionException):
			project_api.get_project_folder(project_folder)

	def testUpdateProjectFolder(self):
		"""Test update_project_folder().

		Admin user can update any folder.
		Project owner can update any folder in his project.
		A folder owner cannot update a folder he owns if the folder is in a project that he is not a member of.
		Project owners and managers can update folder details.
		"""

		project_owner = self.get_non_priv_user(username='projowner')
		project_manager = self.get_non_priv_user(username='projmgr')
		project_contrib = self.get_non_priv_user(username='projcontrib')
		project_user = self.get_non_priv_user(username='projuser')
		non_member = self.get_non_priv_user(username='nonmember')
		folder_owner = self.get_non_priv_user(username='folderowner')

		project_service = ProjectService()
		project = project_service.create_project(
			'Project1',
			'PROJ1',
			'Public',
			project_owner,
		)

		project_service.add_member(project_manager, 'manager')
		project_service.add_member(project_contrib, 'contributor')
		project_service.add_member(project_user, 'user')

		project_home_folder = project_service.get_project_home_folder()

		po_folder = project_service.create_folder(
			'Project Owner Folder',
			project_home_folder,
			project_owner,
		)

		pc_folder = project_service.create_folder(
			'Project Contributor Folder',
			project_home_folder,
			project_contrib,
		)

		non_member_folder = project_service.create_folder(
			'Non-member Folder',
			project_home_folder,
			non_member,
		)

		project_api = ProjectApi(self.admin_user, project)

		# Admin
		for folder in (po_folder, pc_folder, non_member_folder):
			self.assertTrue(project_api.update_project_folder(
				folder, title='My folder'))

		# Project owner
		project_api.set_user(project_owner)
		for folder in (po_folder, pc_folder, non_member_folder):
			self.assertTrue(project_api.update_project_folder(
				folder, title='My folder'))

		# Project manager
		project_api.set_user(project_manager)
		for folder in (po_folder, pc_folder, non_member_folder):
			self.assertTrue(project_api.update_project_folder(
				folder, title='My folder'))

		# Project contributors can update only folders they own.
		project_api.set_user(project_contrib)
		self.assertTrue(project_api.update_project_folder(
			pc_folder, title='My folder'))

		project_api.set_user(project_contrib)
		with self.assertRaises(UnauthorizedActionException):
			project_api.update_project_folder(
				po_folder, title='My folder'
			)

		with self.assertRaises(UnauthorizedActionException):
			project_api.update_project_folder(
				non_member_folder, title='My folder'
			)

		### CANNOT ###

		# Project users cannot update any folders.
		project_api.set_user(project_user)
		with self.assertRaises(UnauthorizedActionException):
			project_api.update_project_folder(
				po_folder, title='My folder'
			)

		with self.assertRaises(UnauthorizedActionException):
			project_api.update_project_folder(
				non_member_folder, title='My folder'
			)

		with self.assertRaises(UnauthorizedActionException):
			project_api.update_project_folder(
				pc_folder, title='My folder'
			)

		# Folder owner non-member
		project_api.set_user(non_member)
		with self.assertRaises(UnauthorizedActionException):
			project_api.update_project_folder(
				non_member_folder, title='My folder'
			)

		# Non-members
		with self.assertRaises(UnauthorizedActionException):
			project_api.update_project_folder(
				pc_folder, title='My folder'
			)

		with self.assertRaises(UnauthorizedActionException):
			project_api.update_project_folder(
				po_folder, title='My folder'
			)

	def testDeleteProjectFolder(self):
		"""Test delete_project_folder().
		
		Admin user can delete any folder in any project.
		Project owners, managers and can delete any folder in their own project.
		Project members can delete their own folders.
		Non members cannot delete any folders.

		"""

		project_owner = self.get_non_priv_user(username='projowner')
		project_manager = self.get_non_priv_user(username='projmgr')
		project_contrib = self.get_non_priv_user(username='projcontrib')
		non_member = self.get_non_priv_user(username='nonmember')

		project_service = ProjectService()
		project = project_service.create_project(
			'Project1',
			'PROJ1',
			'Public',
			project_owner,
		)

		project_service.add_member(project_manager, 'manager')
		project_service.add_member(project_contrib, 'contributor')

		project_home_folder = project_service.get_project_home_folder()

		pc_folder = project_service.create_folder(
			'Project Contributor Folder',
			project_home_folder,
			project_contrib,
		)

		project_api = ProjectApi(self.admin_user, project)

		# Admin
		self.assertTrue(
			project_api.delete_project_folder(pc_folder)
		)

		# Project owner
		pc_folder = project_service.create_folder(
			'Project Contributor Folder',
			project_home_folder,
			project_contrib,
		)

		project_api.set_user(project_owner)
		self.assertTrue(
			project_api.delete_project_folder(pc_folder)
		)

		# Project manager
		pc_folder = project_service.create_folder(
			'Project Contributor Folder',
			project_home_folder,
			project_contrib,
		)

		project_api.set_user(project_manager)
		self.assertTrue(
			project_api.delete_project_folder(pc_folder)
		)

		# Project contributor
		pc_folder = project_service.create_folder(
			'Project Contributor Folder',
			project_home_folder,
			project_contrib,
		)

		project_api.set_user(project_contrib)
		self.assertTrue(
			project_api.delete_project_folder(pc_folder)
		)

		### CANNOT ###

		# Non members

		pc_folder = project_service.create_folder(
			'Project Contributor Folder',
			project_home_folder,
			project_contrib,
		)

		project_api.set_user(non_member)
		with self.assertRaises(UnauthorizedActionException):
			project_api.delete_project_folder(pc_folder)

	def testCreateProjectDocument(self):
		"""Test create_project_document().
		
		Project owners, managers and contributors can create documents in a project.
		Admin users cannot create a document in a project.
		Project users cannot create a document in a project.
		Non members cannot create a document in a project.
		"""

		project_owner = self.get_non_priv_user(username='projowner')
		project_manager = self.get_non_priv_user(username='projmgr')
		project_contrib = self.get_non_priv_user(username='projcontrib')
		project_user = self.get_non_priv_user(username='projuser')
		non_member = self.get_non_priv_user(username='nonmember')

		project_service = ProjectService()
		project = project_service.create_project(
			'Project1',
			'PROJ1',
			'Public',
			project_owner,
		)

		project_service.add_member(project_manager, 'manager')
		project_service.add_member(project_contrib, 'contributor')
		project_service.add_member(project_user, 'user')

		project_home_folder = project_service.get_project_home_folder()
		project_api = ProjectApi(project_owner, project)

		# Project owner
		document = project_api.create_project_document(
			'Test1.txt',
			project_home_folder,
			sample_file,
			project_owner,
		)
		self.assertTrue(document.name, 'Test1.txt')

		# Project manager
		project_api.set_user(project_manager)
		document = project_api.create_project_document(
			'Test2.txt',
			project_home_folder,
			sample_file,
			project_manager,
		)
		self.assertTrue(document.name, 'Test2.txt')

		# Project contributor
		project_api.set_user(project_contrib)
		document = project_api.create_project_document(
			'Test3.txt',
			project_home_folder,
			sample_file,
			project_contrib,
		)
		self.assertTrue(document.name, 'Test3.txt')

		# Admin
		project_api.set_user(self.admin_user)
		with self.assertRaises(UnauthorizedActionException):
			document = project_api.create_project_document(
				'Test4.txt',
				project_home_folder,
				sample_file,
				self.admin_user,
			)

		# Project users
		project_api.set_user(project_user)
		with self.assertRaises(UnauthorizedActionException):
			document = project_api.create_project_document(
				'Test5.txt',
				project_home_folder,
				sample_file,
				project_user,
			)

		# Non-members
		project_api.set_user(non_member)
		with self.assertRaises(UnauthorizedActionException):
			document = project_api.create_project_document(
				'Test6.txt',
				project_home_folder,
				sample_file,
				non_member,
			)

	def testGetProjectDocument(self):
		"""Test get_project_document()

		Admin users and project members can view any document in a project.
		Non members cannot.
		"""

		project_owner = self.get_non_priv_user(username='projowner')
		project_manager = self.get_non_priv_user(username='projmgr')
		project_contrib = self.get_non_priv_user(username='projcontrib')
		project_user = self.get_non_priv_user(username='projuser')
		non_member = self.get_non_priv_user(username='nonmember')

		project_service = ProjectService()
		project = project_service.create_project(
			'Project1',
			'PROJ1',
			'Public',
			project_owner,
		)

		project_service.add_member(project_manager, 'manager')
		project_service.add_member(project_contrib, 'contributor')
		project_service.add_member(project_user, 'user')

		project_home_folder = project_service.get_project_home_folder()
		project_api = ProjectApi(project_owner, project)

		# Project owner
		document = project_api.create_project_document(
			'Test1.txt',
			project_home_folder,
			sample_file,
			project_owner,
		)

		# Admin user
		project_api.set_user(self.admin_user)
		self.assertTrue(project_api.get_project_document(document))

		# Project members
		for user in project_service.get_members():
			project_api.set_user(user)
			self.assertTrue(project_api.get_project_document(document))

		# Non members
		project_api.set_user(non_member)
		with self.assertRaises(UnauthorizedActionException):
			project_api.get_project_document(document)

	def testUpdateProjectDocument(self):
		"""Test update_project_document()

		Project owner, project manager can update any document in the project.
		Project contributor can only update his own document.
		Project user cannot update a document.
		Admin user cannot update a document.
		Non members cannot update a document.
		"""

		project_owner = self.get_non_priv_user(username='projowner')
		project_manager = self.get_non_priv_user(username='projmgr')
		project_contrib = self.get_non_priv_user(username='projcontrib')
		project_user = self.get_non_priv_user(username='projuser')
		non_member = self.get_non_priv_user(username='nonmember')

		project_service = ProjectService()
		project = project_service.create_project(
			'Project1',
			'PROJ1',
			'Public',
			project_owner,
		)

		project_service.add_member(project_manager, 'manager')
		project_service.add_member(project_contrib, 'contributor')
		project_service.add_member(project_user, 'user')

		project_home_folder = project_service.get_project_home_folder()
		project_api = ProjectApi(project_owner, project)

		# Project owner
		document = project_api.create_project_document(
			'Test1.txt',
			project_home_folder,
			sample_file,
			project_contrib,
		)

		# Project owner
		self.assertTrue(
			project_api.update_project_document(
				document, title='My document',
			)
		)

		# Project manager
		project_api.set_user(project_manager)
		self.assertTrue(
			project_api.update_project_document(
				document, title='My document',
			)
		)

		# Project contributor
		project_api.set_user(project_contrib)
		self.assertTrue(
			project_api.update_project_document(
				document, title='My document',
			)
		)

		# Project user
		project_api.set_user(project_user)
		with self.assertRaises(UnauthorizedActionException):
			project_api.update_project_document(
				document, title='My document',
			)

		# Admin user
		project_api.set_user(self.admin_user)
		with self.assertRaises(UnauthorizedActionException):
			project_api.update_project_document(
				document, title='My document',
			)

		# Non member
		project_api.set_user(project_user)
		with self.assertRaises(UnauthorizedActionException):
			project_api.update_project_document(
				document, title='My document',
			)

	def testDeleteProjectDocument(self):
		"""Test delete_project_document()

		Admin user can delete any document in a project.
		Project owner and manager can delete any document in a project.
		Project contributor can delete his own documents.
		Project user cannot delete any documents in a project.
		Non-members cannot delete any documents.
		"""

		project_owner = self.get_non_priv_user(username='projowner')
		project_manager = self.get_non_priv_user(username='projmgr')
		project_contrib = self.get_non_priv_user(username='projcontrib')
		project_user = self.get_non_priv_user(username='projuser')
		non_member = self.get_non_priv_user(username='nonmember')

		project_service = ProjectService()
		project = project_service.create_project(
			'Project1',
			'PROJ1',
			'Public',
			project_owner,
		)

		project_service.add_member(project_manager, 'manager')
		project_service.add_member(project_contrib, 'contributor')
		project_service.add_member(project_user, 'user')

		project_home_folder = project_service.get_project_home_folder()
		project_api = ProjectApi(project_owner, project)

		# Project owner
		document = project_api.create_project_document(
			'Test1.txt',
			project_home_folder,
			sample_file,
			project_contrib,
		)

		# Admin user
		project_api.set_user(self.admin_user)
		self.assertTrue(project_api.delete_project_document(document))

		# Project owner
		project_api.set_user(project_owner)
		self.assertTrue(project_api.delete_project_document(document))

		# Project manager
		project_api.set_user(project_manager)
		self.assertTrue(project_api.delete_project_document(document))

		# Project contributor
		project_api.set_user(project_contrib)
		self.assertTrue(project_api.delete_project_document(document))

		# Project user
		project_api.set_user(project_user)
		with self.assertRaises(UnauthorizedActionException):
			project_api.delete_project_document(document)

		# Non-member
		project_api.set_user(non_member)
		with self.assertRaises(UnauthorizedActionException):
			project_api.delete_project_document(document)
