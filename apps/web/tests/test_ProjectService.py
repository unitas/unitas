"""Unit test for ProjectService class.
"""

from django.contrib.auth.models import Group
from apps.web.tests import UnitasTestCase, ProjectService, Folder, UserService, FolderService, Project


class TestProjectService(UnitasTestCase):
	"""Test ProjectService class.
	"""
	def setUp(self):
		pass

	def get_project(
			self, name='Project1', key='proj1', ptype='Open', owner=None):
		if not owner:
			owner = UserService().get_admin_user()

		title = name
		description = 'This is project: {}'.format(name)
		project_service = ProjectService()
		project = project_service.create_project(
			name,
			key,
			ptype,
			owner,
			title='A Unitas project',
			description='This is a Unitas project',
		)
		return project

	def testInit(self):
		"""Test init().
		"""
		project_service = ProjectService()
		self.assertFalse(project_service.project)

	def testCreate(self):
		"""Test create Project.
		"""
		folder_service = FolderService()
		
		new_project = self.get_project()

		project = Project.objects.get(
			name='Project1')
		self.assertTrue(project)
		self.assertEqual(new_project, project)
		self.assertEqual(project.key, 'PROJ1')
		self.assertTrue(Group.objects.get(name='PROJ1_manager'))
		self.assertTrue(Group.objects.get(name='PROJ1_contributor'))
		self.assertTrue(Group.objects.get(name='PROJ1_user'))

		project_service = ProjectService(project)
		self.assertTrue(
			project_service.project.folder, 
			Folder.objects.get(
				name='Project1', parent_folder=folder_service.get_projects_folder()
			)
		)

	def testGetProject(self):
		project = self.get_project()
		self.assertEqual(ProjectService().get_project(project), project)

	def testUpdateProject(self):
		project = self.get_project()
		project_service = ProjectService(project)
		project_service.update_project(
			title='My project', description='This is my project')
		self.assertEqual(project_service.project.title, 'My project')

	def testDeleteProject(self):
		project = self.get_project()
		project_service = ProjectService(project)
		project_service.delete_project()

		with self.assertRaises(Project.DoesNotExist):
			project = Project.objects.get(name=project.name, key=project.key)

		with self.assertRaises(Folder.DoesNotExist):
			folder = Folder.objects.get(
				name=project.name, 
				parent_folder=FolderService().get_projects_folder())

	def testAddMember(self):
		non_priv_user1 = self.get_non_priv_user('testuser1')
		non_priv_user2 = self.get_non_priv_user('testuser2')
		non_priv_user3 = self.get_non_priv_user('testuser3')

		project = self.get_project()
		project_service = ProjectService(project)
		project_service.add_member(non_priv_user1, 'user')
		project_service.add_member(non_priv_user2, 'contributor')
		project_service.add_member(non_priv_user3, 'manager')

		users = project_service.get_users()
		contributors = project_service.get_contributors()
		managers = project_service.get_managers()

		self.assertEqual(users[0], non_priv_user1)
		self.assertEqual(contributors[0], non_priv_user2)
		self.assertEqual(managers[1], non_priv_user3)

		members = project_service.get_members()
		self.assertTrue(non_priv_user1 in members)
		self.assertTrue(non_priv_user2 in members)
		self.assertTrue(non_priv_user3 in members)

	def testFolderInProjectPath(self):

		project = self.get_project()
		project_service = ProjectService(project)
		project_folder = project_service.get_project_home_folder()

		folder_service = FolderService()
		folder_in_path = folder_service.create(
			'FolderInPath',
			project_folder,
			UserService().get_admin_user()
		)

		folder_in_path2 = folder_service.create(
			'FolderInPath2',
			folder_in_path,
			UserService().get_admin_user()
		)

		root_folder = folder_service.get_root_folder()

		folder_not_in_path = folder_service.create(
			'FolderNotInPath',
			root_folder,
			UserService().get_admin_user()
		)

		folder_not_in_path2 = folder_service.create(
			'FolderNotInPath2',
			folder_not_in_path,
			UserService().get_admin_user()
		)

		self.assertTrue(project_service.is_in_project_path(folder_in_path))
		self.assertFalse(project_service.is_in_project_path(folder_not_in_path))
		self.assertTrue(project_service.is_in_project_path(folder_in_path2))
		self.assertFalse(project_service.is_in_project_path(folder_not_in_path2))