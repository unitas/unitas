"""Unit test for FolderAuthorization class.
"""

from django.contrib.auth.models import User, Group
from apps.web.tests import UnitasTestCaseNoMimeTypes, ProjectAuthorization, Project, ProjectService, UserService

class ProjectAuthorizationTest(UnitasTestCaseNoMimeTypes):
	"""Test project authorization rules.
	"""

	def setUp(self):
		self.admin_user = UserService().get_admin_user()

	def get_project(self, name='Project1', key='PROJ1', ptype='public', owner=None):
		if not owner:
			owner = self.admin_user


		""" name, key, ptype, owner """
		project_service = ProjectService()
		project = project_service.create_project(
			name=name, 
			key=key, 
			ptype=ptype, 
			owner=owner,
		)
		return project

	def testCreateProject(self):
		"""Test create_project().

		Any user should be able to create a project except for system users like NOBODY, SYSTEM or EVERYBODY.
		"""

		# Admin user should be able to create a project.
		project_authorization = ProjectAuthorization(self.admin_user)
		self.assertTrue(project_authorization.is_authorized_creation())

		# Super user should be able to create a project.
		project_authorization = ProjectAuthorization(self.get_super_user())
		self.assertTrue(project_authorization.is_authorized_creation())

		# Non priv user should be able to create a project.
		project_authorization = ProjectAuthorization(self.get_non_priv_user())
		self.assertTrue(project_authorization.is_authorized_creation())

		# NOBODY should not be able to create a project.
		project_authorization = ProjectAuthorization(
			UserService().get_nobody_user())
		self.assertFalse(project_authorization.is_authorized_creation())

		# SYSTEM should not be able to create a project.
		project_authorization = ProjectAuthorization(
			UserService().get_everybody_user())
		self.assertFalse(project_authorization.is_authorized_creation())

		# EVERYBODY should not able to create a project.
		project_authorization = ProjectAuthorization(
			UserService().get_system_user())
		self.assertFalse(project_authorization.is_authorized_creation())

	def testReadProject(self):
		"""Test readProject().

		Any user should be able to read open or public projects. Any member of a project, super user and admin user should be to read the project whether or not it's open, public or private.
		"""
		non_priv_user1 = self.get_non_priv_user()

		open_project = self.get_project(
			name='Open Project1', 
			key='OP1',
			ptype='Open',
			owner=non_priv_user1,
		)

		public_project = self.get_project(
			name='Public Project1', 
			key='PP1',
			ptype='Public',
			owner=non_priv_user1,
		)

		private_project = self.get_project(
			name='Private Project1', 
			key='PRP1',
			ptype='Private',
			owner=non_priv_user1,
		)

		# Admin user should be able to read any project.
		for project in (open_project, public_project, private_project,):
			project_authorization = ProjectAuthorization(
				self.admin_user, project)
			self.assertTrue(project_authorization.is_authorized_read())

		# Super user should be able to read any project.
		super_user = self.get_super_user()
		for project in (open_project, public_project, private_project,):
			project_authorization = ProjectAuthorization(
				super_user, project)
			self.assertTrue(project_authorization.is_authorized_read())

		# Non priv user should be to read any open or public project.
		non_priv_user2 = self.get_non_priv_user(username='nonprivuser2')
		for project in (open_project, public_project,):
			project_authorization = ProjectAuthorization(
				super_user, project)
			self.assertTrue(project_authorization.is_authorized_read())

		# A member of a project should be able to read the project.
		group = Group.objects.get(name='{}_user'.format(private_project.key))
		group.user_set.add(non_priv_user2)

		group = Group.objects.get(name='{}_user'.format(public_project.key))
		group.user_set.add(non_priv_user2)

		group = Group.objects.get(name='{}_user'.format(open_project.key))
		group.user_set.add(non_priv_user2)

		for project in (open_project, public_project, private_project,):
			project_authorization = ProjectAuthorization(
				non_priv_user2, project)
			self.assertTrue(project_authorization.is_authorized_read())

		# Non priv user should not be able to read any private project.
		non_priv_user3 = self.get_non_priv_user(username='nonprivuser3')
		project_authorization = ProjectAuthorization(
				non_priv_user3, private_project)
		self.assertFalse(project_authorization.is_authorized_read())

	def testUpdateProject(self):
		"""Test update_project().

		Only the admin user, project owner can update a project's meta-data.
		"""

		non_priv_user1 = self.get_non_priv_user()
		super_user = self.get_super_user()

		open_project = self.get_project(
			name='Open Project1', 
			key='OP1',
			ptype='Open',
			owner=non_priv_user1,
		)

		public_project = self.get_project(
			name='Public Project1', 
			key='PP1',
			ptype='Public',
			owner=non_priv_user1,
		)

		private_project = self.get_project(
			name='Private Project1', 
			key='PRP1',
			ptype='Private',
			owner=non_priv_user1,
		)

		# Admin user can update a project
		for project in (open_project, public_project, private_project,):
			project_authorization = ProjectAuthorization(self.admin_user, project)
			self.assertTrue(project_authorization.is_authorized_update())

		# Project owner can update a project
		for project in (open_project, public_project, private_project,):
			project_authorization = ProjectAuthorization(non_priv_user1, project)
			self.assertTrue(project_authorization.is_authorized_update())

		# Super user cannot update a project
		for project in (open_project, public_project, private_project,):
			project_authorization = ProjectAuthorization(super_user, project)
			self.assertFalse(project_authorization.is_authorized_update())

		# Non priv user cannot update a project
		non_priv_user2 = self.get_non_priv_user('nonprivuser2')
		for project in (open_project, public_project, private_project,):
			project_authorization = ProjectAuthorization(non_priv_user2, project)
			self.assertFalse(project_authorization.is_authorized_update())

		# Project contributer cannot update a project
		group = Group.objects.get(name='{}_contributor'.format(private_project.key))
		group.user_set.add(non_priv_user2)

		group = Group.objects.get(name='{}_contributor'.format(public_project.key))
		group.user_set.add(non_priv_user2)

		group = Group.objects.get(name='{}_contributor'.format(open_project.key))
		group.user_set.add(non_priv_user2)

		for project in (open_project, public_project, private_project,):
			project_authorization = ProjectAuthorization(non_priv_user2, project)
			self.assertFalse(project_authorization.is_authorized_update())

		# Project user cannot update a project
		non_priv_user3 = self.get_non_priv_user(username='nonprivuser3')

		group = Group.objects.get(name='{}_user'.format(private_project.key))
		group.user_set.add(non_priv_user3)

		group = Group.objects.get(name='{}_user'.format(public_project.key))
		group.user_set.add(non_priv_user3)

		group = Group.objects.get(name='{}_user'.format(open_project.key))
		group.user_set.add(non_priv_user3)

		for project in (open_project, public_project, private_project,):
			project_authorization = ProjectAuthorization(non_priv_user3, project)
			self.assertFalse(project_authorization.is_authorized_update())

	def testDeleteProject(self):
		"""Test delete_project().

		Only the admin user, project owner can delete a project.
		"""

		non_priv_user1 = self.get_non_priv_user()
		super_user = self.get_super_user()

		open_project = self.get_project(
			name='Open Project1', 
			key='OP1',
			ptype='Open',
			owner=non_priv_user1,
		)

		public_project = self.get_project(
			name='Public Project1', 
			key='PP1',
			ptype='Public',
			owner=non_priv_user1,
		)

		private_project = self.get_project(
			name='Private Project1', 
			key='PRP1',
			ptype='Private',
			owner=non_priv_user1,
		)

		# Admin user can delete a project
		for project in (open_project, public_project, private_project,):
			project_authorization = ProjectAuthorization(self.admin_user, project)
			self.assertTrue(project_authorization.is_authorized_delete())

		# Project owner can delete a project
		for project in (open_project, public_project, private_project,):
			project_authorization = ProjectAuthorization(non_priv_user1, project)
			self.assertTrue(project_authorization.is_authorized_delete())

		# Super user cannot delete a project
		for project in (open_project, public_project, private_project,):
			project_authorization = ProjectAuthorization(super_user, project)
			self.assertFalse(project_authorization.is_authorized_delete())

		# Non priv user cannot delete a project
		non_priv_user2 = self.get_non_priv_user('nonprivuser2')
		for project in (open_project, public_project, private_project,):
			project_authorization = ProjectAuthorization(non_priv_user2, project)
			self.assertFalse(project_authorization.is_authorized_delete())

		# Project contributer cannot delete a project
		group = Group.objects.get(name='{}_contributor'.format(private_project.key))
		group.user_set.add(non_priv_user2)

		group = Group.objects.get(name='{}_contributor'.format(public_project.key))
		group.user_set.add(non_priv_user2)

		group = Group.objects.get(name='{}_contributor'.format(open_project.key))
		group.user_set.add(non_priv_user2)

		for project in (open_project, public_project, private_project,):
			project_authorization = ProjectAuthorization(non_priv_user2, project)
			self.assertFalse(project_authorization.is_authorized_delete())

		# Project user cannot delete a project
		non_priv_user3 = self.get_non_priv_user(username='nonprivuser3')
		
		group = Group.objects.get(name='{}_user'.format(private_project.key))
		group.user_set.add(non_priv_user3)

		group = Group.objects.get(name='{}_user'.format(public_project.key))
		group.user_set.add(non_priv_user3)

		group = Group.objects.get(name='{}_user'.format(open_project.key))
		group.user_set.add(non_priv_user3)

		for project in (open_project, public_project, private_project,):
			project_authorization = ProjectAuthorization(non_priv_user3, project)
			self.assertFalse(project_authorization.is_authorized_delete())

