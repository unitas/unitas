"""Test Web Views.
"""

from apps.web.tests import UnitasTestCase
from django.contrib.auth.models import User
from django.test import Client
from django.core.urlresolvers import reverse
from unitas.settings import LOGIN_URL
from apps.repository.lib.service.folder import FolderService
from apps.repository.lib.service.user import UserService
from apps.repository.lib.service.document import DocumentService
from apps.repository.models import Folder, Document, DocumentVersion
from apps.api.lib.document import DocumentApi
from apps.api.lib.folder import FolderApi, IllegalFolderApiOperation
from apps.web.config.preview import previewable_documents, exportable_as_pdf

sample_file = 'scripts/data/samples/Test Document.txt'

class WebViewsTest(UnitasTestCase):

	def setUp(self):
		self.admin_user = UserService().get_admin_user()

	def testLogin(self):

		#c = Client(enforce_csrf_checks=True)
		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': 'admin', 'password': 'admin'},
		)
		self.assertEqual(response.status_code, 302)

		response = c.post(
			reverse('index',)
		)
		self.assertEqual(response.status_code, 302)

		response = c.post(
			reverse('logout',)
		)
		self.assertEqual(response.status_code, 302)

		reponse = c.post(
			reverse('index',)
		)
		self.assertEqual(response.status_code, 302)

	def testFolderView(self):
		username = 'admin'
		password = 'admin'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)
		self.assertEqual(response.status_code, 302)

		folder_service = FolderService()
		folder_service.set_user(self.admin_user)
		user_home_folder_uuid = folder_service.get_user_home_folder().uuid

		response = c.post(
			reverse(
				'folder_view', 
				args=(
					user_home_folder_uuid,
				)
			)
		)

		expected_strings = (
			'base.css', 'dashboard.css', 'Unitas Document Management', 
			'Model Browser', 'Quick Nav:', 'Projects',
			'/Repository/Home/admin', 'Create Folder',
			'Create Document', 'Create Project', 'Log out', 'Trashcan',
		)

		for s in expected_strings:
			self.assertTrue(s in str(response))

	def testRedirectToUserHomeFolderView(self):
		c = Client()

		response = c.post(
			'/unitas/folder/',
		)

		self.assertTrue(
			'Location: /unitas/login/?next=/unitas/folder/' in str(response)
		)

		username, password = 'admin', 'admin'

		response = c.post(
			reverse('folder_view', args=(None,)),
			{'username': username, 'password': password}
		)

		self.assertEquals(response.status_code, 302)

	def testLoginPage(self):
		c = Client()
		response = c.get(
			LOGIN_URL,
		)

		self.assertEquals(response.status_code, 200)

		expected_strings = (
			'Unitas Document Management Server', '<label>Username</label>',
			'<label>Password</label>',
		)
		for s in expected_strings:
			self.assertTrue(s in str(response))

	def testFolderListing(self):
		username, password = 'admin', 'admin'
		root_folder = FolderService().get_root_folder()

		c = Client()
		c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)
		response = c.get(
			reverse('folder_view', args=(root_folder.uuid,))
		)

		expected_strings = (
			'Shared Documents', 'Projects', 'Projects Folder',
			'Global shared documents',
		) 

		for s in expected_strings:
			self.assertTrue(s in str(response))

	def testUserHomeFolderListing(self):

		username = 'admin'
		password = 'admin'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)
		self.assertEqual(response.status_code, 302)

		folder_service = FolderService()
		folder_service.set_user(self.admin_user)
		user_home_folder = folder_service.get_user_home_folder()

		response = c.get(
			reverse('folder_view', args=(user_home_folder.uuid,))
		)

		expected_strings = ('Trashcan',)

		for s in expected_strings:
			self.assertTrue(s in str(response))

	def testCreateFolderView(self):
		username = 'admin'
		password = 'admin'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)
		self.assertEqual(response.status_code, 302)

		folder_service = FolderService()
		folder_service.set_user(self.admin_user)
		user_home_folder_uuid = folder_service.get_user_home_folder().uuid

		response = c.post(
			reverse(
				'create_folder_view', 
				args=(user_home_folder_uuid,)
			),
			{
				'name': 'Folder5',
				'title': 'Folder #5',
				'description': 'This is folder #5',
			},
		)
		
		self.assertTrue(Folder.objects.get(name='Folder5'))

	def testFolderPropertiesView(self):
		username = 'admin'
		password = 'admin'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)
		self.assertEqual(response.status_code, 302)

		folder_service = FolderService()
		folder_service.set_user(self.admin_user)
		user_home_folder_uuid = folder_service.get_user_home_folder().uuid

		response = c.post(
			reverse(
				'folder_properties_view', 
				args=(user_home_folder_uuid,),
			)
		)

		expected_strings = (
			'Name:', 'UUID:', 'Title:', 'Description:', 'Created:', 'Modified:',
		)

		for s in expected_strings:
			self.assertTrue(s in str(response))

	def testDocumentPropertiesView(self):
		username = 'admin'
		password = 'admin'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)
		self.assertEqual(response.status_code, 302)

		folder_service = FolderService()
		folder_service.set_user(self.admin_user)
		user_home_folder = folder_service.get_user_home_folder()

		document_service = DocumentService()
		document = document_service.create(
			'Test.txt',
			user_home_folder,
			sample_file,
			UserService().get_admin_user(),
		)

		response = c.post(
			reverse(
				'document_properties_view',
				args=(document.uuid,),
			)
		)

		expected_strings = (
			'UUID:', 'Name:', 'MimeType:', 'Owner:', 'Title:', 'Description:',
			'Preview', 'Download', 'All Versions', 'Share Document',
			'Upload New Version', 'File Size',
		)

		for s in expected_strings:
			self.assertTrue(s in str(response))

	def testDeleteDocumentView(self):
		username = 'admin'
		password = 'admin'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)
		self.assertEqual(response.status_code, 302)

		folder_service = FolderService()
		folder_service.set_user(self.admin_user)
		user_home_folder = folder_service.get_user_home_folder()

		document_service = DocumentService()
		document = document_service.create(
			'Test.txt',
			user_home_folder,
			sample_file,
			UserService().get_admin_user(),
		)

		c.get(
			reverse(
				'delete_document_view',
				args=(document.uuid,),
			)
		)

		document = Document.objects.get(name=document.name)
		self.assertEqual(document.parent_folder.name, 'Trashcan')

	def testTrashcanView(self):
		username = 'admin'
		password = 'admin'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)
		self.assertEqual(response.status_code, 302)

		folder_service = FolderService()
		folder_service.set_user(self.admin_user)
		user_home_folder = folder_service.get_user_home_folder()

		document_service = DocumentService()
		document = document_service.create(
			'Test.txt',
			user_home_folder,
			sample_file,
			UserService().get_admin_user(),
		)

		document_api = DocumentApi(UserService().get_admin_user())
		document_api.delete_document(document)

		response = c.get(
			reverse(
				'trashcan_folder_view',
			)
		)
		expected_strings = (
			'Test.txt',
		)
		
		for s in expected_strings:
			self.assertTrue(s in str(response))


	def testDeleteFolderView(self):
		username = 'admin'
		password = 'admin'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)
		self.assertEqual(response.status_code, 302)

		folder_service = FolderService()
		folder_service.set_user(self.admin_user)
		user_home_folder = folder_service.get_user_home_folder()

		folder = folder_service.create(
			'Folder1',
			user_home_folder,
			UserService().get_admin_user(),
		)

		response = c.get(
			reverse(
				'delete_folder_view',
				args=(folder.uuid,),
			)
		)

		not_expected_strings = (
			'Folder1',
		)

		for s in not_expected_strings:
			self.assertFalse(s in str(response))

	def testIllegalFolderApiOperation(self):
		username = 'admin'
		password = 'admin'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)
		self.assertEqual(response.status_code, 302)

		folder_service = FolderService()
		folder_service.set_user(self.admin_user)
		user_home_folder = folder_service.get_user_home_folder()

		folder = folder_service.create(
			'Folder1',
			user_home_folder,
			UserService().get_admin_user(),
		)

		response = c.get(
			reverse(
				'delete_folder_view',
				args=(folder.uuid,),
			)
		)

		folder1 = Folder.objects.get(name='Folder1')

		with self.assertRaises(IllegalFolderApiOperation):
			folder_api = FolderApi(UserService().get_admin_user())
			folder_api.create_folder(
				'Folder2',
				folder1,
				UserService().get_admin_user(),
			)

	def testEditFolderView(self):
		username = 'admin'
		password = 'admin'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)
		self.assertEqual(response.status_code, 302)

		folder_service = FolderService()
		folder_service.set_user(self.admin_user)
		user_home_folder_uuid = folder_service.get_user_home_folder().uuid

		response = c.post(
			reverse(
				'create_folder_view', 
				args=(user_home_folder_uuid,)
			),
			{
				'name': 'Folder5',
				'title': 'Folder #5',
				'description': 'This is folder #5',
			},
		)

		folder = Folder.objects.get(name='Folder5')

		description = 'This is folder #5. Additional comment.'

		response = c.post(
			reverse(
				'edit_folder_view',
				args=(folder.uuid,),
			),
			{
				'description': description,
			}
		)

		self.assertTrue(description in str(response))

	def testEditDocumentView(self):
		username = 'admin'
		password = 'admin'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)
		self.assertEqual(response.status_code, 302)

		folder_service = FolderService()
		folder_service.set_user(self.admin_user)
		user_home_folder = folder_service.get_user_home_folder()

		document_service = DocumentService()
		document_service.create(
			'Document1.txt',
			user_home_folder,
			sample_file,
			UserService().get_admin_user(),
			title='Document #1 Text File',
			description='Document #1 Text File',
		)

		document = Document.objects.get(name='Document1.txt')

		description = 'This is Document #1 Text File.'

		response = c.post(
			reverse(
				'edit_document_view',
				args=(document.uuid,),
			),
			{
				'description': description,
			}
		)

		self.assertTrue(description in str(response))
		
	def testUnAuthEditSystemFoldersView(self):
		username = 'admin'
		password = 'admin'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)
		self.assertEqual(response.status_code, 302)

		folder_service = FolderService()
		folder_service.set_user(self.admin_user)
		trashcan_folder = folder_service.get_user_trashcan_folder()

		response = c.get(
			reverse(
				'edit_folder_view',
				args=(trashcan_folder.uuid,),
			)
		)

		self.assertEqual(response.status_code, 302)

	def testSuperUserHasAdminConsoleLink(self):
		username = 'test_superuser'
		password = 'secret'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)
		self.assertEqual(response.status_code, 302)

		folder_service = FolderService()
		folder_service.set_user(User.objects.get(username=username))
		user_home_folder = folder_service.get_user_home_folder()

		response = c.get(
			reverse(
				'folder_view',
				args=(user_home_folder.uuid,),
			)
		)

		expected_strings = (
			'Admin Console', 'a href="/unitas/admin/"',
		)

		for s in expected_strings:
			self.assertTrue(s in str(response))

	def testShareInFolderViewWithDocument(self):
		username = 'admin'
		password = 'admin'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)

		folder_service = FolderService()
		folder_service.set_user(User.objects.get(username=username))
		user_home_folder = folder_service.get_user_home_folder()

		document_service = DocumentService()
		document_service.create(
			'Test1.txt',
			user_home_folder,
			sample_file,
			self.admin_user,
		)

		response = c.get(
			reverse(
				'folder_view',
				args=(user_home_folder.uuid,),
			)
		)

		expected_strings = (
			'Share document with others',
		)

		for s in expected_strings:
			self.assertTrue(s in str(response))

	def testDownloadDocumentView(self):
		username = 'admin'
		password = 'admin'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)

		folder_service = FolderService()
		folder_service.set_user(User.objects.get(username=username))
		user_home_folder = folder_service.get_user_home_folder()

		document_service = DocumentService()
		document_service.create(
			'Test1.txt',
			user_home_folder,
			sample_file,
			self.admin_user,
		)

		document = Document.objects.get(name='Test1.txt')

		response = c.get(
			reverse(
				'download_document_view',
				args=(document.uuid,),
			)
		)

		expected_strings = (
			'aldks',
		)

		for s in expected_strings:
			self.assertTrue(s in str(response))

	def testForceDeleteDocumentView(self):
		username = 'admin'
		password = 'admin'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)

		folder_service = FolderService()
		folder_service.set_user(User.objects.get(username=username))
		user_home_folder = folder_service.get_user_home_folder()

		document_service = DocumentService()
		document_service.create(
			'Test1.txt',
			user_home_folder,
			sample_file,
			self.admin_user,
		)

		document = Document.objects.get(name='Test1.txt')

		c.get(
			reverse(
				'force_delete_document_view',
				args=(document.uuid,),
			)
		)

		with self.assertRaises(Document.DoesNotExist):
			document = Document.objects.get(name='Test1.txt')

	def testForceDeleteDocumentView(self):
		username = 'admin'
		password = 'admin'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)

		folder_service = FolderService()
		folder_service.set_user(User.objects.get(username=username))
		user_home_folder = folder_service.get_user_home_folder()

		document_service = DocumentService()
		document_service.create(
			'Test1.txt',
			user_home_folder,
			sample_file,
			self.admin_user,
		)

		folder_service = FolderService()
		folder_service.create(
			'Folder1',
			user_home_folder,
			self.admin_user,
		)

		folder = Folder.objects.get(name='Folder1')

		c.get(
			reverse(
				'force_delete_folder_view',
				args=(folder.uuid,),
			)
		)

		with self.assertRaises(Folder.DoesNotExist):
			folder = Folder.objects.get(name='Folder1')

	def testFolderWithNoTitleNoDescription(self):
		username = 'admin'
		password = 'admin'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)

		folder_service = FolderService()
		folder_service.set_user(User.objects.get(username=username))
		user_home_folder = folder_service.get_user_home_folder()

		folder_service = FolderService()
		folder_service.create(
			'Folder1',
			user_home_folder,
			self.admin_user,
		)

		response = c.get(
			reverse(
				'folder_view',
				args=(user_home_folder.uuid,),
			)
		)

		expected_strings = (
			'''<strong>Title:</strong> 

			        		

			        			None''',
		)

		for s in expected_strings:
			self.assertTrue(s in str(response))

	def testMoveFolderView(self):
		username = 'admin'
		password = 'admin'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)

		folder_service = FolderService()
		folder_service.set_user(User.objects.get(username=username))
		user_home_folder = folder_service.get_user_home_folder()

		folder_service = FolderService()
		
		folder1 = folder_service.create(
			'Folder1',
			user_home_folder,
			self.admin_user,
		)

		folder11 = folder_service.create(
			'Folder11',
			user_home_folder,
			self.admin_user,
		)

		c.get(
			reverse(
				'move_folder_view',
				args=(folder11.uuid,),
			)
		)

		c.get(
			reverse(
				'paste_folder_view',
				args=(folder1.uuid,),
			)
		)

		folder = Folder.objects.get(name='Folder11')
		self.assertEqual(
			folder.parent_folder,
			folder1
		)

	def testCopyFolderView(self):
		username = 'admin'
		password = 'admin'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)

		folder_service = FolderService()
		folder_service.set_user(User.objects.get(username=username))
		user_home_folder = folder_service.get_user_home_folder()

		folder_service = FolderService()
		
		folder1 = folder_service.create(
			'Folder1',
			user_home_folder,
			self.admin_user,
		)

		folder11 = folder_service.create(
			'Folder11',
			user_home_folder,
			self.admin_user,
		)

		c.get(
			reverse(
				'copy_folder_view',
				args=(folder11.uuid,),
			)
		)

		c.get(
			reverse(
				'paste_folder_view',
				args=(folder1.uuid,),
			)
		)

		folders = Folder.objects.filter(name='Folder11')
		
		self.assertEqual(folders.count(), 2)

	def testMoveDocumentView(self):
		username = 'admin'
		password = 'admin'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)

		folder_service = FolderService()
		folder_service.set_user(User.objects.get(username=username))
		user_home_folder = folder_service.get_user_home_folder()

		folder_service = FolderService()
		document_service = DocumentService()
		
		folder1 = folder_service.create(
			'Folder1',
			user_home_folder,
			self.admin_user,
		)

		document1 = document_service.create(
			'Test.txt',
			user_home_folder,
			sample_file,
			self.admin_user,
		)

		c.get(
			reverse(
				'move_document_view',
				args=(document1.uuid,),
			)
		)

		c.get(
			reverse(
				'paste_folder_view',
				args=(folder1.uuid,),
			)
		)

		document = Document.objects.get(name='Test.txt')
		self.assertEqual(
			document.parent_folder,
			folder1
		)

	def testCopyDocumentView(self):
		username = 'admin'
		password = 'admin'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)

		folder_service = FolderService()
		folder_service.set_user(User.objects.get(username=username))
		user_home_folder = folder_service.get_user_home_folder()

		folder_service = FolderService()
		document_service = DocumentService()
		
		folder1 = folder_service.create(
			'Folder1',
			user_home_folder,
			self.admin_user,
		)

		document1 = document_service.create(
			'Test.txt',
			user_home_folder,
			sample_file,
			self.admin_user,
		)

		c.get(
			reverse(
				'copy_document_view',
				args=(document1.uuid,),
			)
		)

		c.get(
			reverse(
				'paste_folder_view',
				args=(folder1.uuid,),
			)
		)

		documents = Document.objects.filter(name='Test.txt')
		
		self.assertEqual(documents.count(), 2)

	def testPreviewableDocs(self):
		username = 'admin'
		password = 'admin'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)

		folder_service = FolderService()
		folder_service.set_user(User.objects.get(username=username))
		user_home_folder = folder_service.get_user_home_folder()

		document_service = DocumentService()
		for ext in previewable_documents:
			document = document_service.create(
				'SampleFile{}'.format(ext),
				user_home_folder,
				sample_file,
				self.admin_user,
			)

			if document:
				response = c.get(
					reverse(
						'document_properties_view',
						args=(document.uuid,),
					)
				)

				self.assertTrue('Preview' in str(response))

		document = document_service.create(
			'SampleFile.crap',
			user_home_folder,
			sample_file,
			self.admin_user,
		)

		response = c.get(
			reverse(
				'document_properties_view',
				args=(document.uuid,),
			)
		)
		self.assertFalse('Preview' in str(response))

	def testExportableAsPDF(self):
		username = 'admin'
		password = 'admin'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)

		folder_service = FolderService()
		folder_service.set_user(User.objects.get(username=username))
		user_home_folder = folder_service.get_user_home_folder()

		document_service = DocumentService()
		for ext in exportable_as_pdf:
			document = document_service.create(
				'SampleFile{}'.format(ext),
				user_home_folder,
				sample_file,
				self.admin_user,
			)

			if document:
				response = c.get(
					reverse(
						'document_properties_view',
						args=(document.uuid,),
					)
				)

				self.assertTrue('Export as PDF' in str(response))

		document = document_service.create(
			'SampleFile.crap',
			user_home_folder,
			sample_file,
			self.admin_user,
		)

		response = c.get(
			reverse(
				'document_properties_view',
				args=(document.uuid,),
			)
		)
		self.assertFalse('Export as PDF' in str(response))

	def testShareDocumentView(self):
		username = 'admin'
		password = 'admin'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)

		folder_service = FolderService()
		folder_service.set_user(User.objects.get(username=username))
		user_home_folder = folder_service.get_user_home_folder()

		document_service = DocumentService()
		document = document_service.create(
			'Test.txt',
			user_home_folder,
			sample_file,
			self.admin_user,
		)

		c.get(
			reverse(
				'share_document_view',
				args=(document.uuid,),
			)
		)

		username = 'test_nonprivuser'
		password = 'secret'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)

		response = c.get(
			reverse(
				'folder_view',
				args=(FolderService().get_shared_documents_folder().uuid,),
			)
		)

		self.assertTrue('Test.txt' in str(response))

	def testEmptyTrashcan(self):
		username = 'admin'
		password = 'admin'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)

		folder_service = FolderService()
		folder_service.set_user(User.objects.get(username=username))
		user_home_folder = folder_service.get_user_home_folder()

		document_service = DocumentService()
		document = document_service.create(
			'Test.txt',
			user_home_folder,
			sample_file,
			self.admin_user,
		)

		folder1 = folder_service.create(
			'Folder1',
			user_home_folder,
			self.admin_user,
		)

		c.get(
			reverse(
				'delete_document_view',
				args=(document.uuid,),
			)
		)

		c.get(
			reverse(
				'delete_folder_view',
				args=(folder1.uuid,),
			)
		)

		response = c.get(
			reverse(
				'trashcan_folder_view',
			)
		)

		self.assertTrue('Empty Trashcan' in str(response))

		c.get(
			reverse(
				'empty_trashcan_folder_view',
			)
		)

		response = c.get(
			reverse(
				'trashcan_folder_view',
			)
		)

		self.assertTrue('Empty' in str(response))

	'''
	def testCreateNewVersionView(self):
		username = 'admin'
		password = 'admin'

		c = Client()
		response = c.post(
			LOGIN_URL,
			{'username': username, 'password': password},
		)

		folder_service = FolderService()
		folder_service.set_user(User.objects.get(username=username))
		user_home_folder = folder_service.get_user_home_folder()

		document_service = DocumentService()
		document = document_service.create(
			'Test.txt',
			user_home_folder,
			sample_file,
			self.admin_user,
		)

		c.post(
			reverse(
				'upload_new_version_view',
				args=(document.uuid,),
			),
			{
				'mode': 'minor',
				'content_file': sample_file,
			},
			
		)

		versions = DocumentVersion.objects.get(parent_document=document)
		print versions
	'''