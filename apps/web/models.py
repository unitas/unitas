"""Web app models.
"""

from __future__ import unicode_literals
from __future__ import absolute_import

from django.db import models
from django.contrib.auth.models import User
from apps.web.config.project import project_types
import uuid


class Project(models.Model):
	"""Project is an arbitrary collection of folders and documents where users can share and collaborate with other users who are members of the project.
	"""

	uuid = models.UUIDField(
        'UUID',
        primary_key=True, 
        default=uuid.uuid4,
        editable=False
    )
	
	name = models.CharField(
		'Name',
		max_length=255,
		unique=True,
	)
	"""Project's name. Must be unique.
	"""

	key = models.CharField(
		'Key',
		max_length=10,
		unique=True,
	)
	"""Project's unique short name or key.
	"""

	title = models.CharField(
		'Title',
		max_length=255,
		null=True,
		blank=True,
	)
	"""Optional title of the project.
	"""

	description = models.TextField(
		'Description',
		null=True,
		blank=True,
	)
	"""Optional description of the project.
	"""

	ptype = models.CharField(
		'Type',
		max_length=30,
		choices=[(typ, typ) for typ in project_types],
	)
	"""Project type. Choices:
		Open: Anyone can see this project and join it without approval.
		Public: Anyone can see this project but can only join with owner approval.
		Private: No one can see this project except the admin and other super users. Can only join with owner approval.
	"""

	owner = models.ForeignKey(
		User,
	)
	"""Owner of the project.
	"""

	created = models.DateTimeField(
		'Created',
		auto_now_add=True,
	)
	"""Date time when project was created.
	"""

	folder = models.ForeignKey(
		'repository.Folder',
	)

	def save(self, force_insert=False, force_update=False):
		"""Overridden Django model save method to ensure that key will always be upper case.
		"""
		self.key = self.key.upper()
		super(Project, self).save(force_insert, force_update)

	def __str__(self):
		return '{} - {} Project'.format(
			self.name, self.ptype,
		)

def get_file_path(instance, filename):
    """ Returns the os path where content files are created and stored on disk"""

    # Importing these time functions so that we can create a folder tree with year/month/day/hour/minute/etc.
    from time import gmtime, strftime
    import os, uuid
    ext = 'bin'
    filename = "{}.{}".format(uuid.uuid4(), ext)
    return os.path.join(strftime(instance.directory_string_var, gmtime()), filename)

class PreviewDocument(models.Model):

	document_version = models.OneToOneField('repository.DocumentVersion')

	content_file = models.FileField(
        'Content File',
        upload_to=get_file_path,
    )

	created = models.DateTimeField(
		auto_now_add=True,
	)

	""" Directory based on time. See get_file_path(). """
	directory_string_var = 'content/%Y/%m/%d/%H/%M/%S/'

	def __str__(self):
		return '{} / {}'.format(
			self.document_version.parent_document.name, 
			self.document_version
		)