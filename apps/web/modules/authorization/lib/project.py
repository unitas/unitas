"""Module containing ProjectAuthorization class.
"""

from django.contrib.auth.models import User
from apps.web.modules.authorization.lib.authorization import Authorization
from apps.web.modules.authorization.config.project_authorization_filters import project_creation_filters, project_read_filters, project_update_filters, project_delete_filters, folder_creation_filters, folder_read_filters, folder_update_filters, folder_delete_filters, document_creation_filters, document_read_filters, document_update_filters, document_delete_filters
import logging

logger = logging.getLogger(__name__)


class ProjectAuthorization(Authorization):
	"""Class to check for authorizations for projects.
	"""

	def __init__(self, user, project=None):
		"""Sets default user, folder and object (for evaluate()).
		"""
		self.user = user
		self.project = project
		self.object = self.project

	def is_authorized_creation(self):
		"""Returns true/false if default user is authorized to perform create action.
		"""
		result = self.evaluate(project_creation_filters)
		return result

	def is_authorized_read(self):
		"""Returns true/false if default user is authorized to perform read action.
		"""
		result = self.evaluate(project_read_filters)
		return result

	def is_authorized_update(self):
		"""Returns true/false if default user is authorized to perform update action.
		"""
		results = self.evaluate(project_update_filters)
		return results

	def is_authorized_delete(self):
		"""Returns true/false if default user is authorized to perform delete action.
		"""
		result = self.evaluate(project_delete_filters)
		return result

class ProjectFolderAuthorization(Authorization):
	"""Class to check for authorizations for project folders.
	"""

	def __init__(self, user, project, folder=None):
		"""Sets default user, folder and object (for evaluate()).
		"""
		self.user = user
		self.project = project
		self.object = self.project
		self.folder = folder

	def is_authorized_creation(self):
		"""Returns true/false if default user is authorized to perform create action.
		"""
		result = self.evaluate(folder_creation_filters)
		return result

	def is_authorized_read(self):
		"""Returns true/false if default user is authorized to perform read action.
		"""
		result = self.evaluate(folder_read_filters)
		return result

	def is_authorized_update(self):
		"""Returns true/false if default user is authorized to perform update action.
		"""
		results = self.evaluate(folder_update_filters)
		return results

	def is_authorized_delete(self):
		"""Returns true/false if default user is authorized to perform delete action.
		"""
		result = self.evaluate(folder_delete_filters)
		return result


class ProjectDocumentAuthorization(Authorization):
	"""Class to check for authorizations for project documents.
	"""

	def __init__(self, user, project, document=None):
		"""Sets default user, folder and object (for evaluate()).
		"""
		self.user = user
		self.project = project
		self.object = self.project
		self.document = document

	def is_authorized_creation(self):
		"""Returns true/false if default user is authorized to perform create action.
		"""
		result = self.evaluate(document_creation_filters)
		return result

	def is_authorized_read(self):
		"""Returns true/false if default user is authorized to perform read action.
		"""
		result = self.evaluate(document_read_filters)
		return result

	def is_authorized_update(self):
		"""Returns true/false if default user is authorized to perform update action.
		"""
		results = self.evaluate(document_update_filters)
		return results

	def is_authorized_delete(self):
		"""Returns true/false if default user is authorized to perform delete action.
		"""
		result = self.evaluate(document_delete_filters)
		return result