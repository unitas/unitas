"""Set of rules for testing project authorizations.
"""

project_creation_filters = (
	(
		(
			('AUTH_USER', 'EQUALS', 'NOBODY_USER',),
		),
		False
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'EVERYBODY_USER',),
		),
		False
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'SYSTEM_USER',),
		),
		False
	),
	(
		(
			('AUTH_USER',),
		),
		True
	),
)

project_read_filters = (
	(
		(
			('AUTH_USER', 'EQUALS', 'OBJECT_OWNER',),
		),
		True
	),
	(
		(
			('AUTH_USER_IS_PROJECT_MEMBER',),
		),
		True
	),
	(
		(
			('PROJECT_IS_OPEN',),
		),
		True
	),
	(
		(
			('PROJECT_IS_PUBLIC',),
		),
		True
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER',),
		),
		True
	),
	(
		(
			('AUTH_USER_IS_SUPERUSER',),
		),
		True
	),
)

project_update_filters = (
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER',),
		),
		True
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'OBJECT_OWNER',),
		),
		True
	),
	(
		(
			('AUTH_USER_IS_PROJECT_MANAGER',),
		),
		True
	),
)

project_delete_filters = (
	(
		(
			('AUTH_USER', 'EQUALS', 'OBJECT_OWNER',),
		),
		True
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER',),
		),
		True
	),
)

folder_creation_filters = (
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER',),
		),
		True
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'OBJECT_OWNER',),
		),
		True
	),
	(
		(
			('AUTH_USER_IS_PROJECT_MANAGER',),
		),
		True
	),
		(
		(
			('AUTH_USER_IS_PROJECT_CONTRIBUTOR',),
		),
		True
	),
)

folder_read_filters = (
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER',),
		),
		True
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'OBJECT_OWNER',),
		),
		True
	),
	(
		(
			('AUTH_USER_IS_PROJECT_MEMBER',),
		),
		True
	),
)

folder_update_filters = (
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER',),
		),
		True
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'OBJECT_OWNER',),
		),
		True
	),
	(
		(
			('AUTH_USER_IS_PROJECT_MANAGER',),
		),
		True
	),
	(
		(
			('AUTH_USER_IS_PROJECT_CONTRIBUTOR',),
			('AUTH_USER', 'EQUALS', 'FOLDER_OWNER',),
		),
		True
	),
)

folder_delete_filters = (
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER',),
		),
		True
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'OBJECT_OWNER',),
			('AUTH_USER_IS_PROJECT_MEMBER',),
		),
		True
	),
	(
		(
			('AUTH_USER_IS_PROJECT_MANAGER',),
		),
		True
	),
	(
		(
			('AUTH_USER_IS_PROJECT_CONTRIBUTOR',),
			('AUTH_USER', 'EQUALS', 'FOLDER_OWNER',),
		),
		True
	),
)

document_creation_filters = (
	(
		(
			('AUTH_USER', 'EQUALS', 'OBJECT_OWNER',),
		),
		True
	),
	(
		(
			('AUTH_USER_IS_PROJECT_MANAGER',),
		),
		True
	),
		(
		(
			('AUTH_USER_IS_PROJECT_CONTRIBUTOR',),
		),
		True
	),
)

document_read_filters = (
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER',),
		),
		True
	),
	(
		(
			('AUTH_USER_IS_PROJECT_MEMBER',),
		),
		True
	),
)

document_update_filters = (
	(
		(
			('AUTH_USER', 'EQUALS', 'OBJECT_OWNER',),
		),
		True
	),
	(
		(
			('AUTH_USER_IS_PROJECT_MANAGER',),
		),
		True
	),
	(
		(
			('AUTH_USER_IS_PROJECT_CONTRIBUTOR',),
			('AUTH_USER', 'EQUALS', 'FOLDER_OWNER',),
		),
		True
	),
)

document_delete_filters = (
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER',),
		),
		True
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'OBJECT_OWNER',),
			('AUTH_USER_IS_PROJECT_MEMBER',),
		),
		True
	),
	(
		(
			('AUTH_USER_IS_PROJECT_MANAGER',),
		),
		True
	),
	(
		(
			('AUTH_USER_IS_PROJECT_CONTRIBUTOR',),
			('AUTH_USER', 'EQUALS', 'FOLDER_OWNER',),
		),
		True
	),
)