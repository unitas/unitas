"""Django admin page for Web app models.
"""

from django.contrib import admin
from models import Project, PreviewDocument

class ProjectAdmin(admin.ModelAdmin):
	
	class Meta:
		
		fields = [
			'name', 'key', 'title', 'description', 
			'ptype', 'owner', 'folder',
		]

admin.site.register(Project, ProjectAdmin)
admin.site.register(PreviewDocument)