"""Unit tests for DocumentService class
"""

from apps.repository.tests import Document, DocumentService, DocumentVersion, Folder, FolderService, MimeType, UnitasTestCase, User, UserService

import os, sys
sys.path.append('.')
os.environ['DJANGO_SETTINGS_MODULE'] = 'unitas.settings'

import django
django.setup()

sample_file = 'scripts/data/samples/Test Document.txt'

class TestDocumentService(UnitasTestCase):
	"""Tests for DocumentService class.
	"""

	document_service = DocumentService()

	def setUp(self):
		"""Create root folder and admin user for use in tests.
		"""
		self.root_folder = Folder.objects.get(
			name='Repository', parent_folder=None)
		self.admin_user = User.objects.get(username='admin')

	def testInit(self):
		"""Test init for DocumentService()
		"""
		document_service = DocumentService()
		self.assertFalse(document_service.document)

	def testInitByDocId(self):
		"""Test init for DocumentService() with params.
		"""
		document1 = self.document_service.create(
			'Document1.txt',
			self.root_folder,
			sample_file,
			self.admin_user
		)
		document_service = DocumentService()
		document_service.set_document(document1)
		self.assertEqual(document_service.document.name, 'Document1.txt')

	def testSetUser(self):
		"""Test set_user().
		"""
		self.document_service.set_user(self.admin_user)
		self.assertEqual(self.document_service.user.username, 'admin')

	def testCreate(self):
		"""Test create().
		"""
		self.document_service.create(
			'Document2.txt',
			self.root_folder,
			sample_file,
			self.admin_user,
		)

		self.assertTrue(Document.objects.get(
			name='Document2.txt', 
			owner=self.admin_user, 
			parent_folder=self.root_folder)
		)

	def testAddNewVersion(self):
		"""Test add_new_version().
		"""
		document_service = DocumentService()
		document1 = document_service.create(
			'Document1.txt',
			self.root_folder,
			sample_file,
			self.admin_user,
		)

		document_service.add_new_version(
			sample_file,
			'minor',
		)

		document_service.add_new_version(
			sample_file,
			'major',
		)

		document_versions = DocumentVersion.objects.filter(
			parent_document=document1)
		self.assertEqual(document_versions.count(), 3)

	def testGetLatestVersion(self):
		"""Test get_latest_version().
		"""
		document_service = DocumentService()
		document1 = document_service.create(
			'Document1.txt',
			self.root_folder,
			sample_file,
			self.admin_user,
		)

		document_service.add_new_version(
			sample_file,
			'minor',
		)

		document_service.add_new_version(
			sample_file,
			'major',
		)

		self.assertEqual(
			document_service.get_latest_version().version,
			'2.1', 
		)

	def testGetNewVersion(self):
		"""Test get_new_version().
		"""
		document_service = DocumentService()
		document1 = document_service.create(
			'Document1.txt',
			self.root_folder,
			sample_file,
			self.admin_user,
		)

		document_service.add_new_version(
			sample_file,
			'minor',
		)

		document_service.add_new_version(
			sample_file,
			'major',
		)

		self.assertEqual(
			document_service.get_new_version('minor'),
			'2.2',
		)

		self.assertEqual(
			document_service.get_new_version('major'),
			'3.1',
		)

	def testUpdate(self):
		"""Test update().
		"""
		document_service = DocumentService()
		document1 = document_service.create(
			'Document1.txt',
			self.root_folder,
			sample_file,
			self.admin_user,
			title='Document #1',
		)
		document_service.update(
			name='Document 1.txt',
			title='Document # 1',
		)

		document = Document.objects.get(name='Document 1.txt')
		self.assertTrue(document)
		self.assertEqual(
			Document.objects.filter(name='Document1.txt').count(), 0)

	def testUpdateExcludedFields(self):
		"""Test update() against excluded fields.
		"""
		document_service = DocumentService()
		document1 = document_service.create(
			'Document1.txt',
			self.root_folder,
			sample_file,
			self.admin_user,
		)

		user_service = UserService()
		testuser1 = user_service.create(
			'testuser1',
			'testuser1',
			'testuser1@localhost',
		)
		document_service.set_user(self.admin_user)
		document_service.update(
			owner=testuser1,
		)
		self.assertNotEqual(document_service.document.owner, testuser1)
		self.assertEqual(document_service.document.owner, self.admin_user)

		mime_type = MimeType.objects.get(extension='.docx')
		orig_mime_type = document_service.document.mime_type
		document_service.update(
			mime_type=mime_type,
		)
		self.assertNotEqual(document_service.document.mime_type, mime_type)
		self.assertEqual(document_service.document.mime_type, orig_mime_type)

	def testMove(self):
		"""Test move().
		"""
		folder_service = FolderService()
		document_service = DocumentService()

		document1 = document_service.create(
			'Document1.txt',
			self.root_folder,
			sample_file,
			self.admin_user,
		)

		self.assertTrue(document1.parent_folder, self.root_folder)
		
		folder1 = folder_service.create(
			'Folder1',
			self.root_folder,
			self.admin_user,
		)

		document_service.move(folder1)
		self.assertEqual(document_service.document.parent_folder, folder1)

	def testCopy(self):
		"""Test copy().
		"""
		folder_service = FolderService()
		document_service = DocumentService()

		document1 = document_service.create(
			'Document1.txt',
			self.root_folder,
			sample_file,
			self.admin_user,
		)

		self.assertTrue(document1.parent_folder, self.root_folder)
		
		folder1 = folder_service.create(
			'Folder1',
			self.root_folder,
			self.admin_user,
		)

		document_service.copy(folder1)

		documents = Document.objects.filter(
			name='Document1.txt',
		)

		self.assertEqual(documents.count(), 2)

		copy_document = Document.objects.get(
			name='Document1.txt',
			parent_folder=folder1,
		)

		self.assertTrue(copy_document)

	def testSetGetProperty(self):
		"""Test getters and setters for property.
		"""
		document_service = DocumentService()

		document1 = document_service.create(
			'Document1.txt',
			self.root_folder,
			sample_file,
			self.admin_user,
		)
		document_service.set_property({'key1':'value1'})
		self.assertEqual(document_service.get_property('key1'), 'value1')

	def testSetGetProperties(self):
		"""Test getters and setters for properties with multiple key/value pairs.
		"""
		document_service = DocumentService()

		document1 = document_service.create(
			'Document1.txt',
			self.root_folder,
			sample_file,
			self.admin_user,
		)

		document_service.set_properties(
			[
				{'key1': 'value1'},
				{'key2': 'value2'},
				{'key3': 'value3'},
			]
		)

		properties = document_service.get_properties()
		self.assertEqual(properties[0].key, 'key3')
		self.assertEqual(properties[0].value, 'value3')
		self.assertEqual(properties[1].key, 'key2')
		self.assertEqual(properties[1].value, 'value2')
		self.assertEqual(properties[2].key, 'key1')
		self.assertEqual(properties[2].value, 'value1')

	def testForTrailingSlashOnDoc(self):
		"""Test for trailing slash on document paths. Bug.
		"""
		document_service = DocumentService()
		document_service.create(
			'Document1.txt',
			self.root_folder,
			sample_file,
			self.admin_user,	
		)

		document = Document.objects.get(name='Document1.txt')
		self.assertFalse(str(self.root_folder).endswith('/'))
		self.assertFalse(str(document).endswith('/'))

	def testSetOwner(self):
		"""Test set_owner().
		"""
		user_service = UserService()
		testuser = user_service.create(
			'testuser',
			'testuser',
			'testuser@localhot',
		)

		document_service = DocumentService()
		document = document_service.create(
			'Document1.txt',
			self.root_folder,
			sample_file,
			testuser,
		)

		document_service.set_owner(self.admin_user)
		self.assertEqual(document_service.document.owner, self.admin_user)

	def testNoDocumentExtension(self):
		"""Test create() document with no file extension.
		"""
		user_service = UserService()
		testuser = user_service.create(
			'testuser',
			'testuser',
			'testuser@localhot',
		)

		document_service = DocumentService()
		document = document_service.create(
			'Document1',
			self.root_folder,
			sample_file,
			testuser,
		)

		self.assertEqual(
			document_service.document.mime_type.designation, 
			'application/octet-stream'
		)

	def testSetMimeType(self):
		"""Test set_mime_type().
		"""
		user_service = UserService()
		testuser = user_service.create(
			'testuser',
			'testuser',
			'testuser@localhot',
		)

		document_service = DocumentService()
		document_service.create(
			'Document1',
			self.root_folder,
			sample_file,
			testuser,
		)

		mime_type = MimeType.objects.get(extension='.txt')
		document_service.set_mime_type(mime_type)
		self.assertEqual(
			document_service.document.mime_type.name,
			'Text File',
		)

