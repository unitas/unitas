"""Unit tests for FolderService class
"""

from apps.repository.tests import Document, DocumentService, Folder, FolderService, ModelType, UnitasTestCase, User, UserService
from django.db import IntegrityError

sample_file = 'scripts/data/samples/Test Document.txt'

class TestFolderService(UnitasTestCase):
	"""Tests for FolderService class.
	"""

	def setUp(self):
		"""Create root folder, home folder and admin user for use in tests.
		"""
		self.root_folder = Folder.objects.get(
			name='Repository', parent_folder=None)
		self.home_folder = Folder.objects.get(
			name='Home',
			parent_folder=self.root_folder,
		)

		self.admin_user = User.objects.get(
			username='admin',
		)

	def testInit(self):
		"""Test init() for FolderService().
		"""
		folder_service = FolderService()
		self.assertEqual(folder_service.folder, None)

		folder_service = FolderService(self.root_folder)
		self.assertEqual(folder_service.folder, self.root_folder)
		self.assertEqual(folder_service.folder.name, self.root_folder.name)

	def testGetRootFolder(self):
		"""Test get_root_folder().
		"""
		folder_service = FolderService()
		self.assertEqual(folder_service.get_root_folder(), self.root_folder)

	def testGetHomeFolder(self):
		"""Test get_home_folder().
		"""
		folder_service = FolderService()
		self.assertEqual(folder_service.get_home_folder(), self.home_folder)

	def testGetUserHomeFolder(self):
		"""Test testGetUserHomeFolder().
		"""
		folder = Folder.objects.get(name=self.admin_user.username, parent_folder=self.home_folder)
		self.assertEqual(folder.name, self.admin_user.username)
		
	def testGetUserTrashcanFolder(self):
		"""Test get_user_trashcan_folder().
		"""
		admin_home_folder = Folder.objects.get(
			name=self.admin_user.username, parent_folder=self.home_folder)
		folder = Folder.objects.get(
			name='Trashcan', parent_folder=admin_home_folder)

	def testGetSharedDocumentsFolder(self):
		"""Test get_shared_documents_folder().
		"""
		folder_service = FolderService()
		folder = folder_service.get_shared_documents_folder()
		self.assertTrue(folder)
		self.assertEqual(folder.name, 'Shared Documents')

	def testGetProjectsFolder(self):
		""" Test get_projects_folder().
		"""
		folder_service = FolderService()
		folder = folder_service.get_projects_folder()
		self.assertTrue(folder)
		self.assertEqual(folder.name, 'Projects')

	def testSetFolder(self):
		"""Test set_folder().
		"""
		admin_home_folder = Folder.objects.get(
			name=self.admin_user.username, parent_folder=self.home_folder)
		folder_service = FolderService()
		folder_service.set_folder(admin_home_folder)
		self.assertEqual(folder_service.folder.name, self.admin_user.username)

	def testSetUser(self):
		"""Test set_user().
		"""
		folder_service = FolderService()
		folder_service.set_user(self.admin_user)
		self.assertEqual(folder_service.user.username, self.admin_user.username)

	def testCreateFolder(self):
		"""Test create().
		"""
		folder_service = FolderService()
		folder1 = folder_service.create(
			'Folder1',
			self.root_folder,
			self.admin_user,
			title='Folder #1',
			description='This is folder #1',
		)
		self.assertEqual(folder1.name, 'Folder1')
		self.assertEqual(folder1.title, 'Folder #1')

	def testUpdateFolder(self):
		"""Test update().
		"""
		folder_service = FolderService()
		folder1 = folder_service.create(
			'Folder1',
			self.root_folder,
			self.admin_user,
			title='Folder #1',
			description='This is folder #1',
		)
		self.assertEqual(folder_service.folder.name, 'Folder1')

		folder_service.update(
			name='Folder 1',
			title='Folder # 1',
		)

		self.assertEqual(folder_service.folder.name, 'Folder 1')
		self.assertEqual(folder_service.folder.title, 'Folder # 1')

	def testUpdateExcludedFields(self):
		"""Test update() using excluded fields.
		"""
		folder_service = FolderService()
		olderService = FolderService()
		folder1 = folder_service.create(
			'Folder1',
			self.root_folder,
			self.admin_user,
			title='Folder #1',
			description='This is folder #1',
		)
		
		user_service = UserService()
		testuser1 = user_service.create(
			'testuser1',
			'testuser1',
			'testuser1@localhost',
		)

		folder_service.set_folder(folder1)
		folder_service.update(
			owner=testuser1
		)

		self.assertNotEqual(folder_service.folder.owner, testuser1)
		self.assertEqual(folder_service.folder.owner, self.admin_user)

		doc_model_type = ModelType.objects.get(name='document')
		folder_service.update(
			model_type=doc_model_type
		)

		self.assertNotEqual(folder_service.folder.model_type, doc_model_type)
		self.assertEqual(folder_service.folder.model_type.name, 'folder')


	def testGetChildren(self):
		"""Test get_children().
		"""
		folder_service = FolderService()
		document_service = DocumentService()

		folder1 = folder_service.create(
			'Folder1',
			self.root_folder,
			self.admin_user,
			title='Folder #1',
			description='This is folder #1',
		)
		folder11 = folder_service.create(
			'Folder11',
			folder1,
			self.admin_user,
			title='Folder #11',
			description='This is folder #11',
		)
		folder12 = folder_service.create(
			'Folder12',
			folder1,
			self.admin_user,
			title='Folder #12',
			description='This is folder #12',
		)
		document1 = document_service.create(
			'Test1.txt',
			folder1,
			sample_file,
			self.admin_user,
		)
		document2 = document_service.create(
			'Test2.txt',
			folder1,
			sample_file,
			self.admin_user,
		)

		folder_service.set_folder(folder1)
		children = folder_service.get_children()
		"""
		self.assertEqual(children['folders'][0].name, 'Folder12')
		self.assertEqual(children['folders'][1].name, 'Folder11')
		self.assertEqual(children['documents'][0].name, 'Test2.txt')
		self.assertEqual(children['documents'][1].name, 'Test1.txt')
		"""

	def testMove(self):
		"""Test move().
		"""
		folder_service = FolderService()
		folder1 = folder_service.create(
			'Folder1',
			self.root_folder,
			self.admin_user,
		)
		folder2 = folder_service.create(
			'Folder2',
			self.root_folder,
			self.admin_user,
		)
		folder_service.move(folder1)

		self.assertEqual(folder_service.folder.parent_folder, folder1)

	def testCopy(self):
		"""Test copy().
		"""
		folder_service = FolderService()
		document_service = DocumentService()

		folder1 = folder_service.create(
			'Folder1',
			self.root_folder,
			self.admin_user,
		)
		folder2 = folder_service.create(
			'Folder2',
			self.root_folder,
			self.admin_user,
		)
		document1 = document_service.create(
			'Document1.txt',
			folder2,
			sample_file,
			self.admin_user,
		)
		folder_service.copy(folder1)

		origFolder2 = Folder.objects.get(
			name='Folder2',
			parent_folder=self.root_folder,
		)
		self.assertTrue(origFolder2)

		folder2_copy = Folder.objects.get(
			name='Folder2',
			parent_folder=folder1
		)
		self.assertTrue(folder2_copy)

		orig_document1 = Document.objects.get(
			name='Document1.txt',
			parent_folder=origFolder2,
		)
		self.assertTrue(orig_document1)

		document1_copy = Document.objects.get(
			name='Document1.txt',
			parent_folder=folder2_copy,
		)
		self.assertTrue(document1_copy)

	def testSetGetProperty(self):
		"""Test getters and setters for property.
		"""
		folder_service = FolderService()

		folder1 = folder_service.create(
			'Folder1',
			self.root_folder,
			self.admin_user,
		)
		folder_service.set_property({'key1':'value1'})
		self.assertEqual(folder_service.get_property('key1'), 'value1')

	def testSetGetProperties(self):
		"""Test getters and setters for property with multiple key/value pairs.
		"""
		folder_service = FolderService()

		folder1 = folder_service.create(
			'Folder1',
			self.root_folder,
			self.admin_user,
		)

		folder_service.set_properties(
			[
				{'key1': 'value1'},
				{'key2': 'value2'},
				{'key3': 'value3'},
			]
		)

		properties = folder_service.get_properties()
		self.assertEqual(properties[0].key, 'key3')
		self.assertEqual(properties[0].value, 'value3')
		self.assertEqual(properties[1].key, 'key2')
		self.assertEqual(properties[1].value, 'value2')
		self.assertEqual(properties[2].key, 'key1')
		self.assertEqual(properties[2].value, 'value1')

	def testCreateUserFolders(self):
		"""Test create_user_folders().
		"""
		testuser = User()
		testuser.username = 'testuser'
		testuser.email = 'testuser@localhost'
		testuser.set_password('testuser')
		testuser.save()

		folder_service = FolderService()
		folder_service.create_user_folders(testuser)

		user_home_folder = Folder.objects.get(
			name=testuser.username,
			parent_folder = self.home_folder,
		)
		self.assertTrue(user_home_folder)

		user_trashcan_folder = Folder.objects.get(
			name='Trashcan',
			parent_folder=user_home_folder,
		)
		self.assertTrue(user_trashcan_folder)

		user_restored_docs_folder = Folder.objects.get(
			name='Restored Documents',
			parent_folder=user_home_folder,
		)
		self.assertTrue(user_restored_docs_folder)

	def testSetOwner(self):
		"""Test set_owner().
		"""
		user_service = UserService()
		testuser = user_service.create(
			'testuser',
			'testuser',
			'testuser@localhot',
		)

		folder_service = FolderService()
		folder = folder_service.create(
			'Folder1',
			self.root_folder,
			testuser,
		)

		folder_service.set_owner(self.admin_user)
		self.assertEqual(folder_service.folder.owner, self.admin_user)

	def testSameNameFolderCannotBeCreatedInSameSpace(self):

		folder_service = FolderService()

		result = folder_service.create(
			'Folder1',
			self.root_folder,
			self.admin_user,
		)
		self.assertTrue(result)

		result = folder_service.create(
			'Folder1',
			self.root_folder,
			self.admin_user,
		)
		self.assertFalse(result)

	def testDeepDeleteAndTrashcanizeDocuments(self):

		folder_service = FolderService()
		document_service = DocumentService()

		folder1 = folder_service.create(
			'Folder1',
			self.root_folder,
			self.admin_user,
		)

		folder11 = folder_service.create(
			'Folder11',
			folder1,
			self.admin_user,
		)

		folder111 = folder_service.create(
			'Folder111',
			folder11,
			self.admin_user,
		)

		document11 = document_service.create(
			'Document11.txt',
			folder11,
			sample_file,
			self.admin_user,
		)

		document111 = document_service.create(
			'Document111.txt',
			folder111,
			sample_file,
			self.admin_user,
		)

		folder_service.set_folder(folder1)
		folder_service.delete()

		folder1 = Folder.objects.get(name='Folder1')
		folder11 = Folder.objects.get(name='Folder11')
		folder111 = Folder.objects.get(name='Folder111')
		document11 = Document.objects.get(name='Document11.txt')
		document111 = Document.objects.get(name='Document111.txt')

		self.assertEqual(folder1.parent_folder.name, 'Trashcan')

	def testIsInPathOfUserTrashcan(self):

		folder_service = FolderService()
		folder_service.set_user(self.admin_user)
		folder1 = folder_service.create(
			'Folder1',
			folder_service.get_user_home_folder(),
			self.admin_user,
		)

		folder2 = folder_service.create(
			'Folder2',
			folder_service.get_user_trashcan_folder(),
			self.admin_user,
		)

		folder3 = folder_service.create(
			'Folder3',
			folder2,
			self.admin_user,
		)

		self.assertFalse(folder_service.is_in_trashcan_path(self.root_folder))
		self.assertFalse(folder_service.is_in_trashcan_path(folder1))
		self.assertTrue(folder_service.is_in_trashcan_path(folder2))
		self.assertTrue(folder_service.is_in_trashcan_path(folder3))

	def testChildFoldersStickySystem(self):

		folder_service = FolderService()
		folder_service.set_user(self.admin_user)
		user_home_folder = folder_service.get_user_home_folder()
		
		folder1 = folder_service.create(
			'Folder1',
			user_home_folder,
			self.admin_user,
		)

		folder2 = folder_service.create(
			'Another Folder',
			user_home_folder,
			self.admin_user,
		)

		folder_service.set_folder(user_home_folder)
		children = folder_service.get_children(sticky_system_folders=True)

		self.assertEqual(
			children['folders'][0].name,
			'Trashcan',
		)
