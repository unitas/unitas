"""Unit tests for BootstrapScriptsTest class
"""

from apps.repository.tests import Folder, User, UnitasTestCaseNoMimeTypes

class BootstrapScriptsTest(UnitasTestCaseNoMimeTypes):
	"""Tests for bootstrap scripts.
	"""

	def setUp(self):
		pass

	def testCreateNobodyUser(self):
		"""Tests if nobody user was created.
		"""
		user = User.objects.get(username='NOBODY')
		self.assertEqual(user.username, 'NOBODY')
		self.assertNotEqual(user.username, 'obody')

	def testCreateSystemUser(self):
		"""Tests if system user was created.
		"""
		user = User.objects.get(username='SYSTEM')
		self.assertEqual(user.username, 'SYSTEM')
		self.assertNotEqual(user.username, 'ystem')

	def testCreateEverybodyUser(self):
		"""Tests if everybody user was created.
		"""
		user = User.objects.get(username='EVERYBODY')
		self.assertEqual(user.username, 'EVERYBODY')
		self.assertNotEqual(user.username, 'verybody')

	def testCreateRootFolder(self):
		"""Tests if root folder was created.
		"""
		folder = Folder.objects.get(name='Repository', parent_folder=None)
		self.assertEqual(folder.name, 'Repository')
		self.assertNotEqual(folder.name, 'OOT')

	def testCreateHomeFolder(self):
		"""Tests if home folder was created.
		"""
		root_folder = Folder.objects.get(name='Repository', parent_folder=None)
		folder = Folder.objects.get(name='Home', parent_folder=root_folder)
		self.assertEqual(folder.name, 'Home')
		self.assertNotEqual(folder.name, 'OME')

	def testCreateSharedFolder(self):
		"""Tests if shared documents folder was created.
		"""
		root_folder = Folder.objects.get(name='Repository', parent_folder=None)
		folder = Folder.objects.get(name='Shared Documents', parent_folder=root_folder)
		self.assertEqual(folder.name, 'Shared Documents')
		self.assertNotEqual(folder.name, 'hared Documents')

	def testCreateAdminHomeFolder(self):
		"""Tests if admin's home folder was created.
		"""
		root_folder = Folder.objects.get(name='Repository', parent_folder=None)
		home_folder = Folder.objects.get(name='Home', parent_folder=root_folder)
		folder = Folder.objects.get(name='admin', parent_folder=home_folder)
		self.assertEqual(folder.name, 'admin')
		self.assertNotEqual(folder.name, 'dmin')

	def testCreateAdminTraschanFolder(self):
		"""Tests if admin's trashcan folder was created.
		"""
		root_folder = Folder.objects.get(name='Repository', parent_folder=None)
		home_folder = Folder.objects.get(name='Home', parent_folder=root_folder)
		user_home_folder = Folder.objects.get(name='admin', parent_folder=home_folder)
		folder = Folder.objects.get(name='Trashcan', parent_folder=user_home_folder)
		self.assertEqual(folder.name, 'Trashcan')
		self.assertNotEqual(folder.name, 'rashcan')


	