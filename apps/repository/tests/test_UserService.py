"""Unit tests for UserService class
"""

from apps.repository.tests import authenticate, DocumentService, Folder, FolderService, UnitasTestCaseNoMimeTypes, User, UserService

sample_file = 'scripts/data/samples/Test Document.txt'

class TestUserService(UnitasTestCaseNoMimeTypes):
	"""Tests for UserService class.
	"""

	def setUp(self):
		"""Create root folder and admin user for use in tests.
		"""
		self.admin_user = User.objects.get(username='admin')
		self.root_folder = Folder.objects.get(name='Repository', parent_folder=None)

	def testInit(self):
		"""Test init() for UserService().
		"""
		user_service = UserService()
		self.assertFalse(user_service.user)
		self.assertFalse(user_service.profile)

	def testInitByUser(self):
		"""Test init() for UserService() with params.
		"""
		user_service = UserService(self.admin_user)
		self.assertTrue(user_service.user)
		self.assertTrue(user_service.profile)
		self.assertEqual(user_service.user.username, 'admin')

	def testGetUser(self):
		"""Test get_user().
		"""
		user_service = UserService(self.admin_user)
		self.assertEqual(user_service.get_user(), self.admin_user)

		user_service = UserService()
		user_service.set_user(self.admin_user)
		self.assertEqual(user_service.get_user(), self.admin_user)

	def testGetNobodyUser(self):
		"""Test get_nobody_user().
		"""
		user_service = UserService()
		nobody_user = user_service.get_nobody_user()
		self.assertEqual(nobody_user.username, 'NOBODY')

	def testGetAdminUser(self):
		"""Test get_admin_user().
		"""
		user_service = UserService()
		admin_user = user_service.get_admin_user()
		self.assertEqual(admin_user, self.admin_user)

	def testCreate(self):
		"""Test create().
		"""
		user_service = UserService()
		user_service.create(
			'testuser',
			'testuser',
			'testuser@localhost',
		)

		user = User.objects.get(username='testuser')
		self.assertEqual(user.username, 'testuser')
		self.assertEqual(user.email, 'testuser@localhost')

	def testUpdate(self):
		"""Test update().
		"""
		user_service = UserService()
		testuser = user_service.create(
			'testuser',
			'testuser',
			'testuser@localhost',
		)

		user_service.update(
			email='testuser1@localhost',
		)

		user = User.objects.get(username='testuser')
		self.assertEqual(user.email, 'testuser1@localhost')

	def testUpdateExcludedFields(self):
		"""Test update() with excluded fields.
		"""
		user_service = UserService()
		testuser = user_service.create(
			'testuser',
			'testuser',
			'testuser@localhost',
		)
		user_service.update(
			password='blah',
			username='admin2',
		)

		self.assertNotEqual(user_service.user.password, 'blah')
		self.assertNotEqual(user_service.user.username, 'admin2')
		self.assertEqual(user_service.user.username, 'testuser')

	def testDelete(self):
		"""Test delete().
		"""
		nobody_user = User.objects.get(username='NOBODY')
		user_service = UserService()
		testuser = user_service.create(
			'testuser',
			'testuser',
			'testuser@localhost',
		)

		folder_service = FolderService()
		folder_service.create(
			'Folder1',
			self.root_folder,
			testuser,
		)

		user_service.delete()
		folder1 = Folder.objects.get(
			name='Folder1', parent_folder=self.root_folder)
		self.assertEqual(folder1.owner, nobody_user)

	def testSetPassword(self):
		"""Test set_password().
		"""
		user_service = UserService()
		testuser = user_service.create(
			'testuser',
			'testuser',
			'testuser@localhost',
		)

		user_service.set_password('secret')

		user = authenticate(username='testuser', password='secret')
		self.assertEqual(user.username, 'testuser')

	def testDeleteSystemUsers(self):
		"""Testing possibility of deleting system users.
		"""

		user_service = UserService()

		admin_user = user_service.get_admin_user()
		nobody_user = user_service.get_nobody_user()
		everybody_user = user_service.get_everybody_user()
		system_user = user_service.get_system_user()

		for user in (admin_user, nobody_user, everybody_user, system_user):
			user_service.set_user(user)
			user_service.delete()
			self.assertTrue(User.objects.get(id=user.id))