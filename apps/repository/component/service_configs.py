"""Configs that can apply to any service lib modules.
"""

document_update_excluded_fields = (
	'owner', 'mime_type', 'is_deleted', 'model_type',
)
"""Fields that cannot be updated using DocumentService().update(...)
"""

folder_update_excluded_fields = (
	'owner', 'model_type',
)
"""Fields that cannot be updated using FolderService().update(...)
"""

user_update_excluded_fields = (
	'username', 'password', 'is_superuser', 'is_staff',
)
"""Fields that cannot be updated using UserService().update(...)
"""