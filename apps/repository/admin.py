"""Django admin models for repository app models.
"""

from django.contrib import admin
from models import Document, DocumentProperty, DocumentVersion, Folder, FolderProperty, MimeType, SystemSetting, UserProfile
from lib.service.folder import FolderService
from django.template.defaultfilters import escape
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe
import logging

logger = logging.getLogger(__name__)

class BaseModelAdmin(admin.ModelAdmin):
	""" Displays a list of fields for Document and Folder objects. 
	"""
	list_display = (
		'uuid', 'name', 'title', 'description',
		'parent_folder', 'created', 'modified', 
	)

class FolderModelAdmin(BaseModelAdmin):
	"""Page for Folder objects.
	"""
	fields = (
		'uuid', 'name', 'created', 'title', 'description', 
		'model_type', 'parent_folder', 'get_parent_folder', 'owner', 
		'is_shared', 'is_system', 'is_hidden', 'get_full_path', 
		'get_child_folders', 'get_child_documents',
	)

	readonly_fields = (
		'uuid', 'created', 'get_parent_folder', 'get_full_path', 'get_child_folders',
		'get_child_documents',
	)

	def get_parent_folder(self, obj):
		"""Gets parent_folder for Folder object.
		"""
		parent_folder = obj.parent_folder
		return mark_safe(
			'<a href="/admin/repository/folder/{}/change/">{}</a>'.format(
				parent_folder.uuid,
				parent_folder,
			)
		)

	def get_full_path(self, obj):
		"""Gets full path for Folder object.
		"""
		return obj.get_full_path(obj)

	def get_child_folders(self, obj):
		"""Gets child folders for Folder object. Returns each folder name with a link to child folders.
		"""
		folder_service = FolderService()
		folder_service.set_folder(obj)
		child_folders = folder_service.get_children()['folders']
		
		if len(child_folders) > 0:
			return mark_safe(', '.join(
				[
					'<a href="/admin/repository/folder/{}/change/">{}</a>'.format(folder.uuid, folder.name) for folder in child_folders
				]
			))
		else:
			return 'None'

	def get_child_documents(self, obj):
		"""Gets child documents for Folder object. Returns each document with a link to child documents.
		"""
		folder_service = FolderService()
		folder_service.set_folder(obj)
		child_docs = folder_service.get_children()['documents']
		
		if len(child_docs) > 0:
			return mark_safe(', '.join(
				[
					"<a href=\"/admin/repository/document/{}/change/\">{}</a>".format(document.uuid, document.name) for document in child_docs
				]
			))
		else:
			return 'None'

	get_full_path.short_description = 'Full Path to this folder'
	get_child_folders.short_description = 'Child folders'
	get_child_documents.short_description = 'Child documents'
	get_parent_folder.short_description = 'Link to parent folder'

class UserProfileAdmin(admin.ModelAdmin):
	"""Page for UserProfile objects.
	"""
	fields = (
		'user', 'user_link', 'organization', 'job_title', 'location', 
		'biography', 'telephone', 'mobile_phone', 'skype', 'instant_msg',
		'company_address',
	)

	readonly_fields = (
		'user_link',
	)

	def user_link(self, obj):
		"""Returns a link for each user returned.
		"""
		return mark_safe(
			'<a href="/admin/auth/user/{}/change/">{}</a>'.format(
				obj.user.id, obj.user.username
			)
		)

	user_link.short_description = 'Link to user'

""" Registration of models """
admin.site.register(DocumentVersion)
admin.site.register(Document, BaseModelAdmin)
admin.site.register(DocumentProperty)
admin.site.register(Folder, FolderModelAdmin)
admin.site.register(FolderProperty)
admin.site.register(MimeType)
admin.site.register(SystemSetting)
admin.site.register(UserProfile, UserProfileAdmin)