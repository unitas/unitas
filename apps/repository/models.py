"""Models file for apps.repository app.
"""

from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
import uuid
import logging

logger = logging.getLogger(__name__)

def get_file_path(instance, filename):
    """ Returns the os path where content files are created and stored on disk"""

    # Importing these time functions so that we can create a folder tree with year/month/day/hour/minute/etc.
    from time import gmtime, strftime
    import os
    ext = 'bin'
    filename = "{}.{}".format(uuid.uuid4(), ext)
    return os.path.join(strftime(instance.directory_string_var, gmtime()), filename)

class BaseModel(models.Model):
    """ Abstract model for Document and Folder models. """
    
    """ All files and folders must have a name. """
    name = models.CharField(
        'Name',
        max_length=255,
    )
    
    """ All files and folders use the uuid instead of int id. """
    uuid = models.UUIDField(
        'UUID',
        primary_key=True, 
        default=uuid.uuid4,
        editable=False
    )
    
    """ A generic title can be optionally given. """
    title = models.CharField(
        'Title',
        max_length=255,
        null=True,
        blank=True,
    )
    
    """ A longer description of the file/folder. """
    description = models.TextField(
        'Description',
        null=True,
        blank=True,
    )
    
    """ The folder that "contains" the file/folder. """
    parent_folder = models.ForeignKey(
        'Folder',
        null=True,
        blank=True,
    )
    
    """ Datetime when created. """
    created = models.DateTimeField(
        auto_now_add=True,
    )
    
    """ Datetime when last modified. """
    modified = models.DateTimeField(
        auto_now=True,
    )

    """ Differentiater between document/folder. """
    model_type = models.ForeignKey('ModelType')

    """ User who owns document/folder. """
    owner = models.ForeignKey(User)

    def get_full_path(self, cls, path='', delimiter='/', prepend='/'):
        """ Gets full system path for document/folder. Note that this is a recursive method. """
        if hasattr(cls, 'parent_folder'):
            path = getattr(cls, 'name') + delimiter + path
        if hasattr(cls.parent_folder, 'parent_folder'):
            path = self.get_full_path(cls.parent_folder, path, delimiter)

        if not path.startswith('/'):
            path = '/' + path.rstrip('/')
        
        return path
    
    class Meta:
        abstract = True


class Property(models.Model):
    """ Base model for dynamic properties that can be assigned to documents or folders. """

    """ Key for the key/value property. """
    key = models.CharField(
        'Key',
        max_length=255,
    )

    """ Value for the key/value property. """
    value = models.TextField(
        'Value',
    )

    class Meta:
        abstract=True


class ModelType(models.Model):
    """ The model type assigned for documents or folders. """
    name = models.CharField(
        'Name',
        max_length=255,
        unique=True,
    )

    class Meta:
        verbose_name = 'Model Type'
        verbose_name_plural = 'Model Types'
        db_table = 'repo_model_type'

    def __unicode__(self):
        return self.name


class MimeType(models.Model):
    """ Each document will be assigned a mimetype based on the document name's extension. For example, test.txt will be assigned an application/txt mimetype. """

    """ Name assigned for the mimetype. """
    name = models.CharField(
        'Name',
        max_length=100
    )

    """ Officially and/or widely known internet name for the mimetype. For example: a PDF would be application/pdf. """
    designation = models.CharField(
        'Designation',
        max_length=100,
    )

    """ Usual file extension for said mimetype. .txt for text, .pdf for PDF document, etc. """
    extension = models.CharField(
        'Extension',
        max_length=20,
        unique=True,
    )

    """ Extra description for mimetype if needed. """
    description = models.TextField(
        'Description',
        null=True,
        blank=True,
    )

    class Meta:
        """ All mimetypes are unique by all 4 fields. """
        unique_together = (
            ('name', 'designation', 'extension', 'description'),
        )
        verbose_name = 'Mimetype'
        verbose_name_plural = 'Mimetypes'
        db_table = 'repo_mimetype'

    def __unicode__(self):
        return unicode('{} - {}'.format(self.name, self.extension))


class Folder(BaseModel):
    """ Folder model. Contains documents and other folders. """

    """ System folders can't be deleted. """
    is_system = models.BooleanField(
        'Is System?',
        default=False,
    )

    """ Hidden folders won't show up in UI listings. """
    is_hidden = models.BooleanField(
        'Is Hidden?',
        default=False,
    )

    class Meta:
        verbose_name = 'Folder'
        verbose_name_plural = 'Folders'
        db_table = 'repo_folder'
        
    def __unicode__(self):
        return self.get_full_path(self)
    
    
class Document(BaseModel):
    """ Content file. """

    """ The mimetype for a particular document determined initially by its file extension. """
    mime_type = models.ForeignKey(MimeType)

    """ Is Document shared? """
    is_shared = models.BooleanField(
        default=False,
    )

    class Meta:
        verbose_name = 'Document'
        verbose_name_plural = 'Documents'
        db_table = 'repo_document'
    
    def __unicode__(self):
        return self.get_full_path(self)


class DocumentProperty(Property):
    """ Dynamic property for documents. """
    document = models.ForeignKey(
        Document,
    )

    class Meta:
        unique_together = (
            ('key', 'document'),
        )
        verbose_name = 'Document Property'
        verbose_name_plural = 'Document Properties'
        db_table = 'repo_document_prop'

    def __unicode__(self):
        return '{} | ["{}":"{}"]'.format(
            self.document,
            self.key,
            self.value,
        )


class FolderProperty(Property):
    """ Dynamic property for folders. """
    folder = models.ForeignKey(
        Folder,
    )

    class Meta:
        unique_together = (
            ('key', 'folder'),
        )

        verbose_name = 'Folder Property'
        verbose_name_plural = 'Folder Properties'
        db_table = 'repo_folder_prop'

    def __unicode__(self):
        return '{} | ["{}":"{}"]'.format(
            self.folder,
            self.key,
            self.value,
        )
    
  
class DocumentVersion(models.Model):
    """ Document version for parent document. Each Document model can have multiple versions and multiple "documents"."""

    """ Directory based on time. See get_file_path(). """
    directory_string_var = 'content/%Y/%m/%d/%H/%M/%S/'

    uuid = models.UUIDField(
        'UUID',
        primary_key=True, 
        default=uuid.uuid4,
        editable=False
    )

    """ Actual version name string like: 1.0 or 0.1. """
    version = models.CharField(
        'Version',
        max_length=11,
    )
    
    """ When created. There will be no "modified" because new document versions will be created when documents are "modified." """
    created = models.DateTimeField(
        auto_now_add=True,
    )
    
    """ Parent document meta-data. """
    parent_document = models.ForeignKey(
        Document,
    )
    
    """ The actual content file for this version. """
    content_file = models.FileField(
        'Content File',
        upload_to=get_file_path,
    )

    comment = models.TextField(
        'Comment',
        null=True,
        blank=True,
    )

    class Meta:
        verbose_name = 'Document Version'
        verbose_name_plural = 'Document Versions'
        db_table = 'repo_document_version'
    
    def __unicode__(self):
        return '{}/v{}'.format(self.parent_document.name, self.version)
    
class UserProfile(models.Model):
    """ UserProfile extends the User model allowing for extra fields that do not come OOTB with Django. """

    """ Parent User model. """
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
    )

    """ Some miscellaneous fields. """
    organization = models.CharField(
        'Organization',
        max_length=255,
        null=True,
        blank=True,
    )

    job_title = models.CharField(
        'Job Title',
        max_length=255,
        null=True,
        blank=True,
    )

    location = models.CharField(
        'Location',
        max_length=255,
        null=True,
        blank=True,
    )

    biography = models.TextField(
        'Biography',
        null=True,
        blank=True,
    )

    telephone = models.CharField(
        'Telephone',
        max_length=20,
        null=True,
        blank=True,
    )

    mobile_phone = models.CharField(
        'Mobile phone',
        max_length=20,
        null=True,
        blank=True,
    )

    skype = models.CharField(
        'Skype username',
        max_length=50,
        null=True,
        blank=True,
    )

    instant_msg = models.CharField(
        'Instant Message',
        max_length=50,
        null=True,
        blank=True,
    )

    company_address = models.TextField(
        'Company Address',
        null=True,
        blank=True,
    )

    class Meta:
        verbose_name = 'User Profile'
        verbose_name_plural = 'User Profiles'
        db_table = 'repo_user_profile'

    def __unicode__(self):
        return '{}'.format(self.user.username)

class SystemSetting(Property):

    class Meta:
        verbose_name = 'System Setting'
        verbose_name_plural = 'System Settings'
        db_table = 'repo_system_setting'

    def __unicode__(self):
        return '["{}":"{}"]'.format(
            self.key,
            self.value,
        )