"""No views for apps.repository application.
"""

from django.shortcuts import render
import logging

logger = logging.getLogger(__name__)