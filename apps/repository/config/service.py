"""Service property settings.
"""

default_version='1.0'
"""Default starting version for all document versions.
"""

default_mime_type='application/octet-stream'
"""Default mimetype used when document without extension is imported.
"""