"""FolderService class
"""

from __future__ import absolute_import

from apps.repository.lib.service import Document, DocumentService, FDService, Folder, FolderProperty, folder_update_excluded_fields, logging, ModelType, User

logger = logging.getLogger(__name__)

class FolderService(FDService):
	"""Wrapper for working Folder, Document, FolderProperty and ModelType models. 
	"""

	def __init__(self, folder=None):
		"""Set default folder if id is known. Set default folder to None if it is unknown. 
		"""
		self.set_folder(folder)

	def get_root_folder(self):
		"""Return the system root folder. 
		"""
		root_folder = Folder.objects.get(name='Repository', parent_folder=None)
		return root_folder

	def get_home_folder(self):
		"""Return the system /home folder. 
		"""
		home_folder = Folder.objects.get(name='Home', parent_folder=self.get_root_folder())
		return home_folder

	def get_user_home_folder(self):
		"""Return the default user's home folder: /home/myuser 
		"""

		user_home_folder = Folder.objects.get(
			owner=self.user, 
			name=self.user.username, 
			parent_folder=self.get_home_folder()
		)
		return user_home_folder

	def get_user_trashcan_folder(self):
		"""Return the default user's trashcan folder: /home/myuser/trashcan 
		"""
		user_trashcan_folder = Folder.objects.get(
			name='Trashcan', parent_folder=self.get_user_home_folder())
		return user_trashcan_folder

	def get_shared_documents_folder(self):
		"""Return the global Shared Documents folder. 
		"""
		sharedDocumentFolder = Folder.objects.get(
			name='Shared Documents',
			parent_folder=self.get_root_folder(),
		)
		return sharedDocumentFolder

	def get_restored_docs_folder(self):
		folder = Folder.objects.get(
			name='Restored Documents',
			parent_folder=self.get_user_home_folder(),
		)
		return folder

	def get_projects_folder(self):
		"""Return the system Projects folder.
		"""
		projects_folder = Folder.objects.get(
			name='Projects',
			parent_folder=self.get_root_folder(),
		)
		return projects_folder

	def set_folder(self, folder):
		"""Set the default folder for the wrapper. 
		"""
		try:
			self.folder = folder
			try:
				folder_uuid = self.folder.uuid
			except AttributeError:
				folder_uuid = None
		except Folder.DoesNotExist:
			self.folder = None
			folder_uuid = None
		self.object = self.folder

	def get_folder(self, folder):
		"""Return a folder object.
		"""
		return Folder.objects.get(uuid=folder.uuid)

	def create(self, name, parent_folder, owner, **kwargs):
		"""Create a folder and return the new folder. 
		"""

		try:
			folder_exists_in_same_space = Folder.objects.get(
				name=name,
				parent_folder=parent_folder
			)
		except Folder.DoesNotExist:
			folder_exists_in_same_space = None

		if folder_exists_in_same_space:
			return False

		folder_type = ModelType.objects.get(name='folder')

		folder = Folder.objects.create(
			name=name,
			parent_folder=parent_folder,
			owner=owner,
			model_type=folder_type,
		)

		for k, v in kwargs.items():
			setattr(folder, k, v)
		folder.save()
		self.set_folder(folder)
		return folder

	def update(self, **kwargs):
		"""Update the folder's metadata. Except owner and model_type.
		"""
		for k, v in kwargs.items():
			if k not in folder_update_excluded_fields:
				setattr(self.folder, k, v)
			else:
				pass
				logger.warning('Field "{}" is an excludedField. Cannot update.'.format(
						k, k
					)
				)
		self.folder.save()

	def get_children(self, sticky_system_folders=False):
		"""Return documents and subfolders for the default folder. 
		"""

		if sticky_system_folders:
			folders = Folder.objects.filter(
				parent_folder=self.folder, 
				is_hidden=False
			).order_by('-is_system', 'name',)
		else:
			folders = Folder.objects.filter(
				parent_folder=self.folder, 
				is_hidden=False
			).order_by('name',)			

		documents = Document.objects.filter(
			parent_folder=self.folder
		).order_by('name')
		
		children = {
			'folders': folders,
			'documents': documents,
		}
		return children		

	def delete(self):
		"""Deletes a folder and its content. Container documents' parent_folder is set as the owner's trashcan folder.
		"""
		from apps.repository.lib.service.user import UserService

		self.set_user(self.folder.owner)
		try:
			self.folder.parent_folder = self.get_user_trashcan_folder()
		except Folder.DoesNotExist:
			self.set_user(UserService().get_admin_user())
			self.folder.parent_folder = self.get_user_trashcan_folder()
		self.folder.save()

	def move(self, destination_folder):
		"""Move default folder to destination folder. 
		"""
		self.folder.parent_folder = destination_folder
		self.folder.save()
		self.set_folder(self.folder)

	def copy(self, destination_folder):
		"""Copy default folder to destination folder. 
		"""
		children = self.get_children()

		destination_folder = destination_folder

		new_folder = self.folder
		new_folder.pk = None
		new_folder.parent_folder = destination_folder 
		new_folder.save()

		for document in children['documents']:
			document_service = DocumentService(document)
			document_service.copy(new_folder)

		for folder in children['folders']:
			self.set_folder(folder)
			self.copy(new_folder)

	def set_property(self, key_value_pair):
		"""Set a key pair property for a folder. 
		"""
		for key in key_value_pair:
			folder_property = FolderProperty()
			folder_property.key = key
			folder_property.value = key_value_pair[key]
			folder_property.folder = self.folder
			folder_property.save()
		return True

	def get_property(self, key):
		"""Get a key pair property for a folder by its key. 
		"""
		folder_property = FolderProperty.objects.get(
			key=key,
			folder=self.folder,
		)
		return folder_property.value

	def set_properties(self, key_value_pairs):
		"""Set a list of key pair properties for a folder. 
		"""
		for key_value_pair in key_value_pairs:
			for key in key_value_pair:
				folder_property = FolderProperty()
				folder_property.key = key
				folder_property.value = key_value_pair[key]
				folder_property.folder = self.folder
				folder_property.save()
		return True

	def get_properties(self):
		"""Get a list of all properties set for the default folder. 
		"""
		folder_properties = FolderProperty.objects.filter(
			folder=self.folder,	
		)
		return folder_properties

	def create_user_folders(self, user):
		"""Create user home folders.
		"""
		user = user

		system_user = User.objects.get(username='SYSTEM')

		home_folder = self.create(
			user.username,
			self.get_home_folder(),
			user,
			title=user.username,
			description='Home folder for user: {}'.format(user.username),
			is_system=True,
		)

		self.create(
			'Trashcan',
			home_folder,
			system_user,
			title='{} user trashcan'.format(user.username),
			description='Trashcan folder for user: {}'.format(user.username),
			is_system=True,
			is_hidden=True,
		)

		self.create(
			'Restored Documents',
			home_folder,
			system_user,
			title='{} user trashcan'.format(user.username),
			description='Trashcan folder for user: {}'.format(user.username),
			is_system=True,
			is_hidden=True,
		)			

	def is_in_trashcan_path(self, folder):
		
		if folder == self.get_user_trashcan_folder():
			return True

		if hasattr(folder, 'parent_folder'):
			return self.is_in_trashcan_path(folder.parent_folder)

		return False