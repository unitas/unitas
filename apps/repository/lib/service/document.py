"""DocumentService class
"""

from apps.repository.lib.service import Document, DocumentProperty, document_update_excluded_fields, DocumentVersion, FDService, File, logging, MimeType, ModelType, User
from apps.repository.config.service import default_version, default_mime_type
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.db import IntegrityError


logger = logging.getLogger(__name__)

class DocumentService(FDService):
	"""Wrapper for working with Document, DocumentVersion, Folder and MimeType models. 
	"""

	def __init__(self, document=None):
		"""Sets default document if id is given. 
		"""
		self.set_document(document)

	def set_document(self, document):
		"""Explicitly sets default document based on id. If the document cannot be found, the default document is set to None. 
		"""
		self.document = document

		self.object = self.document

	def get_document(self, document):
		"""Get document object.
		"""
		return Document.objects.get(uuid=document.uuid)

	def get_latest_version(self):
		"""Gets latest version of document version. 
		"""
		document_version = DocumentVersion.objects.filter(
			parent_document=self.document
		).order_by('-created')[0]
		return document_version

	def get_new_version(self, mode):
		"""Get the next version string based on mode: major or minor. Major will increase 1.0 to 2.0 and minor will increase 1.0 to 1.1. 
		"""
		latest_version = self.get_latest_version()
		major, minor = latest_version.version.split('.')
		if mode == 'major':
			major = int(major) + 1
		elif mode == 'minor':
			minor = int(minor) + 1
		new_version = '.'.join([str(major), str(minor),])
		return new_version

	def create(self, name, parent_folder, content_file, owner, **kwargs):
		"""Create a document and version. 
		"""

		try:
			document_exists_in_same_space = Document.objects.get(
				name=name,
				parent_folder=parent_folder
			)
		except Document.DoesNotExist:
			document_exists_in_same_space = None

		if document_exists_in_same_space:
			return False

		documentExtension = '.' + name.split('.')[-1]
		
		try:
			mime_type = MimeType.objects.get(
				extension=documentExtension,
			)
		except MimeType.DoesNotExist:
			mime_type = MimeType.objects.get(
				designation=default_mime_type,
			)

		document_type = ModelType.objects.get(name='document')

		document = Document.objects.create(
			name=name,
			parent_folder=parent_folder,
			mime_type=mime_type,
			model_type=document_type,
			owner=owner,
		)

		for k, v in kwargs.items():
			setattr(document, k, v)
		document.save()

		if isinstance(content_file, str):
			content_file=File(open(content_file))
		elif isinstance(content_file, InMemoryUploadedFile):
			content_file = content_file

		document_version = DocumentVersion.objects.create(
			version=default_version,
			parent_document=document,
			content_file=content_file,
			comment='Initial version',
		)	

		self.set_document(document)

		return document

	def update(self, **kwargs):
		"""Update a document's metadata. This won't update the user, mimetype, is_deleted flag or model type. 
		"""

		for k, v in kwargs.items():
			if k not in document_update_excluded_fields:
				setattr(self.document, k, v)
			else:
				pass
				logger.warning('Field "{}" is an excludedField. Cannot update.'.format(
						k
				))

		self.document.save()

	def add_new_version(self, content_file, mode, **kwargs):
		"""Add new version of a document by creating a new DocumentVersion. 
		"""
		document_version = DocumentVersion.objects.create(
			version=self.get_new_version(mode),
			parent_document=self.document,
			content_file=content_file,
		)

		for k,v in kwargs.items():
			
			setattr(document_version, k, v)

		document_version.save()

		return document_version

	def delete(self):
		"""Delete document by setting is_deleted to true. This will only make the user's trashcan folder as the parent folder. 
		"""
		from folder import FolderService
		folder_service = FolderService()
		folder_service.set_user(self.document.owner)

		self.document.is_deleted = True
		self.document.parent_folder = folder_service.get_user_trashcan_folder()
		self.document.save()

	def move(self, destination_folder):
		"""Move default document to the destination folder. 
		"""
		self.document.parent_folder = destination_folder
		self.document.save()
		self.set_document(self.document)

	def copy(self, destination_folder):
		"""Copy default document to the destination folder. 
		"""

		src_document_uuid = self.document.uuid

		new_document = self.document
		new_document.pk = None
		new_document.parent_folder = destination_folder
		new_document.description = 'Copy of document: {}'.format(self.document)
		new_document.save()

		src_parent_document = Document.objects.get(uuid=src_document_uuid)

		for new_version in DocumentVersion.objects.filter(parent_document=src_parent_document):
			new_version.pk = None
			new_version.parent_document = new_document
			new_version.save()

	def set_property(self, key_value_pair):
		"""Set a key pair property for a document. 
		"""
		for key in key_value_pair:
			document_property = DocumentProperty()
			document_property.key = key
			document_property.value = key_value_pair[key]
			document_property.document = self.document
			document_property.save()
		return True

	def get_property(self, key):
		"""Get a key pair property for a document by its key. 
		"""
		document_property = DocumentProperty.objects.get(
			key=key,
			document=self.document,
		)
		return document_property.value

	def set_properties(self, key_value_pairs):
		"""Set a list of key pair properties for a document. 
		"""
		for key_value_pair in key_value_pairs:
			for key in key_value_pair:
				document_property = DocumentProperty()
				document_property.key = key
				document_property.value = key_value_pair[key]
				document_property.document = self.document
				document_property.save()
		return True

	def get_properties(self):
		"""Get a list of all properties set for the default document. 
		"""
		document_properties = DocumentProperty.objects.filter(
			document=self.document,	
		)
		return document_properties

	def set_mime_type(self, mime_type):
		"""Set a mimetype for a document.
		"""
		self.document.mime_type = mime_type
		self.document.save()
		self.set_document(self.document)