"""Package do-noter for apps.repository.lib.service
"""

from __future__ import absolute_import

import logging

from django.core.files import File
from django.contrib.auth.models import User
from apps.repository.models import Document, DocumentProperty, DocumentVersion, Folder, FolderProperty, MimeType, ModelType, UserProfile
from apps.repository.component.service_configs import document_update_excluded_fields, folder_update_excluded_fields
from apps.repository.lib.service.repository import FDService
from apps.repository.lib.service.document import DocumentService
from apps.repository.lib.service.folder import FolderService
from unitas.settings import create_user_home_dir
from apps.repository.component.service_configs import user_update_excluded_fields
from apps.repository.lib.service.repository import RepoService
