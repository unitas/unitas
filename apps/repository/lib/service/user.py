"""UserService class
"""

from apps.repository.lib.service import create_user_home_dir, Document, Folder, FolderService, logging, RepoService, User, UserProfile, user_update_excluded_fields

logger = logging.getLogger(__name__)

class UserService(RepoService):
	"""Wrapper for working with User and UserProfile models. 
	"""

	def __init__(self, user=None):
		"""Set the default user and its associated profile. 
		"""
		self.set_user(user)
		self.set_profile(user)

	def set_profile(self, user):
		"""Explicitly set the user profile based on the user id. 
		"""
		if user:
			self.user = user
			try:
				self.profile = UserProfile.objects.get(user=self.user)
			except UserProfile.DoesNotExist:
				self.profile = UserProfile.objects.create(user=self.user)
		else:
			self.profile = None

	def get_user(self, user=None):
		"""Return the default user. 
		"""
		if not user:
			return self.user
		else:
			return User.objects.get(id=user.id)

	def get_nobody_user(self):
		"""Return the nobody user. 
		"""
		nobody_user = User.objects.get(username='NOBODY')
		return nobody_user

	def get_everybody_user(self):
		"""Return the nobody user. 
		"""
		everybody_user = User.objects.get(username='EVERYBODY')
		return everybody_user

	def get_system_user(self):
		"""Return the system user. 
		"""
		system_user = User.objects.get(username='SYSTEM')
		return system_user

	def get_admin_user(self):
		"""Return the admin user. 
		"""
		admin_user = User.objects.get(username='admin')
		return admin_user

	def create(self, username, password, email, **kwargs):
		"""Create the user and associated profile and set those to be default for this UserService class. 
		"""
		user = User()
		user.username = username
		user.email = email
		user.set_password(password)

		for k, v in kwargs.items():
			setattr(user, k, v)

		user.save()
		self.user = user
		self.set_profile(user)

		for k, v in kwargs.items():
			setattr(self.profile, k, v)
		self.profile.save()

		if create_user_home_dir:
			folder_service = FolderService()
			folder_service.create_user_folders(self.user)

		return self.get_user()

	def update(self, **kwargs):
		"""Update the user and profile meta data. 
		"""
		if self.user and self.profile:
			for k, v in kwargs.items():
				if k not in user_update_excluded_fields:
					setattr(self.user, k, v)
					setattr(self.profile, k, v)
			self.user.save()
			self.profile.save()
			return True
		else:
			return False

	def delete(self):
		"""Delete user and reassign ownership of user owned objects to NOBODY user.

		System users (admin, nobody, everybody and system) cannot be deleted without direct calls to the database. This should never be done as it will disable core functionality of Unitas.
		"""
		if self.user:
			admin_user = self.get_admin_user()
			nobody_user = self.get_nobody_user()
			everybody_user = self.get_everybody_user()
			system_user = self.get_system_user()

			excluded_users = (
				admin_user,
				nobody_user,
				everybody_user,
				system_user,
			)

			if self.user not in excluded_users:
				userDocuments = Document.objects.filter(owner=self.user)
				user_folders = Folder.objects.filter(owner=self.user)

				for doc in userDocuments:
					doc.owner = nobody_user
					doc.save()
				
				for folder in user_folders:
					folder.owner = nobody_user
					folder.save()

				self.user.delete()
			else:
				logger.warn(
					"Unable to delete '{}' user in excluded_users list.".format(
						self.user
					)
				)
			return True

		else:
			return False

	def set_password(self, password):
		"""Set password for user.
		"""
		self.user.set_password(password)
		self.user.save()
		self.set_user(self.user)
