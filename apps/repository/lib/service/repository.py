""" Abstract classes for repository service modules.
"""

import logging

logger = logging.getLogger(__name__)

class RepoService(object):
	"""Abstract repository service class.
	"""
	def set_user(self, user):
		"""Set the default user for the service class.
		"""
		self.user = user
		try:
			userId = user.id
		except AttributeError:
			userId = None

	def set_object(self, obj):
		"""Set the default object when using this base class.
		"""
		self.obj = obj

	def is_in_path_of_self(self, obj):
		"""Checks to see if either document or folder is in the same path of itself.
		"""
		
		if obj == self.obj:
			return True

		if hasattr(obj, 'parent_folder'):
			return self.is_in_path_of_self(obj.parent_folder)

		return False
			

class FDService(RepoService):
	"""Abstract repository service class for Folder and Document Services.
	"""
	def set_owner(self, user):
		"""Set the owner for the default object in the service class.
		"""
		self.object.owner = user
		self.object.save()