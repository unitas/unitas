"""Unit tests for Auditor class
"""

from apps.services.tests import Auditor, Audit, DocumentService, Folder, UnitasTestCase, UnsupportedAuditEntryOperation, User, UserService

sample_file = 'scripts/data/samples/Test Document.txt'

class AuditorTest(UnitasTestCase):
	"""Tests for Auditor class.
	"""

	def setUp(self):
		"""Create root folder and admin user for use in tests.
		"""
		self.root_folder = Folder.objects.get(name='Repository', parent_folder=None)
		self.admin_user = User.objects.get(username='admin', is_superuser=True)

	def create_test_user(self):
		"""Create a non priv user.
		"""
		user_service = UserService()
		self.test_user = user_service.create(
			'testuser',
			'testuser',
			'testuser@localhost',
		)

	def create_test_document(self):
		"""Create a test document.
		"""
		document_service = DocumentService()
		self.test_document = document_service.create(
			'TestDocument.txt',
			self.root_folder,
			sample_file,
			self.test_user,
		)

	def testInit(self):
		"""Test init() for Auditor().
		"""
		self.create_test_user()
		self.create_test_document()

		with self.assertRaises(TypeError):
			auditor = Auditor()

		auditor = Auditor(
			self.test_user,
			self.test_document,
		)

		self.assertTrue(isinstance(auditor, Auditor))
		self.assertTrue(auditor.user, self.test_user)
		self.assertTrue(auditor.object, self.test_document)

	def testCreateEntry(self):
		"""Test create_entry().
		"""
		self.create_test_user()
		self.create_test_document()

		auditor = Auditor(
			self.test_user,
			self.test_document,
		)

		auditor.create_entry('read')
		audit = Audit.objects.get(operation='read')
		self.assertEqual(audit.object_name, '/Repository/TestDocument.txt')

	def testCreateEntryWithInvalidOp(self):
		"""Test create_entry() with unauthorized user.
		"""
		self.create_test_user()
		self.create_test_document()

		auditor = Auditor(
			self.test_user,
			self.test_document,
		)
		
		with self.assertRaises(UnsupportedAuditEntryOperation):
			auditor.create_entry('get')
