"""Unit test for FolderAuthorization class.
"""

from apps.services.tests import UnitasTestCaseNoMimeTypes, FolderAuthorization, User, Folder, FolderService, UserService

class FolderAuthorizationTest(UnitasTestCaseNoMimeTypes):
	"""Test folder authorization rules.
	"""

	def setUp(self):
		"""Create root_folder, admin user, test folder and a couple of non priv test users.
		"""
		self.root_folder = Folder.objects.get(name='Repository', parent_folder=None)

		folder_list = Folder.objects.all()

		self.admin_user = User.objects.get(username='admin', is_superuser=True)
		self.testFolder = FolderService().create(
			'testfolder1',
			self.root_folder,
			self.admin_user,
		)
		self.test_user1 = UserService().create(
			'testuser1',
			'testuser1',
			'testuser1@localhost',
		)
		self.test_user2 = UserService().create(
			'testuser2',
			'testuser2',
			'testuser2@localhost',
		)

	def testInit(self):
		"""Test init() for DocumentAuthorization class.
		"""
		folder_service = FolderService()
		folder1 = folder_service.create(
			'Folder1',
			self.root_folder,
			self.admin_user,
		)

		folder_authorization = FolderAuthorization(
			self.admin_user, folder1)
		self.assertTrue(folder_authorization.user)
		self.assertTrue(folder_authorization.folder)
		self.assertEqual(folder_authorization.user, self.admin_user)
		self.assertEqual(folder_authorization.folder, folder1)

	def testIsAuthorizedCreationByAdminOnFolderOwnedByAdmin(self):
		"""Test authorization to create folder by admin.
		"""
		folder_service = FolderService()
		folder1 = folder_service.create(
			'Folder1',
			self.root_folder,
			self.admin_user,
		)

		folder_authorization = FolderAuthorization(
			self.admin_user, folder1,
		)
		self.assertTrue(folder_authorization.is_authorized_creation())

	def testIsAuthorizedCreationByUserOnFolderOwnedByAdmin(self):
		"""Test authorization to create folder in space not owned by admin.
		"""
		folder_service = FolderService()
		folder1 = folder_service.create(
			'Folder1',
			self.root_folder,
			self.admin_user,
		)

		folder_authorization = FolderAuthorization(
			self.test_user1, folder1,
		)
		self.assertFalse(folder_authorization.is_authorized_creation())

	def testIsAuthorizedCreationByUserOnFolderOwnedByOtherUser(self):
		"""Test authorization for non priv user to create folder in space not owned by admin or himself.
		"""	
		folder_service = FolderService()
		folder_service.set_user(self.test_user1)

		folder1 = folder_service.create(
			'Folder1',
			folder_service.get_user_home_folder(),
			self.test_user1,
		)

		folder_authorization = FolderAuthorization(
			self.test_user2, folder1,
		)
		self.assertFalse(folder_authorization.is_authorized_creation())

	def testIsAuthorizedCreationByAdminOnFolderOwnedBySystem(self):
		"""Test authorization by admin to create folder in system folder.
		"""
		admin_user = User.objects.get(username='admin', is_superuser=True)
		folder_authorization = FolderAuthorization(
			admin_user, self.root_folder)
		self.assertTrue(folder_authorization.is_authorized_creation())

	def testIsAuthorizedCreationByAdminOnFolderOwnedByNobody(self):
		"""Test authorization for admin to create folder in folder owned by NOBODY.
		"""
		admin_user = User.objects.get(username='admin', is_superuser=True)
		nobody_user = User.objects.get(username='NOBODY')
		folder_service = FolderService()
		folder_owned_by_nobody = folder_service.create(
			'Folder',
			self.root_folder,
			nobody_user,
		)
		folder_authorization = FolderAuthorization(
			admin_user, folder_owned_by_nobody,
		)
		self.assertTrue(folder_authorization.is_authorized_creation())

	def testIsAuthorizedCreationByUserOnFolderOwnedByNobody(self):
		"""Test authorization for non priv user to create folder in folder owned by NOBODY.
		"""
		admin_user = User.objects.get(username='admin', is_superuser=True)
		nobody_user = User.objects.get(username='NOBODY')
		folder_service = FolderService()
		folder_owned_by_nobody = folder_service.create(
			'Folder',
			self.root_folder,
			nobody_user,
		)
		folder_authorization = FolderAuthorization(
			self.test_user1, folder_owned_by_nobody,
		)
		self.assertFalse(folder_authorization.is_authorized_creation())

	def testIsAuthorizedReadByOwner(self):
		"""Test folder read of folder by folder's owner.
		"""
		folder_service = FolderService()
		folder_service.set_user(self.test_user1)


		folder1 = folder_service.create(
			'Folder1',
			folder_service.get_user_home_folder(),
			self.test_user1,	
		)

		folder_authorization = FolderAuthorization(
			self.test_user1, folder1, 
		)
		self.assertTrue(folder_authorization.is_authorized_read())

	def testIsAuthorizedReadByNonOwner(self):
		"""Test folder read of folder by folder's non-owner.
		"""
		folder_service = FolderService()
		folder_service.set_user(self.test_user1)


		folder1 = folder_service.create(
			'Folder1',
			folder_service.get_user_home_folder(),
			self.test_user1,	
		)

		folder_authorization = FolderAuthorization(
			self.test_user2, folder1, 
		)
		self.assertFalse(folder_authorization.is_authorized_read())

		admin_user = User.objects.get(username='admin', is_superuser=True)
		folder_authorization = FolderAuthorization(
			admin_user, folder1
		)		
		self.assertTrue(folder_authorization.is_authorized_read())

		shared_docs_folder = Folder.objects.get(
			name='Shared Documents', parent_folder=self.root_folder)
		folder_authorization = FolderAuthorization(
			self.test_user1, shared_docs_folder
		)
		self.assertTrue(folder_authorization.is_authorized_read())

	def testIsAuthorizedUpdateByOwner(self):
		"""Test folder update for owner.
		"""
		folder_service = FolderService()
		folder_service.set_user(self.test_user1)


		folder1 = folder_service.create(
			'Folder1',
			folder_service.get_user_home_folder(),
			self.test_user1,	
		)

		folder_authorization = FolderAuthorization(
			self.test_user1, folder1, 
		)
		self.assertTrue(folder_authorization.is_authorized_update())

	def testIsAuthorizedUpdateByNonOwner(self):
		"""Test folder update for non-owner.
		"""
		folder_service = FolderService()
		folder_service.set_user(self.test_user1)

		folder1 = folder_service.create(
			'Folder1',
			folder_service.get_user_home_folder(),
			self.test_user1,	
		)

		folder_authorization = FolderAuthorization(
			self.test_user2, folder1, 
		)
		self.assertFalse(folder_authorization.is_authorized_update())

		admin_user = User.objects.get(username='admin', is_superuser=True)
		folder_authorization = FolderAuthorization(
			admin_user, folder1
		)		
		self.assertTrue(folder_authorization.is_authorized_update())

		shared_docs_folder = Folder.objects.get(
			name='Shared Documents', parent_folder=self.root_folder)
		folder_authorization = FolderAuthorization(
			self.test_user1, shared_docs_folder
		)
		self.assertFalse(folder_authorization.is_authorized_update())

	def testIsAuthorizedDeleteSystemByAdmin(self):
		"""Test folder deletion for admin."""
		admin_user = User.objects.get(username='admin', is_superuser=True)
		system_user = User.objects.get(username='SYSTEM')
		system_folders = Folder.objects.filter(owner=system_user)
		for folder in system_folders:
			folder_authorization = FolderAuthorization(
				admin_user,
				folder
			)
			self.assertFalse(folder_authorization.is_authorized_delete())

	def testIsAuthorizedDeleteSystemByTestUser(self):
		"""Test folder deletion for non priv user.
		"""
		testuser1 = User.objects.get(username='testuser1')
		system_user = User.objects.get(username='SYSTEM')
		system_folders = Folder.objects.filter(owner=system_user)
		for folder in system_folders:
			folder_authorization = FolderAuthorization(
				testuser1,
				folder
			)
			self.assertFalse(folder_authorization.is_authorized_delete())

	def testDeleteByFolderOwner(self):
		"""Test delete by folder owner.
		"""
		user_service = UserService()
		user_service.create(
			'testuser',
			'secret',
			'testuser@localhost',
		)

		folder_service = FolderService()
		folder_service.create(
			'Folder1',
			self.root_folder,
			User.objects.get(username='testuser'),
		)

		folder_authorization = FolderAuthorization(
			User.objects.get(username='testuser'),
			Folder.objects.get(name='Folder1'),
		)

		self.assertTrue(folder_authorization.is_authorized_delete())

