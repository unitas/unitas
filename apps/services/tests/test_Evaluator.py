"""Unit tests for Authorization Evaluator class
"""

from apps.services.tests import UnitasTestCase, FolderAuthorization, FolderService, UserService, User, Folder

class EvaluatorTest(UnitasTestCase):
	"""Test Evaluator.
	"""

	def setUp(self):
		pass

	def testAuthorizedCreationFolder(self):
		"""Test singular rules.
		"""
		tests = ( 
			(
				(
					('AUTH_USER', 'EQUALS', 'ADMIN_USER'),
				),
				True,
			),
			(
				(
					('AUTH_USER_IS_SUPERUSER',),
				),
				True,
			)
		)

		root_folder = Folder.objects.get(name='Repository', parent_folder=None)
		admin_user = User.objects.get(username='admin')
		
		folder_authorization = FolderAuthorization(admin_user, root_folder)
		self.assertTrue(folder_authorization.evaluate(tests))

		test_user = UserService().create(
			'testuser',
			'testuser',
			'testuser@localhost',
		)

		folder_authorization = FolderAuthorization(test_user, root_folder)
		self.assertFalse(folder_authorization.evaluate(tests))

	def testMultipleExpressionsInTest(self):
		"""Test rules with multiple boolean expressions.
		"""
		tests = ( 
			(
				(
					('AUTH_USER', 'EQUALS', 'ADMIN_USER'),
					('OBJECT_OWNER', 'EQUALS', 'SYSTEM_USER'),
				),
				True,
			),
		)

		root_folder = Folder.objects.get(name='Repository', parent_folder=None)
		admin_user = User.objects.get(username='admin')
		
		folder_authorization = FolderAuthorization(admin_user, root_folder)
		self.assertTrue(folder_authorization.evaluate(tests))

	def testAuthorizedCreationFolderForSuperUser(self):
		"""Test AUTH_USER_IS_SUPERUSER.
		"""
		tests = ( 
			(
				(
					('AUTH_USER', 'EQUALS', 'ADMIN_USER'),
				),
				True,
			),
			(
				(
					('AUTH_USER_IS_SUPERUSER',),
				),
				True,
			)
		)

		root_folder = Folder.objects.get(name='Repository', parent_folder=None)
		admin_user = User.objects.get(username='admin')
		test_user = UserService().create(
			'testuser',
			'testuser',
			'testuser@localhost',
			is_superuser=True,
		)

		test_user2 = UserService().create(
			'testuser2',
			'testuser',
			'testuser2@localhost',
		)
		
		folder_authorization = FolderAuthorization(test_user, root_folder)
		self.assertTrue(folder_authorization.evaluate(tests))

		folder_authorization = FolderAuthorization(test_user2, root_folder)
		self.assertFalse(folder_authorization.evaluate(tests))



