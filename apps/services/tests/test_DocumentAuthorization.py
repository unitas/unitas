"""Unit tests for DocumentAuthorization class
"""

from apps.services.tests import DocumentAuthorization, DocumentService, Folder, FolderService, UnitasTestCase, User, UserService

sample_file = 'scripts/data/samples/Test Document.txt'

class DocumentAuthorizationTest(UnitasTestCase):
	""""Test DocumentAuthorization class.
	"""

	def setUp(self):
		"""Create root_folder, admin user, test folder and a couple of non priv test users.
		"""
		self.root_folder = Folder.objects.get(name='Repository', parent_folder=None)

		folder_list = Folder.objects.all()

		self.admin_user = User.objects.get(username='admin', is_superuser=True)
		self.testFolder = FolderService().create(
			'testfolder1',
			self.root_folder,
			self.admin_user,
		)
		self.test_user1 = UserService().create(
			'testuser1',
			'testuser1',
			'testuser1@localhost',
		)
		self.test_user2 = UserService().create(
			'testuser2',
			'testuser2',
			'testuser2@localhost',
		)

	def testInit(self):
		"""Test init() for DocumentAuthorization class.
		"""
		folder_service = FolderService()
		folder1 = folder_service.create(
			'Folder1',
			self.root_folder,
			self.admin_user,
		)

		document_service = DocumentService()
		document1 = document_service.create(
			'Document.txt',
			folder1,
			sample_file,
			self.admin_user,
		)

		document_authorization = DocumentAuthorization(
			self.admin_user, document1.parent_folder, document1)
		self.assertTrue(document_authorization.user)
		self.assertTrue(document_authorization.document)
		self.assertEqual(document_authorization.user, self.admin_user)
		self.assertEqual(document_authorization.document, document1)

	def testSystemAdminUser(self):
		"""Test create document for admin user.
		"""
		user_service = UserService()
		folder_service = FolderService()
		folder1 = folder_service.create(
			'Folder1',
			self.root_folder,
			self.admin_user,
		)

		document_service = DocumentService()
		document1 = document_service.create(
			'Document.txt',
			folder1,
			sample_file,
			self.admin_user,
		)

		document_authorization = DocumentAuthorization(
			self.admin_user,
			document1.parent_folder,
			document1
		)
		self.assertEqual(
			user_service.get_admin_user().username, 
			'admin',
		)

	def testIsAuthorizedReadByOwner(self):
		"""Test if document can be read by owner.
		"""
		folder_service = FolderService()
		folder_service.set_user(self.test_user1)

		folder1 = folder_service.create(
			'Folder1',
			folder_service.get_user_home_folder(),
			self.test_user1,	
		)

		document_service = DocumentService()
		document1 = document_service.create(
			'Document1.txt',
			folder1,
			sample_file,
			self.test_user1,
		)

		document_authorization = DocumentAuthorization(
			self.test_user1, document1.parent_folder, document1 
		)
		self.assertTrue(document_authorization.is_authorized_read())

	def testIsAuthorizedReadByNonOwner(self):
		"""Test if document can be read by non-owner.
		"""
		folder_service = FolderService()
		folder_service.set_user(self.test_user1)

		folder1 = folder_service.create(
			'Folder1',
			folder_service.get_user_home_folder(),
			self.test_user1,	
		)

		document_service = DocumentService()
		document1 = document_service.create(
			'Document1.txt',
			folder1,
			sample_file,
			self.test_user1,
		)

		document_authorization = DocumentAuthorization(
			self.test_user2, document1.parent_folder, document1 
		)
		self.assertFalse(document_authorization.is_authorized_read())

		admin_user = User.objects.get(username='admin', is_superuser=True)
		document_authorization = DocumentAuthorization(
			admin_user, document1.parent_folder, document1
		)		
		self.assertTrue(document_authorization.is_authorized_read())

		shared_docs_folder = Folder.objects.get(
			name='Shared Documents', parent_folder=self.root_folder)

		document2 = document_service.create(
			'SharedDocument1.txt',
			shared_docs_folder,
			sample_file,
			self.test_user1,
		)

		document_authorization = DocumentAuthorization(
			self.test_user2, document2.parent_folder, document2
		)
		self.assertTrue(document_authorization.is_authorized_read())

	def testIsAuthorizedUpdateByOwner(self):
		"""Test if owner can update document.
		"""
		folder_service = FolderService()
		folder_service.set_user(self.test_user1)


		folder1 = folder_service.create(
			'Folder1',
			folder_service.get_user_home_folder(),
			self.test_user1,	
		)

		document_service = DocumentService()
		document1 = document_service.create(
			'Document1.txt',
			folder1,
			sample_file,
			self.test_user1,
		)

		document_authorization = DocumentAuthorization(
			self.test_user1, document1.parent_folder, document1 
		)
		self.assertTrue(document_authorization.is_authorized_update())

	def testIsAuthorizedUpdateByNonOwner(self):
		"""Test if non owner can update document.
		"""
		folder_service = FolderService()
		folder_service.set_user(self.test_user1)

		folder1 = folder_service.create(
			'Folder1',
			folder_service.get_user_home_folder(),
			self.test_user1,	
		)

		document_service = DocumentService()
		document1 = document_service.create(
			'Document1.txt',
			folder1,
			sample_file,
			self.test_user1,
		)

		document_authorization = DocumentAuthorization(
			self.test_user2, document1.parent_folder, document1 
		)
		self.assertFalse(document_authorization.is_authorized_update())

		admin_user = User.objects.get(username='admin', is_superuser=True)
		document_authorization = DocumentAuthorization(
			admin_user, document1.parent_folder, document1
		)		
		self.assertTrue(document_authorization.is_authorized_update())

		shared_docs_folder = Folder.objects.get(
			name='Shared Documents', parent_folder=self.root_folder)

		document2 = document_service.create(
			'Document2.txt',
			shared_docs_folder,
			sample_file,
			self.test_user1,
		)

		document_authorization = DocumentAuthorization(
			self.test_user2, document2.parent_folder, document2
		)
		self.assertFalse(document_authorization.is_authorized_update())

		document3 = document_service.create(
			'Document3.txt',
			shared_docs_folder,
			sample_file,
			self.test_user1,
		)

		document_authorization = DocumentAuthorization(
			self.test_user1, document3.parent_folder, document3
		)
		self.assertTrue(document_authorization.is_authorized_update())

	def testDeleteByDocumentOwner(self):
		"""Test if document can be deleted by owner.
		"""
		folder_service = FolderService()
		folder_service.set_user(self.test_user1)

		folder1 = folder_service.create(
			'Folder1',
			folder_service.get_user_home_folder(),
			self.test_user1,	
		)

		document_service = DocumentService()
		document1 = document_service.create(
			'Document1.txt',
			folder1,
			sample_file,
			self.test_user1,
		)

		document_authorization = DocumentAuthorization(
			self.test_user1, document1.parent_folder, document1, 
		)
		self.assertTrue(document_authorization.is_authorized_delete())

	def testDeleteByNonDocumentOwner(self):
		"""Test if document can be deleted by non-owner.
		"""
		folder_service = FolderService()
		folder_service.set_user(self.test_user1)

		folder1 = folder_service.create(
			'Folder1',
			folder_service.get_user_home_folder(),
			self.test_user1,	
		)

		document_service = DocumentService()
		document1 = document_service.create(
			'Document1.txt',
			folder1,
			sample_file,
			self.test_user1,
		)

		document_authorization = DocumentAuthorization(
			self.test_user2, document1.parent_folder, document1 
		)
		self.assertFalse(document_authorization.is_authorized_delete())

	def testDeleteByAdmin(self):
		"""Test if any document can be deleted by admin user.
		"""
		folder_service = FolderService()
		folder_service.set_user(self.test_user1)

		folder1 = folder_service.create(
			'Folder1',
			folder_service.get_user_home_folder(),
			self.test_user1,	
		)

		document_service = DocumentService()
		document1 = document_service.create(
			'Document1.txt',
			folder1,
			sample_file,
			self.test_user1,
		)

		document_authorization = DocumentAuthorization(
			self.admin_user, document1.parent_folder, document1, 
		)
		self.assertTrue(document_authorization.is_authorized_delete())
