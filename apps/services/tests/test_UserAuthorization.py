"""Unit test for UserAuthorization class.
"""

from apps.services.tests import User, UnitasTestCaseNoMimeTypes, UserAuthorization, UserService

class UserAuthorizationTest(UnitasTestCaseNoMimeTypes):
	"""Test UserAuthorization class.
	"""

	def setUp(self):
		"""Create root_folder, admin user, test folder and a couple of non priv test users.
		"""

		self.admin_user = User.objects.get(username='admin')

		user_service = UserService()
		self.test_np_user = user_service.create(
			'testuser',
			'testuser',
			'testuser@localhost',
		)

		self.test_np_user2 = user_service.create(
			'testuser2',
			'testuser2',
			'testuser2@localhost',
		)

		self.asst_admin_user = user_service.create(
			'asstadmin',
			'asstadmin',
			'asstadmin@localhost',
			is_superuser=True,
		)

	def testInit(self):
		"""Test init() for UserAuthorization class.
		"""
		user_authorization = UserAuthorization(
			self.admin_user, self.test_np_user,
		)

		self.assertEqual(user_authorization.user, self.admin_user)
		self.assertEqual(user_authorization.user_object, self.test_np_user)

	def testIsAuthorizedCreationAdmin(self):
		"""Test authorization for admin user to create new user.
		"""
		user_authorization = UserAuthorization(
			self.admin_user, self.test_np_user,
		)
		self.assertTrue(user_authorization.is_authorized_creation())

	def testIsAuthorizedCreationAsstAdmin(self):
		"""Test authorization for super user to create new user.
		"""
		user_authorization = UserAuthorization(
			self.asst_admin_user, self.test_np_user,
		)
		self.assertTrue(user_authorization.is_authorized_creation())

	def testIsAuthorizedCreationNpUser(self):
		"""Test authorization for non priv user to create new user.
		"""
		user_authorization = UserAuthorization(
			self.test_np_user2, self.test_np_user,
		)
		self.assertFalse(user_authorization.is_authorized_creation())

		user_authorization = UserAuthorization(
			self.test_np_user, self.test_np_user2,
		)
		self.assertFalse(user_authorization.is_authorized_creation())

	def testIsAuthorizedReadAdmin(self):
		"""Test authorization for admin user to read user details.
		"""
		user_authorization = UserAuthorization(
			self.admin_user, self.test_np_user,
		)
		self.assertTrue(user_authorization.is_authorized_read())

	def testIsAuthorizedReadAsstAdmin(self):
		"""Test authorization for super user to read user details.
		"""
		user_authorization = UserAuthorization(
			self.asst_admin_user, self.test_np_user,
		)
		self.assertTrue(user_authorization.is_authorized_read())

	def testIsAuthorizedReadNpUser(self):
		"""Test authorization for non priv user to read user details.
		"""
		user_authorization = UserAuthorization(
			self.test_np_user2, self.test_np_user,
		)
		self.assertFalse(user_authorization.is_authorized_read())

		user_authorization = UserAuthorization(
			self.test_np_user, self.test_np_user2,
		)
		self.assertFalse(user_authorization.is_authorized_read())

		user_authorization = UserAuthorization(
			self.test_np_user, self.test_np_user,
		)
		self.assertTrue(user_authorization.is_authorized_read())

	def testIsAuthorizedUpdateAdmin(self):
		"""Test authorization for admin to update user details.
		"""
		user_authorization = UserAuthorization(
			self.admin_user, self.test_np_user,
		)
		self.assertTrue(user_authorization.is_authorized_update())

	def testIsAuthorizedUpdateAsstAdmin(self):
		"""Test authorization for super user to update user details.
		"""
		user_authorization = UserAuthorization(
			self.asst_admin_user, self.test_np_user,
		)
		self.assertTrue(user_authorization.is_authorized_update())

	def testIsAuthorizedUpdateNpUser(self):
		"""Test authorization for non priv user to udpate user details.
		"""
		user_authorization = UserAuthorization(
			self.test_np_user2, self.test_np_user,
		)
		self.assertFalse(user_authorization.is_authorized_update())

		user_authorization = UserAuthorization(
			self.test_np_user, self.test_np_user2,
		)
		self.assertFalse(user_authorization.is_authorized_update())

		user_authorization = UserAuthorization(
			self.test_np_user, self.test_np_user,
		)
		self.assertTrue(user_authorization.is_authorized_update())

	def testIsAuthorizedDeleteAdmin(self):
		"""Test authorization for admin to delete users.
		"""
		user_authorization = UserAuthorization(
			self.admin_user, self.test_np_user,
		)
		self.assertTrue(user_authorization.is_authorized_delete())

	def testIsAuthorizedDeleteAsstAdmin(self):
		"""Test authorization for super user to delete users.
		"""
		user_authorization = UserAuthorization(
			self.asst_admin_user, self.test_np_user,
		)
		self.assertTrue(user_authorization.is_authorized_delete())

	def testIsAuthorizedDeleteNpUser(self):
		"""Test authorization for non priv user to delete users.
		"""
		user_authorization = UserAuthorization(
			self.test_np_user2, self.test_np_user,
		)
		self.assertFalse(user_authorization.is_authorized_delete())

		user_authorization = UserAuthorization(
			self.test_np_user, self.test_np_user2,
		)
		self.assertFalse(user_authorization.is_authorized_delete())

		user_authorization = UserAuthorization(
			self.test_np_user, self.test_np_user,
		)
		self.assertFalse(user_authorization.is_authorized_delete())

	def testIsAuthorizedReadBySuperUser(self):
		"""Test authorization for super user to read users.
		"""
		super_user = UserService().create(
			'superuser',
			'secret',
			'superuser@localhost',
		)

		super_user.is_superuser = True
		super_user.save()

		admin_user = UserService().get_admin_user()

		user_authorization = UserAuthorization(super_user, admin_user)
		self.assertFalse(user_authorization.is_authorized_read())