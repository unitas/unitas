"""Django admin for auditing module models.
"""

from django.contrib import admin

from modules.auditing.models import Audit
from modules.search.models import Content

admin.site.register(Audit)
admin.site.register(Content)