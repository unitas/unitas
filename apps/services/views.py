""" No views for services modules.
"""

from __future__ import absolute_import

from django.shortcuts import render
import logging

logger = logging.getLogger(__name__)
