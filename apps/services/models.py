""" Service model.
"""

from __future__ import unicode_literals

from django.db import models
from modules.auditing.models import *
from modules.search.models import *
