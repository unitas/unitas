"""Module containing FolderAuthorization class.
"""

from apps.services.modules.authorization.lib import Authorization
from apps.services.modules.authorization.config.folder_authorization_filters import create_filters, delete_filters, read_filters, update_filters
import logging

logger = logging.getLogger(__name__)


class FolderAuthorization(Authorization):
	"""Class to check for authorizations for folders.
	"""

	def __init__(self, user, folder):
		"""Sets default user, folder and object (for evaluate()).
		"""
		self.user = user
		self.folder = folder
		self.object = self.folder

	def is_authorized_creation(self):
		"""Returns true/false if default user is authorized to perform create action.
		"""
		result = self.evaluate(create_filters)
		return result

	def is_authorized_read(self):
		"""Returns true/false if default user is authorized to perform read action.
		"""
		result = self.evaluate(read_filters)
		return result

	def is_authorized_update(self):
		"""Returns true/false if default user is authorized to perform update action.
		"""
		results = self.evaluate(update_filters)
		return results

	def is_authorized_delete(self):
		"""Returns true/false if default user is authorized to perform delete action.
		"""
		result = self.evaluate(delete_filters)
		return result

		
