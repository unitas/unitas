"""Module containing UserAuthorization class.
"""

from apps.services.modules.authorization.lib import Authorization
from apps.services.modules.authorization.config.user_authorization_filters import create_filters, read_filters, update_filters, delete_filters
import logging

logger = logging.getLogger(__name__)

 
class UserAuthorization(Authorization):
	"""Class to check for authorizations for user actions.
	"""

	def __init__(self, auth_user, user_object=None):
		"""Sets default user and object (for evaluate()).
		"""
		self.user = auth_user
		self.user_object = user_object
		self.object = self.user_object

	def is_authorized_creation(self):
		"""Returns true/false if default user is authorized to perform create action.
		"""
		return self.evaluate(create_filters)

	def is_authorized_read(self):
		"""Returns true/false if default user is authorized to perform read action.
		"""
		return self.evaluate(read_filters)

	def is_authorized_update(self):
		"""Returns true/false if default user is authorized to perform update action.
		"""
		return self.evaluate(update_filters)

	def is_authorized_delete(self):
		"""Returns true/false if default user is authorized to perform delete action.
		"""
		return self.evaluate(delete_filters)