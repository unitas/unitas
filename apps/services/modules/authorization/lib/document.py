"""Module containing DocumentAuthorization class.
"""

from apps.services.modules.authorization.lib import Authorization
from apps.services.modules.authorization.config.document_authorization_filters import create_filters, read_filters, update_filters, delete_filters
import logging

logger = logging.getLogger(__name__)

class DocumentAuthorization(Authorization):
	"""Class to check for authorizations for documents.
	"""

	def __init__(self, user, parent_folder, document=None):
		"""Sets default user, document and object (for evaluate()).
		"""
		self.user = user
		self.document = document
		self.object = parent_folder

	def is_authorized_creation(self):
		"""Returns true/false if default user is authorized to perform create action.
		"""
		return self.evaluate(create_filters)

	def is_authorized_read(self):
		"""Returns true/false if default user is authorized to perform read action.
		"""
		self.object = self.document
		return self.evaluate(read_filters)

	def is_authorized_update(self):
		"""Returns true/false if default user is authorized to perform update action.
		"""
		self.object = self.document
		return self.evaluate(update_filters)

	def is_authorized_delete(self):
		"""Returns true/false if default user is authorized to perform delete action.
		"""
		self.object = self.document
		return self.evaluate(delete_filters)