"""Authorization base class Authorization.
"""

import logging

logger = logging.getLogger(__name__)

from apps.services.modules.authorization.lib import FolderService, operator, User, UserService

class Authorization(object):
	"""Class for evaluating authorization tests.
	"""

	def evaluate(self, tests):
		""" Evaluates whether an authorization test passes or not. """

		logger.debug('self.object is {}'.format(self.object)) 

		user_service = UserService()
		folder_service = FolderService()
		user_map, oper_map, tgt_map = {}, {}, {}

		#Left side value
		user_map['AUTH_USER'] = self.user
		"""The authenticated user requesting to perform action.
		"""
		user_map['ADMIN_USER'] = user_service.get_admin_user()
		"""The admin user.
		"""
		user_map['AUTH_USER_IS_SUPERUSER'] = self.user.is_superuser
		"""Boolean True if authenticated user is a superuser. False if not.
		"""
		user_map['SYSTEM_USER'] = user_service.get_system_user()
		"""The SYSTEM user.
		"""
		user_map['NOBODY_USER'] = user_service.get_nobody_user()
		"""The NOBODY user.
		"""
		user_map['EVERYBODY_USER'] = user_service.get_everybody_user()
		"""The EVERYBODY user.
		"""
		user_map['OBJECT'] = self.object
		"""The object defined with authorization initialization.
		"""
		user_map['SHARED_DOCUMENTS_FOLDER'] = folder_service.get_shared_documents_folder()
		"""The system shared documents folder.
		"""

		""" Since these can apply also to user objects (which don't have a parent_folder), we have a try/except block. If no such thing, then the user_map gets set to None. """
		try:
			logger.debug('SELF OBJECT PARENT FOLDER IS {}'.format(self.object.parent_folder))
		except AttributeError:
			logger.debug('SELF OBJECT_PARENT_FOLDER_OWNER IS NONE.')

		logger.debug('SELF OBJECT IS {}'.format(self.object))
		try:
			user_map['OBJECT_PARENT_FOLDER_OWNER'] = self.object.parent_folder.owner
		except AttributeError:
			if self.object == User:
				user_map['OBJECT_PARENT_FOLDER_OWNER'] = None

		try:
			user_map['OBJECT_OWNER'] = self.object.owner
		except AttributeError:
			if self.object == User:
				user_map['OBJECT_OWNER'] = None

		""" Operator -- for now, either = or != """
		oper_map['EQUALS'] = operator.eq
		oper_map['NOT EQUALS'] = operator.ne

		""" Right side value """
		tgt_map['AUTH_USER'] = user_map['AUTH_USER']
		tgt_map['ADMIN_USER'] = user_map['ADMIN_USER']
		tgt_map['SYSTEM_USER'] = user_map['SYSTEM_USER']
		tgt_map['NOBODY_USER'] = user_map['NOBODY_USER']
		tgt_map['EVERYBODY_USER'] = user_map['EVERYBODY_USER']
		tgt_map['OBJECT'] = user_map['OBJECT']
		tgt_map['SHARED_DOCUMENTS_FOLDER'] = user_map['SHARED_DOCUMENTS_FOLDER']

		""" Since these can apply also to user objects (which don't have a parent_folder), we have a try/except block. If no such thing, then the tgt_map gets set to None. """
		try:
			tgt_map['OBJECT_OWNER'] = user_map['OBJECT_OWNER']
		except KeyError:
			if self.object == User:
				tgt_map['OBJECT_OWNER'] = None

		try:
			tgt_map['OBJECT_PARENT_FOLDER_OWNER'] = user_map[
				'OBJECT_PARENT_FOLDER_OWNER']
		except KeyError:
			if self.object == User:
				tgt_map['OBJECT_PARENT_FOLDER_OWNER'] = None
				
		for test in tests:
			expressions, result = test
			expr_builder = []
			for expression in expressions:
				try:
					user_obj, oper, target_obj = expression
					user_obj = user_map[user_obj]
					oper = oper_map[oper]
					target_obj = tgt_map[target_obj]
					
				except ValueError:
					user_obj, oper, target_obj = expression[0], None, None
					user_obj = user_map[user_obj]

				expr_builder.append([user_obj, oper, target_obj])

			""" See Issue #60 - Removed because iter() isn't really necessary. Will remove this comment once we confirm that this works for a web client.
			"""
			#expr_builder_iter = iter(expr_builder)

			i = 0
			for expr in expr_builder:
				i += 1
				logger.debug('Expression is {}'.format(expr))
				user_obj, oper, target_obj = expr
				if oper and target_obj:
					logger.debug(
						'= Evaluating {} and {} using operation: {}'.format(
							user_obj, target_obj, oper,
						)
					)
					if oper(user_obj, target_obj):
						logger.debug('== Evaluates to true.')
						if i == len(expr_builder):
							return result
					else:
						logger.debug('== Evaluates to false.')
						logger.debug('== Breaking out ...')
						break	
				else:
					logger.debug(
						'= Evaluating {} as True/False'.format(
							user_obj
						)
					)
					if user_obj:
						logger.debug('== Evaluates to true.')
						if i == len(expr_builder):
							return result
					else:
						logger.debug('== Evaluates to false.')
						logger.debug('== Breaking out ...')
						break
				logger.debug('\n') # for appearance sake
		logger.debug('Returning a false...')
		return False