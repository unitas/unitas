"""Package de-noter for apps.services.modules.authorization.lib.
"""

import operator

from django.contrib.auth.models import User
from apps.repository.lib.service.user import UserService
from apps.repository.lib.service.folder import FolderService
from apps.repository.lib.service.document import DocumentService
from apps.services.modules.authorization.lib.authorization import Authorization
