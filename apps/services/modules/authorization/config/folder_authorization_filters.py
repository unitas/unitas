"""Set of rules for testing folder authorizations.
"""

create_filters = (
	(
		(
			('AUTH_USER_IS_SUPERUSER',),
			('AUTH_USER', 'NOT EQUALS', 'ADMIN_USER',),
			('OBJECT_OWNER', 'EQUALS', 'ADMIN_USER',),
		),
		False
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'OBJECT_OWNER'),
		),
		True,
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER'),
			('OBJECT_OWNER', 'EQUALS', 'SYSTEM_USER'),
		),
		True,
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER'),
			('OBJECT_OWNER', 'EQUALS', 'NOBODY_USER'),
		),
		True,
	),
	(
		(
			('OBJECT_OWNER', 'EQUALS', 'EVERYBODY_USER'),
		),
		True,
	),
	(
		(
			('AUTH_USER_IS_SUPERUSER',),
		),
		True,
	)

)
"""For testing creation of folders.
"""

read_filters = (
	(
		(
			('AUTH_USER_IS_SUPERUSER',),
			('OBJECT_OWNER', 'EQUALS', 'ADMIN_USER',),
			('AUTH_USER', 'NOT EQUALS', 'ADMIN_USER',),
		),
		False
	),
	(
		(
			('AUTH_USER_IS_SUPERUSER',),
			('OBJECT_OWNER', 'EQUALS', 'ADMIN_USER'),
		),
		True,
	),
	(
		(
			('AUTH_USER_IS_SUPERUSER',),
		),
		True,
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'OBJECT_OWNER'),
		),
		True
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER'),
		),
		True
	),
	(
		(
			('OBJECT', 'EQUALS', 'SHARED_DOCUMENTS_FOLDER'),
		),
		True
	)
)
"""For testing read of folders.
"""

update_filters = (
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER'),
			('OBJECT_OWNER', 'EQUALS', 'SYSTEM_USER'),
		),
		False
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'OBJECT_OWNER'),
		),
		True
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER'),
		),
		True
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER'),
			('OBJECT_OWNER', 'EQUALS', 'NOBODY_USER'),
		),
		True
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER'),
			('OBJECT_OWNER', 'EQUALS', 'EVERYBODY_USER'),
		),
		True
	),
	(
		(
			('AUTH_USER_IS_SUPERUSER',),
			('AUTH_USER', 'NOT EQUALS', 'ADMIN_USER',),
			('OBJECT_OWNER', 'EQUALS', 'NOBODY_USER',),
		),
		True
	),
)
"""For testing update of folders.
"""

delete_filters = (
	(
		(
			('AUTH_USER', 'EQUALS', 'OBJECT_OWNER',),
		),
		True,
	),
	(
		(
			('OBJECT_OWNER', 'EQUALS', 'SYSTEM_USER',),
		),
		False,
	),
	(
		(
			('AUTH_USER_IS_SUPERUSER',),
			('AUTH_USER', 'NOT EQUALS', 'ADMIN_USER',),
			('OBJECT_OWNER', 'EQUALS', 'EVERYBODY_USER',),
		),
		False,
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER',),
		),
		True,
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER',),
			('OBJECT_OWNER', 'EQUALS', 'NOBODY_USER',),
		),
		True,
	),
)
"""For testing deletion of folders.
"""