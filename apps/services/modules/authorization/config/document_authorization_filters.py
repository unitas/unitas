"""Set of rules for testing document authorizations.
"""

create_filters = (
	(
		(
			('OBJECT_OWNER', 'EQUALS', 'EVERYBODY_USER',),
		),
		True,
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'OBJECT_OWNER',),
		),
		True,
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER',),
			('OBJECT_OWNER', 'EQUALS', 'SYSTEM_USER',),
		),
		True,
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER',),
			('OBJECT_OWNER', 'EQUALS', 'EVERYBODY_USER',),
		),
		True,
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER'),
			('OBJECT_OWNER', 'EQUALS', 'NOBODY_USER',),
		),
		True,
	),
	(
		(
			('AUTH_USER_IS_SUPERUSER',),
			('OBJECT_OWNER', 'EQUALS', 'EVERYBODY_USER',),
		),
		True,
	),
)
"""For testing creation of documents.
"""

read_filters = (
	(
		(
			('AUTH_USER', 'EQUALS', 'OBJECT_OWNER',),
			('AUTH_USER', 'EQUALS', 'OBJECT_PARENT_FOLDER_OWNER',),
		),
		True,
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER'),
		),
		True,
	),
	(
		(
			('AUTH_USER_IS_SUPERUSER',),
			('OBJECT_OWNER', 'EQUALS', 'NOBODY_USER',),
		),
		True,
	),
	(
		(
			('OBJECT_PARENT_FOLDER_OWNER', 'EQUALS', 'EVERYBODY_USER'),
		),
		True,
	)
)
"""For testing read of documents.
"""

update_filters = (
	(
		(
			('AUTH_USER', 'EQUALS', 'OBJECT_OWNER'),
		),
		True
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER'),
		),
		True
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER'),
			('OBJECT_OWNER', 'EQUALS', 'SYSTEM_USER'),
		),
		True
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER'),
			('OBJECT_OWNER', 'EQUALS', 'NOBODY_USER'),
		),
		True
	),
	(
		(
			('AUTH_USER_IS_SUPERUSER',),
			('OBJECT_OWNER', 'EQUALS', 'NOBODY_USER',),
		),
		True,
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER'),
			('OBJECT_OWNER', 'EQUALS', 'EVERYBODY_USER'),
		),
		True
	),
)
"""For testing update of documents.
"""

delete_filters = (
	(
		(
			('OBJECT_OWNER', 'EQUALS', 'SYSTEM_USER'),
		),
		False
	),
	(
		(
			('OBJECT_OWNER', 'EQUALS', 'EVERYBODY_USER'),
		),
		False
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'OBJECT_OWNER'),
		),
		True
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER'),
		),
		True
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER'),
			('OBJECT_OWNER', 'EQUALS', 'NOBODY_USER'),
		),
		True
	),
)
"""For testing deletion of documents.
"""