"""Set of rules for testing user authorizations.
"""

create_filters = (
	(
		(
			('AUTH_USER_IS_SUPERUSER',),
		),
		True
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER'),
		),
		True
	),
)
"""For testing creation of users.
"""

read_filters = (
	(
		(
			('AUTH_USER', 'NOT EQUALS', 'ADMIN_USER'),
			('AUTH_USER_IS_SUPERUSER',),
			('OBJECT', 'NOT EQUALS', 'ADMIN_USER'),
		),
		True
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER'),
		),
		True
	),	
	(
		(
			('AUTH_USER', 'EQUALS', 'OBJECT'),
		),
		True
	),	
)
"""For testing reading of users' details.
"""

update_filters = (
	(
		(
			('AUTH_USER', 'NOT EQUALS', 'ADMIN_USER'),
			('AUTH_USER_IS_SUPERUSER',),
			('ADMIN_USER', 'EQUALS', 'OBJECT'),
		),
		False
	),	
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER'),
		),
		True,
	),
	(
		(
			('AUTH_USER_IS_SUPERUSER',),
		),
		True,
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'OBJECT'),
		),
		True,
	),	
)
"""For testing update of users.
"""

delete_filters = (
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER'),
			('OBJECT', 'EQUALS', 'ADMIN_USER'),
		),
		False
	),
	(
		(
			('AUTH_USER_IS_SUPERUSER',),
			('OBJECT', 'EQUALS', 'ADMIN_USER',),
		),
		False
	),
	(
		(
			('AUTH_USER_IS_SUPERUSER',),
			('OBJECT', 'EQUALS', 'AUTH_USER',),
		),
		False
	),
	(
		(
			('AUTH_USER_IS_SUPERUSER',),
			('OBJECT', 'NOT EQUALS', 'ADMIN_USER',),
		),
		True
	),
	(
		(
			('AUTH_USER', 'EQUALS', 'ADMIN_USER'),
		),
		True
	),
)
"""For testing deletion of users.
"""