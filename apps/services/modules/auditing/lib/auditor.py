from apps.services.modules.auditing.models import Audit
from apps.services.modules.auditing.config.auditing import audit_operations
import logging

logger = logging.getLogger(__name__)

class UnsupportedAuditEntryOperation(Exception):
	
	def __init__(self, message):
		self.message = message

	def __str__(self):
		return repr(self.message)
	
class Auditor(object):

	def __init__(self, user, obj):
		self.user = user
		self.object = obj
		self.object_type = type(obj)
		try:
			self.object_id = obj.uuid
		except AttributeError:
			try:
				self.object_id = obj.id
			except AttributeError:
				self.object_id = None

	def create_entry(self, operation, message=None):
		if operation not in audit_operations:
			raise UnsupportedAuditEntryOperation(
				'"{}" operation not supported for audit entry.'.format(
					operation,
				)
			)

		else:
			audit = Audit()
			audit.object_name = self.object
			audit.object_type = self.object_type
			audit.object_id = self.object_id
			audit.user = self.user
			audit.operation = operation
			audit.message = message
			audit.save()
