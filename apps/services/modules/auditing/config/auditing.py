"""Auditing configurations.
"""

audit_operations = (
	'create',
	'read',
	'update',
	'delete',
	'security',
)
"""Registered operations that the Audit class can use.
"""