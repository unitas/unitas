"""Models file for the Auditing module.
"""

from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from apps.services.modules.auditing.config.auditing import audit_operations

class Audit(models.Model):
	"""Class to represent an audit entry for a particular action.
	"""

	object_name = models.TextField(
		'Object Name/Path',
	)
	"""Name or path of the object.
	"""

	object_type = models.CharField(
		'Object',
		max_length=100,
		null=True,
		blank=True,
	)
	"""Type of the object (i.e. Folder, Document, User, etc.)
	"""

	object_id = models.CharField(
		'Object ID',
		max_length=200,
		null=True,
		blank=True,
	)
	"""Object's id.
	"""

	created = models.DateTimeField(
		'Created',
		auto_now_add=True,
	)
	"""When entry was created.
	"""

	user = models.ForeignKey(
		User,
	)
	"""User whose authority was used for the action.
	"""

	operation = models.CharField(
		'Operation',
		max_length=10,
		choices=[(op, op) for op in audit_operations],
	)
	"""Audit operation. These are confirgured in config.auditing.
	"""

	message = models.TextField(
		'Message',
		null=True,
		blank=True,
	)
	"""Optional message to add for the audit entry.
	"""

	def __str__(self):
		return '{} id#{} ; {}; {}; {} id#{}; {}'.format(
			self.object_name,
			self.object_id,
			self.object_type,
			self.created,
			self.user.username,
			self.user.id,
			self.operation,
		)