"""Models file for the Search module.
"""

from __future__ import unicode_literals

from django.db import models

class Content(models.Model):

	document_version = models.ForeignKey(
		'repository.DocumentVersion'
	)

	created = models.DateTimeField(
		'Created',
		auto_now_add=True,
	)

	text = models.TextField(
		'Text',
	)

	def __str__(self):
		return '{}'.format(self.document_version)