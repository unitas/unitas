
from __future__ import absolute_import

import os, subprocess
from apps.services.modules.search.models import Content
from apps.repository.lib.service.document import DocumentService

class ContentIndexer(object):

	def __init__(self, document, preview_file):

		self.document = document
		self.content_file = preview_file

	def index(self):

		output_filename = 'tmp' + os.path.sep + self.document.name + '.txt'

		document_service = DocumentService()
		document_service.set_document(self.document)
		document_version = document_service.get_latest_version()

		subprocess.call(
			[
				'scripts/pdf2txt.py',
				'-o',
				output_filename,
				self.content_file,
			]
		)

		fh = open(output_filename, 'r')
		text = fh.read()
		fh.close()

		content = Content()
		content.document_version = document_version
		content.text = text
		content.save()

		os.remove(output_filename)
		
