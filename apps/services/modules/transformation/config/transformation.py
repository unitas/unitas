""" General Transformation configs
"""

xforms_enabled = True

allowable_xform_2pdf = (
	'.doc', '.docx', '.dot', '.fodt', '.html', '.odt', '.docx', '.ott', 
	'.rtf', '.txt', '.xml', '.csv', '.dif', '.fods', '.ods', '.xlsx',
	'.ots', '.slk', '.uos', '.xls', '.xlsx', '.xlt', '.fodp', '.odg',
	'.odp', '.otp', '.pot', '.potm', '.pps', '.ppt',
)

xform_2pdf_soffice = (
	'.doc', '.docx', '.dot', '.fodt', '.html', '.odt', '.docx', '.ott', 
	'.rtf', '.txt', '.xml', '.csv', '.dif', '.fods', '.ods', '.xlsx',
	'.ots', '.slk', '.uos', '.xls', '.xlsx', '.xlt', '.fodp', '.odg',
	'.odp', '.otp', '.pot', '.potm', '.pps', '.ppt',
)