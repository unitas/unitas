
from __future__ import absolute_import
import os, subprocess, shutil, threading
from django.core.files import File
from apps.services.modules.transformation.config.transformation import allowable_xform_2pdf, xform_2pdf_soffice
from apps.repository.lib.service.document import DocumentService
from apps.web.models import PreviewDocument

class SofficeTransformer(object):

	def __init__(self, document):

		self.document = document

	def xform_2pdf(self):

		document_service = DocumentService()
		document_service.set_document(self.document)
		document_file = document_service.get_latest_version().content_file

		subprocess.call(
			[
				'soffice', 
				'--headless', 
				'--convert-to',
				'pdf',
				'--outdir',
				'tmp',
				document_file.name
			]
		)

		return os.path.join('tmp', document_file.name.split('/')[-1].replace('.bin', '.pdf'))

class Transformer(object):

	def __init__(self, document=None):
		self.document = document

	def get_extension(self):
		return '.' + self.document.name.split('.')[-1]

	def create_preview(self):

		doc_extension = self.get_extension()

		if doc_extension in allowable_xform_2pdf:

			if doc_extension in xform_2pdf_soffice:

				soffice_xform = SofficeTransformer(self.document)
				preview_file = soffice_xform.xform_2pdf()

				document_service = DocumentService()
				document_service.set_document(self.document)
				document_version = document_service.get_latest_version()

				preview_document = PreviewDocument()
				preview_document.content_file = File(open(preview_file))
				preview_document.document_version = document_version
				preview_document.save()

				return preview_file



