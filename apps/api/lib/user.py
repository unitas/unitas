"""UserAPI Class
"""

from apps.api.lib import logging, auditing_enabled, Api, Auditor, User, UserAuthorization, UserService

logger = logging.getLogger(__name__)

class UserApi(Api):
	"""The API used by clients for interacting with users and user profiles by going through 
	authorization and auditing modules (service modules).
	"""

	def __init__(self, auth_user):
		"""Sets the class auth_user. The auth_user is the one who is making the 
		service request.
		"""
		self.auth_user = auth_user

	def create_user(self, username, password, email):
		"""Create a user and associated profile. 
		Args: 
			username (string): User username
			password (string): User password
			email (string): User email address

		Returns:
			bool: True for successful creation of a user and False for unsuccessful attempt.

		"""
		user_authorization = UserAuthorization(self.auth_user)
		if user_authorization.is_authorized_creation():
			new_user = UserService().create(
				username, password, email
			)
			if new_user:
				if auditing_enabled:
					auditor = Auditor(self.auth_user, new_user)
					auditor.create_entry('create')
				return True
			else:
				logger.error('Authorized to create user but unable to do so.')
				return False

		else:
			if auditing_enabled:
				auditor = Auditor(self.auth_user, User)
				message = "Unauthorized user creation attempt by '{}' to create user '{}'".format(
					self.auth_user,
					username,
				)
				auditor.create_entry('security', message)
			raise self.UnauthorizedActionException(
				"User '{}' is not authorized to create user '{}'".format(
					self.auth_user, username
				)
			)

	def get_user(self, user_obj):
		"""Returns a user object.

		Args: 
			user_obj (User): User object

		Returns:
			User: User object or UnauthorizedActionException

		"""
		user_authorization = UserAuthorization(self.auth_user, user_obj)
		if user_authorization.is_authorized_read():
			if auditing_enabled:
				auditor = Auditor(self.auth_user, user_obj)
				auditor.create_entry('read')
			return UserService().get_user(user_obj)
		else:
			if auditing_enabled:
				auditor = Auditor(self.auth_user, user_obj)
				message = "Unauthorized read attempt by '{}' to read user '{}'".format(
					self.auth_user,
					user_obj.username,
				)
				auditor.create_entry('security', message)
			raise self.UnauthorizedActionException(
				"User '{}' is not authorized to see user '{}' details".format(
					self.auth_user, user_obj.username
				)
			)

	def update_user(self, user_obj, **kwargs):
		"""Updates user and user profile metadata.
			Args:
				user_obj (User): User object
				**kwargs (kwargs): Various attribute arguments 
			Returns: None
		"""
		user_authorization = UserAuthorization(self.auth_user, user_obj)
		if user_authorization.is_authorized_update():
			if auditing_enabled:
				auditor = Auditor(self.auth_user, user_obj)
				auditor.create_entry('update')
			user_service = UserService(user_obj)
			user_service.update(**kwargs)
		else:
			if auditing_enabled:
				auditor = Auditor(self.auth_user, user_obj)
				message = "Unauthorized update attempt by '{}' to update user '{}'".format(
					self.auth_user,
					user_obj.username,
				)
				auditor.create_entry('security', message)
			raise self.UnauthorizedActionException(
				"User '{}' is not authorized to update user '{}' details".format(
					self.auth_user, user_obj.username
				)
			)

	def delete_user(self, user_obj):
		"""'Deletes user permanently. Assigns the owner for all previously owned documents and folders to the NOBODY user.

			Args:
				user_obj (User): User object

			Returns: None
		"""
		user_authorization = UserAuthorization(self.auth_user, user_obj)
		if user_authorization.is_authorized_delete():
			if auditing_enabled:
				auditor = Auditor(self.auth_user, user_obj)
				auditor.create_entry('delete')
			user_service = UserService(user_obj)
			user_service.delete()
		else:
			if auditing_enabled:
				auditor = Auditor(self.auth_user, user_obj)
				message = "Unauthorized deletion attempt by '{}' to delete user '{}'".format(
					self.auth_user,
					user_obj.username,
				)
				auditor.create_entry('delete', message)
			raise self.UnauthorizedActionException(
				"User '{}' is not authorized to delete user '{}' details".format(
					self.auth_user, user_obj.username
				)
			)