"""FolderApi Class
"""
from apps.api.lib import logging, auditing_enabled, Api, Auditor, Folder, FolderAuthorization, FolderService, DocumentService, DocumentApi

logger = logging.getLogger(__name__)

class IllegalFolderApiOperation(Exception):

	def __init__(self, message=None):
		self.message = message

	def __str__(self):
		return self.message

class FolderApi(Api):
	"""The API used by clients for interacting with folders by going through 
	authorization and auditing modules (service modules).
	"""

	def __init__(self, auth_user):
		"""Sets the class auth_user. The auth_user is the one who is making the 
		service request.
		"""
		self.auth_user = auth_user

	def create_folder(self, name, parent_folder, owner, **kwargs):
		"""Create a folder. 
		Args: 
			name (string): Name of the document
			parent_folder (Folder): Destination folder for document
			owner (User): Owner user of the document

		Returns:
			bool: True for successful creation of a folder and False for unsuccessful attempt.

		"""

		folder_service = FolderService()
		folder_service.set_user(self.auth_user)
		if folder_service.is_in_trashcan_path(parent_folder):
			raise IllegalFolderApiOperation('Cannot create folder in path of user trashcan folder.')

		folder_authorization = FolderAuthorization(
			self.auth_user, parent_folder)
		if folder_authorization.is_authorized_creation():
			new_folder = FolderService().create(
				name, parent_folder, owner, **kwargs
			)
			if new_folder:
				if auditing_enabled:
					auditor = Auditor(self.auth_user, new_folder)
					auditor.create_entry('create')
				return True
			else:
				logger.error(
					'Authorized to create folder but unable to do so.')
				return False

		else:
			if auditing_enabled:
				auditor = Auditor(self.auth_user, Folder)
				message = "Unauthorized folder creation attempt by '{}' to create folder '{}'".format(
					self.auth_user,
					name,
				)
				auditor.create_entry('security', message)
			raise self.UnauthorizedActionException(
				"User '{}' is not authorized to create folder '{}' in folder '{}' owned by '{}'".format(
					self.auth_user, name, parent_folder, parent_folder.owner,
				)
			)

	def copy_folder(self, folder, destination_folder):

		folder_authorization = FolderAuthorization(
			self.auth_user, destination_folder
		)

		if folder_authorization.is_authorized_creation():

			folder_service = FolderService()
			folder_service.set_folder(folder)

			children = folder_service.get_children()

			destination_folder = destination_folder

			new_folder = folder
			new_folder.pk = None
			new_folder.parent_folder = destination_folder 
			new_folder.save()

			for document in children['documents']:
				document_api = DocumentApi(self.auth_user)
				document_api.copy_document(document, new_folder)

			for folder in children['folders']:
				self.copy_folder(folder, new_folder)

			if new_folder:
				if auditing_enabled:
					auditor = Auditor(self.auth_user, new_folder)
					auditor.create_entry('create')
				return True
			else:
				logger.error(
					'Authorized to create folder but unable to do so.')
				return False

		else:
			if auditing_enabled:
				auditor = Auditor(self.auth_user, Folder)
				message = "Unauthorized folder creation attempt by '{}' to create folder '{}'".format(
					self.auth_user,
					name,
				)
				auditor.create_entry('security', message)
			raise self.UnauthorizedActionException(
				"User '{}' is not authorized to create folder '{}' in folder '{}' owned by '{}'".format(
					self.auth_user, name, parent_folder, parent_folder.owner,
				)
			)

	def get_folder(self, folder_obj):
		"""Returns a folder object.

		Args: 
			folder_obj (Folder): Folder object

		Returns:
			Folder: Folder object or UnauthorizedActionException

		"""
		folder_authorization = FolderAuthorization(self.auth_user, folder_obj)
		if folder_authorization.is_authorized_read():
			if auditing_enabled:
				auditor = Auditor(self.auth_user, folder_obj)
				auditor.create_entry('read')
			return FolderService().get_folder(folder_obj)
		else:
			if auditing_enabled:
				auditor = Auditor(self.auth_user, folder_obj)
				message = "Unauthorized read attempt by '{}' to read folder '{}'".format(
					self.auth_user,
					folder_obj,
				)
				auditor.create_entry('security', message)
			raise self.UnauthorizedActionException(
				"User '{}' is not authorized to see folder '{}' details".format(
					self.auth_user, folder_obj.name
				)
			)

	def update_folder(self, folder_obj, **kwargs):
		"""Updates folder metadata.
			Args:
				folder_obj (Folder): Folder object
				**kwargs (kwargs): Various attribute arguments like
					* Title
					* Description
					* etc.
			Returns: None
		"""
		folder_authorization = FolderAuthorization(self.auth_user, folder_obj)
		if folder_authorization.is_authorized_update():
			if auditing_enabled:
				auditor = Auditor(self.auth_user, folder_obj)
				auditor.create_entry('update')
			folder_service = FolderService(folder_obj)
			folder_service.update(**kwargs)
		else:
			if auditing_enabled:
				auditor = Auditor(self.auth_user, folder_obj)
				message = "Unauthorized update attempt by '{}' to update folder '{}'".format(
					self.auth_user,
					folder_obj.name,
				)
				auditor.create_entry('security', message)
			raise self.UnauthorizedActionException(
				"User '{}' is not authorized to update folder '{}' details owned by {}".format(
					self.auth_user, folder_obj.name, folder_obj.owner,
				)
			)

	def delete_folder(self, folder_obj):
		"""'Deletes folder permanently. Unlike documents, folders are completely deleted.

			Args:
				folder_obj (Folder): Folder object

			Returns: None
		"""
		folder_authorization = FolderAuthorization(self.auth_user, folder_obj)
		if folder_authorization.is_authorized_delete():
			if auditing_enabled:
				auditor = Auditor(self.auth_user, folder_obj)
				auditor.create_entry('delete')
			folder_service = FolderService(folder_obj)
			folder_service.set_user(self.auth_user)
			folder_service.delete()
		else:
			if auditing_enabled:
				auditor = Auditor(self.auth_user, folder_obj)
				message = "Unauthorized deletion attempt by '{}' to delete folder '{}'".format(
					self.auth_user,
					folder_obj.name,
				)
				auditor.create_entry('delete', message)
			raise self.UnauthorizedActionException(
				"User '{}' is not authorized to delete folder '{}' owned by '{}'".format(
					self.auth_user, folder_obj.name, folder_obj.owner,
				)
			)
