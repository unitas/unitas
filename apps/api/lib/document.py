"""DocumentApi Class
"""

from __future__ import absolute_import

import os

from apps.api.lib import logging, auditing_enabled, Api, Auditor, Document, DocumentAuthorization, DocumentService, DocumentVersion
from apps.services.modules.transformation.config.transformation import xforms_enabled
from apps.services.modules.transformation.lib.transformation import Transformer
from apps.services.modules.search.config.index import content_indexing_enabled
from apps.services.modules.search.lib.index import ContentIndexer

logger = logging.getLogger(__name__)

class DocumentApi(Api):
	"""The API used by clients for interacting with documents by going through 
	authorization and auditing modules (service modules).
	"""

	def __init__(self, auth_user):
		"""Sets the class auth_user. The auth_user is the one who is making the 
		service request.
		"""
		self.auth_user = auth_user

	def create_document(self, name, parent_folder, file_name, owner, **kwargs):
		"""Create a document and new version. 
		Args: 
			name (string): Name of the document
			parent_folder (Folder): Destination folder for document
			file_name (File or str): Actual content file object of document
			owner (User): Owner user of the document

		Returns:
			bool: True for successful creation of document and False for unsuccessful attempt.

		"""
		document_authorization = DocumentAuthorization(
			self.auth_user, parent_folder)
		if document_authorization.is_authorized_creation():
			
			new_document = DocumentService().create(
				name, parent_folder, file_name, owner, **kwargs
			)

			if xforms_enabled:

				transformer = Transformer(new_document)
				preview_file = transformer.create_preview()

				if content_indexing_enabled and preview_file:

					content_indexer = ContentIndexer(
						new_document, preview_file)
					content_indexer.index()

					os.remove(preview_file)
			
			if new_document:
				
				if auditing_enabled:
					auditor = Auditor(self.auth_user, new_document)
					auditor.create_entry('create')
				return True
			
			else:
				
				logger.error(
					'Authorized to create document but unable to do so.')
				return False

		else:
			if auditing_enabled:
				auditor = Auditor(self.auth_user, Document)
				message = "Unauthorized document version creation attempt by '{}' to create document version.".format(
					self.auth_user,
				)
				auditor.create_entry('security', message)
			raise self.UnauthorizedActionException(
				"User '{}' is not authorized to create document version.".format(
					self.auth_user
				)
			)
		return False

	def copy_document(self, document, destination_folder):

		document_authorization = DocumentAuthorization(
			self.auth_user, document.parent_folder
		)

		if document_authorization.is_authorized_creation():

			src_document_uuid = document.uuid

			new_document = document
			new_document.pk = None
			new_document.parent_folder = destination_folder
			new_document.description = 'Copy of document: {}'.format(document)
			new_document.save()

			src_parent_document = Document.objects.get(uuid=src_document_uuid)

			for new_version in DocumentVersion.objects.filter(parent_document=src_parent_document):
				new_version.pk = None
				new_version.parent_document = new_document
				new_version.save()

			if new_document:
				
				if auditing_enabled:
					auditor = Auditor(self.auth_user, new_document)
					auditor.create_entry('create')

				if xforms_enabled:

					transformer = Transformer(new_document)
					preview_file = transformer.create_preview()

					if content_indexing_enabled and preview_file:

						content_indexer = ContentIndexer(
							new_document, preview_file)
						content_indexer.index()

						os.remove(preview_file)

				return True
			
			else:
				
				logger.error(
					'Authorized to create document but unable to do so.')
				return False

		else:
			if auditing_enabled:
				auditor = Auditor(self.auth_user, Document)
				message = "Unauthorized document creation attempt by '{}' to create document.".format(
					self.auth_user,
				)
				auditor.create_entry('security', message)
			raise self.UnauthorizedActionException(
				"User '{}' is not authorized to create document.".format(
					self.auth_user
				)
			)
		return False


			

	def add_new_version(self, document_obj, content_file, mode, **kwargs):

		document_authorization = DocumentAuthorization(
			self.auth_user, document_obj.parent_folder, document_obj
		)

		if document_authorization.is_authorized_creation():
			
			document_service = DocumentService()
			document_service.set_document(document_obj)
			new_version = document_service.add_new_version(
				content_file, mode, **kwargs
			)

			if xforms_enabled:

				transformer = Transformer(document_obj)
				transformer.create_preview()
				
			if new_version:
				if auditing_enabled:
					auditor = Auditor(self.auth_user, new_version)
					auditor.create_entry('create')
				return True
			else:
				logger.error('Authorized to create new version but unable to do so.')
				return False

		else:
			if auditing_enabled:
				auditor = Auditor(self.auth_user, DocumentVersion)
				message = "Unauthorized document creation attempt by '{}' to create document '{}' in folder '{}'".format(
					self.auth_user,
					name,
					parent_folder,
				)
				auditor.create_entry('security', message)
			raise self.UnauthorizedActionException(
				"User '{}' is not authorized to create document '{}' in folder '{}' owned by '{}'".format(
					self.auth_user, name, parent_folder, parent_folder.owner,
				)
			)
		return False

	def get_document(self, document_obj):
		"""Returns a document object.

		Args: 
			document_obj (Document): Document object

		Returns:
			Document: Document object or self.UnauthorizedActionException

		"""
		document_authorization = DocumentAuthorization(
			self.auth_user, document_obj.parent_folder, document_obj)
		if document_authorization.is_authorized_read():
			if auditing_enabled:
				auditor = Auditor(self.auth_user, document_obj)
				auditor.create_entry('read')
			return DocumentService().get_document(document_obj)
		else:
			if auditing_enabled:
				auditor = Auditor(self.auth_user, document_obj)
				message = "Unauthorized read attempt by '{}' to read document '{}'".format(
					self.auth_user,
					document_obj,
				)
				auditor.create_entry('security', message)
			raise self.UnauthorizedActionException(
				"User '{}' is not authorized to see document '{}' details".format(
					self.auth_user, document_obj.name
				)
			)

	def update_document(self, document_obj, **kwargs):
		"""Updates document metadata.
			Args:
				document_obj (Document): Document object
				**kwargs (kwargs): Various attribute arguments like
					* Title
					* Description
					* etc.
			Returns: None
		"""
		document_authorization = DocumentAuthorization(
			self.auth_user, document_obj.owner, document_obj)
		if document_authorization.is_authorized_update():
			document_service = DocumentService(document_obj)
			document_service.update(**kwargs)
			if auditing_enabled:
				auditor = Auditor(self.auth_user, document_obj)
				auditor.create_entry('update')
		else:
			if auditing_enabled:
				auditor = Auditor(self.auth_user, document_obj)
				message = "Unauthorized update attempt by '{}' to update document '{}'".format(
					self.auth_user,
					document_obj.name,
				)
				auditor.create_entry('security', message)
			raise self.UnauthorizedActionException(
				"User '{}' is not authorized to update document '{}' details owned by {}".format(
					self.auth_user, document_obj.name, document_obj.owner,
				)
			)

	def delete_document(self, document_obj):
		"""'Deletes' document. Actually, it sets is_deleted to true and sets the parent folder to the user's trashcan folder.

			Args:
				document_obj (Document): Document object

			Returns: None
		"""
		document_authorization = DocumentAuthorization(self.auth_user, document_obj.owner, document_obj)
		if document_authorization.is_authorized_delete():
			document_service = DocumentService(document_obj)
			document_service.delete()
			if auditing_enabled:
				auditor = Auditor(self.auth_user, document_obj)
				auditor.create_entry('delete')
		else:
			if auditing_enabled:
				auditor = Auditor(self.auth_user, document_obj)
				message = "Unauthorized deletion attempt by '{}' to delete document '{}'".format(
					self.auth_user,
					document_obj.name,
				)
				auditor.create_entry('delete', message)
			raise self.UnauthorizedActionException(
				"User '{}' is not authorized to delete document '{}' owned by '{}'".format(
					self.auth_user, document_obj.name, document_obj.owner,
				)
			)