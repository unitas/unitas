"""Abstract class for DocumentApi, FolderApi and UserApi
"""

import logging

logger = logging.getLogger(__name__)

class Api(object):

	class UnauthorizedActionException(Exception):
		"""Exception for unauthorized action repository service request.
		""" 

		def __init__(self, message):
			self.message = message

		def __str__(self):
			return self.message