"""Unit tests for DocumentApi class
"""

from apps.api.tests import Audit, Document, DocumentApi, DocumentService, Folder, FolderService, UnitasTestCase, UserService

sample_file = 'scripts/data/samples/Test Document.txt'

class DocumentApiTest(UnitasTestCase):
	"""Test for apps.api.lib.document class.
	"""

	def setUp(self):
		pass

	def get_root_folder(self):
		""" Return the system 'Root' folder. 
		"""
		return FolderService().get_root_folder()

	def get_shared_docs_folder(self):
		"""Return the system 'Shared Documents' Folder.
		"""
		return FolderService().get_shared_documents_folder()

	def get_non_priv_user(self, username='testuser'):
		"""Return an arbitrary non privilged user.
		""" 
		non_priv_user = UserService().create(
			'{}'.format(username),
			'secret',
			'{}@localhost'.format(username),
		)
		return non_priv_user

	def get_super_user(self, username='superuser'):
		"""Return an arbitrary non privileged user with is_superuser=True.
		"""
		super_user = UserService().create(
			'{}'.format(username),
			'secret',
			'{}@localhost'.format(username),
			is_superuser=True,
		)
		return super_user

	def testCreateDocumentByAdmin(self):
		"""Tests if an admin can create documents in certain situations.
		"""
		admin_user = UserService().get_admin_user()
		super_user = self.get_super_user()
		non_priv_user = self.get_non_priv_user()
		folder_service = FolderService()

		folder_service.set_user(admin_user)
		admin_user_home_folder = folder_service.get_user_home_folder()

		document_api = DocumentApi(admin_user)

		""" Admin should be able to create document anywhere except in other users' folders. """

		""" CAN """
		""" Admin folders. """
		
		document_api.create_document(
			'Document1.txt',
			admin_user_home_folder,
			sample_file,
			admin_user,
		)
		self.assertTrue(Document.objects.get(name='Document1.txt'))

		""" System folders. """
		document_api.create_document(
			'Document2.txt',
			self.get_root_folder(),
			sample_file,
			admin_user,
		)
		self.assertTrue(Document.objects.get(name='Document2.txt'))

		""" Everybody folders. """
		document_api.create_document(
			'Document3.txt',
			self.get_shared_docs_folder(),
			sample_file,
			admin_user,
		)
		self.assertTrue(Document.objects.get(name='Document3.txt'))

		""" Nobody's folders. """
		FolderService().create(
			'folder1',
			self.get_root_folder(),
			UserService().get_nobody_user(),
		)
		folder = Folder.objects.get(name='folder1')
		document_api.create_document(
			'Document4.txt',
			folder,
			sample_file,
			admin_user,
		)
		self.assertTrue(Document.objects.get(name='Document4.txt'))

		""" CANNOT """
		""" Non priv users' folders. """
		folder_service.set_user(non_priv_user)
		user_home_folder = folder_service.get_user_home_folder()
		with self.assertRaises(DocumentApi.UnauthorizedActionException):
			document_api.create_document(
				'Document5.txt',
				user_home_folder,
				sample_file,
				admin_user,
			)

		""" Super user's folders. """
		folder_service.set_user(super_user)
		user_home_folder = folder_service.get_user_home_folder()
		with self.assertRaises(DocumentApi.UnauthorizedActionException):
			document_api.create_document(
				'Document6.txt',
				user_home_folder,
				sample_file,
				admin_user,
			)

	def testCreateDocumentBySuperUser(self):
		"""Tests if a super user can create documents in certain situations.
		"""
		admin_user = UserService().get_admin_user()
		super_user = self.get_super_user()
		non_priv_user = self.get_non_priv_user()
		non_priv_user2 = self.get_non_priv_user(username='testuser2')
		folder_service = FolderService()

		folder_service.set_user(admin_user)
		admin_user_home_folder = folder_service.get_user_home_folder()

		document_api = DocumentApi(super_user)

		""" Super user should be able to create document anywhere except in other users' folders. """

		""" CAN """
		""" Super user's folders. """
		folder_service.set_user(super_user)
		user_home_folder = folder_service.get_user_home_folder()
		document_api.create_document(
			'Document6.txt',
			user_home_folder,
			sample_file,
			super_user,
		)
		
		""" Everybody folders. """
		document_api.create_document(
			'Document3.txt',
			self.get_shared_docs_folder(),
			sample_file,
			admin_user,
		)
		self.assertTrue(Document.objects.get(name='Document3.txt'))

		
		""" CANNOT """
		""" System folders. """
		with self.assertRaises(DocumentApi.UnauthorizedActionException):
			document_api.create_document(
				'Document2.txt',
				self.get_root_folder(),
				sample_file,
				super_user,
			)

		""" Nobody's folders. """
		FolderService().create(
			'folder1',
			self.get_root_folder(),
			UserService().get_nobody_user(),
		)
		folder = Folder.objects.get(name='folder1')
		with self.assertRaises(DocumentApi.UnauthorizedActionException):
			document_api.create_document(
				'Document4.txt',
				folder,
				sample_file,
				admin_user,
			)

		""" Admin folders. """	
		with self.assertRaises(DocumentApi.UnauthorizedActionException):	
			document_api.create_document(
				'Document1.txt',
				admin_user_home_folder,
				sample_file,
				admin_user,
			)

		""" Non priv users' folders. """
		folder_service.set_user(non_priv_user2)
		user_home_folder = folder_service.get_user_home_folder()
		with self.assertRaises(DocumentApi.UnauthorizedActionException):
			document_api.create_document(
				'Document5.txt',
				user_home_folder,
				sample_file,
				admin_user,
			)

	def testCreateDocumentByNonPrivUser(self):
		"""Tests if a non priv user can create documents in certain situations.
		"""
		admin_user = UserService().get_admin_user()
		super_user = self.get_super_user()
		non_priv_user = self.get_non_priv_user()
		non_priv_user2 = self.get_non_priv_user(username='testuser2')
		folder_service = FolderService()

		folder_service.set_user(admin_user)
		admin_user_home_folder = folder_service.get_user_home_folder()

		document_api = DocumentApi(non_priv_user)

		""" Super user should be able to create document anywhere except in other users' folders. """

		""" CAN """
		""" Non priv user folders. """
		folder_service.set_user(non_priv_user)
		user_home_folder = folder_service.get_user_home_folder()
		document_api.create_document(
			'Document6.txt',
			user_home_folder,
			sample_file,
			non_priv_user,
		)
		
		""" Everybody folders. """
		document_api.create_document(
			'Document3.txt',
			self.get_shared_docs_folder(),
			sample_file,
			non_priv_user,
		)
		self.assertTrue(Document.objects.get(name='Document3.txt'))

		""" CANNOT """
		""" System folders. """
		with self.assertRaises(DocumentApi.UnauthorizedActionException):
			document_api.create_document(
				'Document2.txt',
				self.get_root_folder(),
				sample_file,
				non_priv_user,
			)

		""" Nobody's folders. """
		FolderService().create(
			'folder1',
			self.get_root_folder(),
			UserService().get_nobody_user(),
		)
		folder = Folder.objects.get(name='folder1')
		with self.assertRaises(DocumentApi.UnauthorizedActionException):
			document_api.create_document(
				'Document4.txt',
				folder,
				sample_file,
				non_priv_user,
			)


		""" Admin folders. """	
		with self.assertRaises(DocumentApi.UnauthorizedActionException):	
			document_api.create_document(
				'Document1.txt',
				admin_user_home_folder,
				sample_file,
				non_priv_user,
			)

		""" Non priv users' folders. """
		folder_service.set_user(non_priv_user2)
		user_home_folder = folder_service.get_user_home_folder()
		with self.assertRaises(DocumentApi.UnauthorizedActionException):
			document_api.create_document(
				'Document5.txt',
				user_home_folder,
				sample_file,
				non_priv_user,
			)

	def testReadDocumentByAdmin(self):
		"""Tests if an admin can read documents in certain situations.
		"""
		admin_user = UserService().get_admin_user()
		super_user = self.get_super_user()
		non_priv_user = self.get_non_priv_user()
		nobody_user = UserService().get_nobody_user()

		folder_service = FolderService()
		document_service = DocumentService()

		folder_service.set_user(admin_user)
		user_home_folder = folder_service.get_user_home_folder()

		admin_owned_document = document_service.create(
			'AdminOwnedDocument.txt',
			user_home_folder,
			sample_file,
			admin_user,
		)

		folder_service.set_user(super_user)
		user_home_folder = folder_service.get_user_home_folder()

		super_user_owned_document = document_service.create(
			'Superuser_owned_document.txt',
			user_home_folder,
			sample_file,
			admin_user,
		)

		folder_service.set_user(non_priv_user)
		user_home_folder = folder_service.get_user_home_folder()

		non_priv_user_owned_document = document_service.create(
			'NonPrivUserOwnedDocument.txt',
			user_home_folder,
			sample_file,
			admin_user,
		)

		nobody_user_owned_document = document_service.create(
			'NonPrivUserOwnedDocument.txt',
			folder_service.get_root_folder(),
			sample_file,
			admin_user,
		)

		document_api = DocumentApi(admin_user)

		""" Admin should be able to read any document anywhere. """
		""" Owned by admin. """

		self.assertTrue(document_api.get_document(admin_owned_document))

		""" Owned by super_user. """

		self.assertTrue(document_api.get_document(super_user_owned_document))

		""" Owned by non_priv_user. """

		self.assertTrue(document_api.get_document(non_priv_user_owned_document))

		""" Owned by Nobody user. """

		self.assertTrue(document_api.get_document(nobody_user_owned_document))

		""" No documents can be created by everybody or system users. """

	def testReadDocumentBySuperUser(self):
		"""Tests if a super user can read documents in certain situations.
		"""
		admin_user = UserService().get_admin_user()
		super_user = self.get_super_user()
		non_priv_user = self.get_non_priv_user()
		nobody_user = UserService().get_nobody_user()

		folder_service = FolderService()
		document_service = DocumentService()

		folder_service.set_user(super_user)
		user_home_folder = folder_service.get_user_home_folder()

		admin_owned_document = document_service.create(
			'AdminOwnedDocument.txt',
			user_home_folder,
			sample_file,
			admin_user,
		)

		folder_service.set_user(super_user)
		user_home_folder = folder_service.get_user_home_folder()

		super_user_owned_document = document_service.create(
			'Superuser_owned_document.txt',
			user_home_folder,
			sample_file,
			super_user,
		)

		folder_service.set_user(non_priv_user)
		user_home_folder = folder_service.get_user_home_folder()

		non_priv_user_owned_document = document_service.create(
			'NonPrivUserOwnedDocument.txt',
			user_home_folder,
			sample_file,
			non_priv_user,
		)

		nobody_user_owned_document = document_service.create(
			'NonPrivUserOwnedDocument.txt',
			folder_service.get_root_folder(),
			sample_file,
			nobody_user,
		)

		document_api = DocumentApi(super_user)

		""" Super user should not be able to read documents owned by admin. """
		with self.assertRaises(DocumentApi.UnauthorizedActionException):
			document_api.get_document(admin_owned_document)

		""" Owned by super_user. """

		self.assertTrue(document_api.get_document(super_user_owned_document))

		""" Owned by non_priv_user. """
		with self.assertRaises(DocumentApi.UnauthorizedActionException):
			document_api.get_document(non_priv_user_owned_document)

		""" Owned by Nobody user. """

		self.assertTrue(document_api.get_document(nobody_user_owned_document))

		""" No documents can be created by everybody or system users. """

	def testReadDocumentByNonPrivUser(self):
		"""Tests if a non priv user can read documents in certain situations.
		"""
		admin_user = UserService().get_admin_user()
		super_user = self.get_super_user()
		non_priv_user = self.get_non_priv_user()
		non_priv_user2 = self.get_non_priv_user(username='testuser2')
		nobody_user = UserService().get_nobody_user()

		folder_service = FolderService()
		document_service = DocumentService()

		folder_service.set_user(admin_user)
		user_home_folder = folder_service.get_user_home_folder()

		admin_owned_document = document_service.create(
			'AdminOwnedDocument.txt',
			user_home_folder,
			sample_file,
			admin_user,
		)

		folder_service.set_user(super_user)
		user_home_folder = folder_service.get_user_home_folder()

		super_user_owned_document = document_service.create(
			'Superuser_owned_document.txt',
			user_home_folder,
			sample_file,
			super_user,
		)

		folder_service.set_user(non_priv_user)
		user_home_folder = folder_service.get_user_home_folder()

		non_priv_user_owned_document = document_service.create(
			'NonPrivUserOwnedDocument.txt',
			user_home_folder,
			sample_file,
			non_priv_user,
		)

		folder_service.set_user(non_priv_user2)
		user_home_folder = folder_service.get_user_home_folder()

		non_priv2_user_owned_document = document_service.create(
			'NonPrivUserOwnedDocument.txt',
			user_home_folder,
			sample_file,
			non_priv_user,
		)

		nobody_user_owned_document = document_service.create(
			'NobodyUserOwnedDocument.txt',
			folder_service.get_root_folder(),
			sample_file,
			nobody_user,
		)

		document_api = DocumentApi(non_priv_user)

		""" Non priv user should not be able to read documents owned by admin. """
		with self.assertRaises(DocumentApi.UnauthorizedActionException):
			document_api.get_document(admin_owned_document)

		""" Owned by super_user. """
		with self.assertRaises(DocumentApi.UnauthorizedActionException):
			document_api.get_document(super_user_owned_document)

		""" Owned by non_priv_user. """
		self.assertTrue(document_api.get_document(non_priv_user_owned_document))

		""" Owned by non_priv_user2. """
		with self.assertRaises(DocumentApi.UnauthorizedActionException):
			document_api.get_document(non_priv2_user_owned_document)

		""" Owned by Nobody user. """
		with self.assertRaises(DocumentApi.UnauthorizedActionException):
			document_api.get_document(nobody_user_owned_document)

		""" No documents can be created by everybody or system users. """

	def testUpdateByAdminUser(self):
		"""Tests if an admin can update documents in certain situations.
		"""
		admin_user = UserService().get_admin_user()
		super_user = self.get_super_user()
		non_priv_user = self.get_non_priv_user()
		nobody_user = UserService().get_nobody_user()

		folder_service = FolderService()
		document_service = DocumentService()

		folder_service.set_user(admin_user)
		user_home_folder = folder_service.get_user_home_folder()

		admin_owned_document = document_service.create(
			'AdminOwnedDocument.txt',
			user_home_folder,
			sample_file,
			admin_user,
		)

		folder_service.set_user(super_user)
		user_home_folder = folder_service.get_user_home_folder()

		super_user_owned_document = document_service.create(
			'Superuser_owned_document.txt',
			user_home_folder,
			sample_file,
			admin_user,
		)

		folder_service.set_user(non_priv_user)
		user_home_folder = folder_service.get_user_home_folder()

		non_priv_user_owned_document = document_service.create(
			'NonPrivUserOwnedDocument.txt',
			user_home_folder,
			sample_file,
			admin_user,
		)

		nobody_user_owned_document = document_service.create(
			'NonPrivUserOwnedDocument.txt',
			folder_service.get_root_folder(),
			sample_file,
			admin_user,
		)

		document_api = DocumentApi(admin_user)

		""" Admin should be able to update any document metadata. """

		""" Owned by admin. """
		document_api.update_document(
			admin_owned_document, title='Admin updated this')

		""" Owned by superuser. """
		document_api.update_document(
			super_user_owned_document, title='Admin updated this')

		""" Owned by non priv user. """
		document_api.update_document(
			non_priv_user_owned_document, title='Admin updated this')

		""" Owned by nobody user. """
		document_api.update_document(
			nobody_user_owned_document, title='Admin updated this')


	def testUpdateBySuperUser(self):
		"""Tests if a super user can update documents in certain situations.
		"""
		admin_user = UserService().get_admin_user()
		super_user = self.get_super_user()
		non_priv_user = self.get_non_priv_user()
		nobody_user = UserService().get_nobody_user()

		folder_service = FolderService()
		document_service = DocumentService()

		folder_service.set_user(admin_user)
		user_home_folder = folder_service.get_user_home_folder()

		admin_owned_document = document_service.create(
			'AdminOwnedDocument.txt',
			user_home_folder,
			sample_file,
			admin_user,
		)

		folder_service.set_user(super_user)
		user_home_folder = folder_service.get_user_home_folder()

		super_user_owned_document = document_service.create(
			'Superuser_owned_document.txt',
			user_home_folder,
			sample_file,
			super_user,
		)

		folder_service.set_user(non_priv_user)
		user_home_folder = folder_service.get_user_home_folder()

		non_priv_user_owned_document = document_service.create(
			'NonPrivUserOwnedDocument.txt',
			user_home_folder,
			sample_file,
			non_priv_user,
		)

		nobody_user_owned_document = document_service.create(
			'NonPrivUserOwnedDocument.txt',
			folder_service.get_root_folder(),
			sample_file,
			nobody_user,
		)

		document_api = DocumentApi(super_user)
		""" Admin should be able to update any document metadata. """

		""" Owned by admin. """
		with self.assertRaises(DocumentApi.UnauthorizedActionException):
			document_api.update_document(
				admin_owned_document, title='Admin updated this')

		""" Owned by superuser. """
		document_api.update_document(
			super_user_owned_document, title='Admin updated this')


		""" Owned by non priv user. """
		with self.assertRaises(DocumentApi.UnauthorizedActionException):
			document_api.update_document(
				non_priv_user_owned_document, title='Admin updated this')

		""" Owned by nobody user. """
		document_api.update_document(
			nobody_user_owned_document, title='Admin updated this')

	def testUpdateByNonPrivUser(self):
		"""Tests if a non priv user can update documents in certain situations.
		"""
		admin_user = UserService().get_admin_user()
		super_user = self.get_super_user()
		non_priv_user = self.get_non_priv_user()
		nobody_user = UserService().get_nobody_user()

		folder_service = FolderService()
		document_service = DocumentService()

		folder_service.set_user(admin_user)
		user_home_folder = folder_service.get_user_home_folder()

		admin_owned_document = document_service.create(
			'AdminOwnedDocument.txt',
			user_home_folder,
			sample_file,
			admin_user,
		)

		folder_service.set_user(super_user)
		user_home_folder = folder_service.get_user_home_folder()

		super_user_owned_document = document_service.create(
			'Superuser_owned_document.txt',
			user_home_folder,
			sample_file,
			super_user,
		)

		folder_service.set_user(non_priv_user)
		user_home_folder = folder_service.get_user_home_folder()

		non_priv_user_owned_document = document_service.create(
			'NonPrivUserOwnedDocument.txt',
			user_home_folder,
			sample_file,
			non_priv_user,
		)

		nobody_user_owned_document = document_service.create(
			'NonPrivUserOwnedDocument.txt',
			folder_service.get_root_folder(),
			sample_file,
			nobody_user,
		)

		document_api = DocumentApi(non_priv_user)
		""" Admin should be able to update any document metadata. """

		""" Owned by admin. """
		with self.assertRaises(DocumentApi.UnauthorizedActionException):
			document_api.update_document(
				admin_owned_document, title='Admin updated this')

		""" Owned by superuser. """
		with self.assertRaises(DocumentApi.UnauthorizedActionException):
			document_api.update_document(
				super_user_owned_document, title='Admin updated this')


		""" Owned by non priv user. """
		document_api.update_document(
			non_priv_user_owned_document, title='Admin updated this')

		""" Owned by nobody user. """
		with self.assertRaises(DocumentApi.UnauthorizedActionException):
			document_api.update_document(
				nobody_user_owned_document, title='Admin updated this')

	def testDeleteByAdminUser(self):
		"""Tests if an admin can delete documents in certain situations.
		"""
		admin_user = UserService().get_admin_user()
		super_user = self.get_super_user()
		non_priv_user = self.get_non_priv_user()
		nobody_user = UserService().get_nobody_user()

		folder_service = FolderService()
		document_service = DocumentService()

		folder_service.set_user(admin_user)
		user_home_folder = folder_service.get_user_home_folder()

		admin_owned_document = document_service.create(
			'AdminOwnedDocument.txt',
			user_home_folder,
			sample_file,
			admin_user,
		)

		folder_service.set_user(super_user)
		user_home_folder = folder_service.get_user_home_folder()

		super_user_owned_document = document_service.create(
			'Superuser_owned_document.txt',
			user_home_folder,
			sample_file,
			admin_user,
		)

		folder_service.set_user(non_priv_user)
		user_home_folder = folder_service.get_user_home_folder()

		non_priv_user_owned_document = document_service.create(
			'NonPrivUserOwnedDocument.txt',
			user_home_folder,
			sample_file,
			admin_user,
		)

		nobody_user_owned_document = document_service.create(
			'NonPrivUserOwnedDocument.txt',
			folder_service.get_root_folder(),
			sample_file,
			admin_user,
		)

		document_api = DocumentApi(admin_user)

		""" Admin should be able to update any document metadata. """

		""" Owned by admin. """
		document_api.delete_document(
			admin_owned_document)

		""" Owned by superuser. """
		document_api.delete_document(
			super_user_owned_document)

		""" Owned by non priv user. """
		document_api.delete_document(
			non_priv_user_owned_document)

		""" Owned by nobody user. """
		document_api.delete_document(
			nobody_user_owned_document)


	def testDeleteBySuperUser(self):
		"""Tests if a super user can delete documents in certain situations.
		"""
		admin_user = UserService().get_admin_user()
		super_user = self.get_super_user()
		non_priv_user = self.get_non_priv_user()
		nobody_user = UserService().get_nobody_user()

		folder_service = FolderService()
		document_service = DocumentService()

		folder_service.set_user(admin_user)
		user_home_folder = folder_service.get_user_home_folder()

		admin_owned_document = document_service.create(
			'AdminOwnedDocument.txt',
			user_home_folder,
			sample_file,
			admin_user,
		)

		folder_service.set_user(super_user)
		user_home_folder = folder_service.get_user_home_folder()

		super_user_owned_document = document_service.create(
			'Superuser_owned_document.txt',
			user_home_folder,
			sample_file,
			super_user,
		)

		folder_service.set_user(non_priv_user)
		user_home_folder = folder_service.get_user_home_folder()

		non_priv_user_owned_document = document_service.create(
			'NonPrivUserOwnedDocument.txt',
			user_home_folder,
			sample_file,
			non_priv_user,
		)

		nobody_user_owned_document = document_service.create(
			'NonPrivUserOwnedDocument.txt',
			folder_service.get_root_folder(),
			sample_file,
			nobody_user,
		)

		document_api = DocumentApi(super_user)
		""" Admin should be able to update any document metadata. """

		""" Owned by admin. """
		with self.assertRaises(DocumentApi.UnauthorizedActionException):
			document_api.delete_document(
				admin_owned_document)

		""" Owned by superuser. """
		document_api.delete_document(
			super_user_owned_document)


		""" Owned by non priv user. """
		with self.assertRaises(DocumentApi.UnauthorizedActionException):
			document_api.delete_document(
				non_priv_user_owned_document)

		""" Owned by nobody user. """
		with self.assertRaises(DocumentApi.UnauthorizedActionException):
			document_api.delete_document(
				nobody_user_owned_document)

	def testDeleteByNonPrivUser(self):
		"""Tests if a non priv user can delete documents in certain situations.
		"""
		admin_user = UserService().get_admin_user()
		super_user = self.get_super_user()
		non_priv_user = self.get_non_priv_user()
		nobody_user = UserService().get_nobody_user()

		folder_service = FolderService()
		document_service = DocumentService()

		folder_service.set_user(admin_user)
		user_home_folder = folder_service.get_user_home_folder()

		admin_owned_document = document_service.create(
			'AdminOwnedDocument.txt',
			user_home_folder,
			sample_file,
			admin_user,
		)

		folder_service.set_user(super_user)
		user_home_folder = folder_service.get_user_home_folder()

		super_user_owned_document = document_service.create(
			'Superuser_owned_document.txt',
			user_home_folder,
			sample_file,
			super_user,
		)

		folder_service.set_user(non_priv_user)
		user_home_folder = folder_service.get_user_home_folder()

		non_priv_user_owned_document = document_service.create(
			'NonPrivUserOwnedDocument.txt',
			user_home_folder,
			sample_file,
			non_priv_user,
		)

		nobody_user_owned_document = document_service.create(
			'NonPrivUserOwnedDocument.txt',
			folder_service.get_root_folder(),
			sample_file,
			nobody_user,
		)

		document_api = DocumentApi(non_priv_user)
		""" Admin should be able to update any document metadata. """

		""" Owned by admin. """
		with self.assertRaises(DocumentApi.UnauthorizedActionException):
			document_api.delete_document(
				admin_owned_document)

		""" Owned by superuser. """
		with self.assertRaises(DocumentApi.UnauthorizedActionException):
			document_api.delete_document(
				super_user_owned_document)


		""" Owned by non priv user. """
		document_api.delete_document(
			non_priv_user_owned_document)

		""" Owned by nobody user. """
		with self.assertRaises(DocumentApi.UnauthorizedActionException):
			document_api.delete_document(
				nobody_user_owned_document)