"""Unit tests for FolderApi class
"""

from apps.api.tests import Audit, Folder, FolderApi, FolderService, UnitasTestCase, UserService

class FolderApiTest(UnitasTestCase):
	"""Test for apps.api.lib.folder.
	"""

	def setUp(self):
		pass

	def get_root_folder(self):
		""" Return the system 'Root' folder. 
		"""
		return FolderService().get_root_folder()

	def get_non_priv_user(self, username='nonprivuser'):
		"""Return an arbitrary non priviliged user.
		""" 
		return UserService().create(
			username,
			'secret',
			'{}@localhost'.format(username),
		)

	def get_super_user(self, username='superuser'):
		"""Return an arbitrary non privileged user with is_superuser=True.
		"""
		super_user = self.get_non_priv_user(username)
		super_user.is_superuser = True
		super_user.save()
		return super_user

	def testInit(self):
		user = UserService().get_admin_user()
		folder_api = FolderApi(user)
		self.assertEqual(folder_api.auth_user, user)

	def testCreateFolderByAdmin(self):
		"""Tests if an admin can create folders in certain situations.
		"""
		admin_user = UserService().get_admin_user()
		root_folder = self.get_root_folder()

		folder_api = FolderApi(admin_user)

		""" An admin user should be able to create a folder anywhere. """
		folder_api.create_folder(
			'Folder1',
			root_folder,
			admin_user,
		)
		folder = Folder.objects.get(name='Folder1', parent_folder=root_folder)
		self.assertTrue(folder)

		""" When a folder is created with auditing_enabled=True, there should be an associated audit entry."""
		self.assertTrue(Audit.objects.get(operation='create'))

		""" An admin user should be able to create a folder in folder owned by user 'EVERYBODY' and 'NOBODY' """

		shared_docs_folder = FolderService().get_shared_documents_folder()

		folder_api.create_folder(
			'Folder2',
			shared_docs_folder,
			admin_user,
		)
		folder2 = Folder.objects.get(
			name='Folder2', parent_folder=shared_docs_folder)
		self.assertTrue(folder2)

		nobody_owned_folder = FolderService().create(
			'Folder3',
			root_folder,
			UserService().get_nobody_user(),
		)

		folder_api.create_folder(
			'Folder3_1',
			nobody_owned_folder,
			admin_user,
		)

		self.assertTrue(Folder.objects.get(name='Folder3_1'))

	def testCreateFolderBySuperUser(self):
		"""Tests if a super user can create folders in certain situations.
		"""
		admin_user = UserService().get_admin_user()
		root_folder = self.get_root_folder()
		super_user = self.get_super_user()
		
		folder_api = FolderApi(super_user)

		""" Super User should be able to create folders anywhere except in Admin user folder. """

		folder_api.create_folder(
			'Folder1',
			root_folder,
			super_user,
		)

		self.assertTrue(Folder.objects.get(name='Folder1', owner=super_user))

		folder_service = FolderService()
		folder_service.set_user(admin_user)
		admin_user_home_folder = folder_service.get_user_home_folder()

		with self.assertRaises(FolderApi.UnauthorizedActionException):
			folder_api.create_folder(
				'Folder2',
				admin_user_home_folder,
				super_user,
			)

		shared_docs_folder = FolderService().get_shared_documents_folder()

		folder_api.create_folder(
			'Folder2',
			shared_docs_folder,
			super_user,
		)
		folder2 = Folder.objects.get(
			name='Folder2', parent_folder=shared_docs_folder)
		self.assertTrue(folder2)

		nobody_owned_folder = FolderService().create(
			'Folder3',
			root_folder,
			UserService().get_nobody_user(),
		)

		folder_api.create_folder(
			'Folder3_1',
			nobody_owned_folder,
			super_user,
		)

		self.assertTrue(Folder.objects.get(name='Folder3_1'))

	def testCreateFolderByNonPrivUser(self):
		"""Tests if a non priv user can create folders in certain situations.
		"""
		admin_user = UserService().get_admin_user()
		super_user = self.get_super_user()
		non_priv_user = self.get_non_priv_user()
		folder_service = FolderService()
		folder_service.set_user(non_priv_user)
		testuser1 = self.get_non_priv_user(username='testuser1')

		user_home_folder = folder_service.get_user_home_folder()
		shared_docs_folder = folder_service.get_shared_documents_folder()
		root_folder = self.get_root_folder()
		folder_service.set_user(testuser1)
		test_user1_home_folder = folder_service.get_user_home_folder()

		folder_api = FolderApi(non_priv_user)

		""" Non priv user should be able to create a folder in a folder owned by themselves. """

		folder_api.create_folder(
			'Folder1',
			user_home_folder,
			non_priv_user,
		)

		self.assertTrue(Folder.objects.get(
			name='Folder1', parent_folder=user_home_folder, owner=non_priv_user))

		""" Non priv user should be able to create a folder in a folder owned by user 'EVERYBODY'. """

		folder_api.create_folder(
			'Folder2',
			shared_docs_folder,
			non_priv_user,
		)

		self.assertTrue(Folder.objects.get(
			name='Folder2', parent_folder=shared_docs_folder, owner=non_priv_user,
		))

		""" Non priv user should not be able to create a folder anywhere else. """
		with self.assertRaises(FolderApi.UnauthorizedActionException):
			folder_api.create_folder(
				'Folder3',
				root_folder,
				non_priv_user,
			)

		with self.assertRaises(FolderApi.UnauthorizedActionException):
			folder_api.create_folder(
				'Folder4',
				test_user1_home_folder,
				non_priv_user,
			)

		""" Admin user should be able to create folders in the two folders above. """
		folder_api = FolderApi(admin_user)

		folder_api.create_folder(
			'Folder4',
			test_user1_home_folder,
			admin_user,
		)

		self.assertTrue(Folder.objects.get(name='Folder4', parent_folder=test_user1_home_folder, owner=admin_user))

		""" Super user should be able to create folders in non priv user's folder. """
		folder_api = FolderApi(super_user)

		folder_api.create_folder(
			'Folder5',
			test_user1_home_folder,
			super_user,
		)

		self.assertTrue(
			Folder.objects.get(
				name='Folder5', 
				parent_folder=test_user1_home_folder, 
				owner=super_user
			)
		)

	def testReadFolderByAdminUser(self):
		"""Tests if an admin can read folders in certain situations.
		"""
		admin_user = UserService().get_admin_user()
		non_priv_user = self.get_non_priv_user()

		folder_api = FolderApi(admin_user)
		folder_service = FolderService()
		
		root_folder = self.get_root_folder()
		shared_docs_folder = FolderService().get_shared_documents_folder()

		""" Admin user should be able to read any folder owned by anyone. """
		""" Read system folders """
		folder = folder_api.get_folder(root_folder)
		self.assertEqual(folder, root_folder)

		folder = folder_api.get_folder(shared_docs_folder)
		self.assertEqual(folder, shared_docs_folder)

		""" Read non priv user folders """
		folder_service.set_user(non_priv_user)
		user_home_folder = folder_service.get_user_home_folder()
		folder = folder_api.get_folder(user_home_folder)

	def testReadFolderBySuperUser(self):
		"""Tests if a super user can read folders in certain situations.
		"""
		admin_user = UserService().get_admin_user()
		super_user = self.get_super_user()
		non_priv_user = self.get_non_priv_user()

		folder_service = FolderService()

		""" Super user should be able to read any folder owned by anyone except owned by admin user. """
		folder_api = FolderApi(super_user)
		folder_service.set_user(super_user)
		user_home_folder = folder_service.get_user_home_folder()
		folder = folder_api.get_folder(user_home_folder)
		self.assertEqual(folder, user_home_folder)

		folder_service.set_user(non_priv_user)
		user_home_folder = folder_service.get_user_home_folder()
		folder = folder_api.get_folder(user_home_folder)
		self.assertEqual(folder, user_home_folder)

		folder_service.set_user(admin_user)
		user_home_folder = folder_service.get_user_home_folder()
		with self.assertRaises(FolderApi.UnauthorizedActionException):
			folder = folder_api.get_folder(user_home_folder)

		shared_docs_folder = folder_service.get_shared_documents_folder()
		self.assertTrue(shared_docs_folder)

		home_folder = folder_service.get_home_folder()
		self.assertTrue(home_folder)

	def testReadFolderByNonPrivUser(self):
		"""Tests if a non priv user can read folders in certain situations.
		"""
		folder_service = FolderService()
		root_folder = self.get_root_folder()

		admin_user = UserService().get_admin_user()
		non_priv_user = self.get_non_priv_user()
		non_priv_user2 = self.get_non_priv_user('nonprivuser2')
		nobody_user = UserService().get_nobody_user()

		folder_api = FolderApi(non_priv_user)
		folder_service.set_user(non_priv_user)

		user_home_folder = folder_service.get_user_home_folder()
		shared_docs_folder = folder_service.get_shared_documents_folder()

		""" Non priv user should be able to read their own folders. """
		folder = folder_api.get_folder(user_home_folder)
		self.assertEqual(folder, user_home_folder)

		""" Non priv user should be able to read system and everybody folders. """
		folder = folder_api.get_folder(shared_docs_folder)
		self.assertEqual(folder, shared_docs_folder)

		""" Non priv user should not be able to read other nonpriv user folders. """
		folder_service.set_user(non_priv_user2)
		user_home_folder = folder_service.get_user_home_folder()
		with self.assertRaises(FolderApi.UnauthorizedActionException):
			folder = folder_api.get_folder(user_home_folder)

		""" Non priv user should not be able to read folders owned by NOBODY. """
		folder_owned_by_nobody = folder_service.create(
			'Folder1',
			root_folder,
			nobody_user,
		)
		with self.assertRaises(FolderApi.UnauthorizedActionException):
			folder = folder_api.get_folder(folder_owned_by_nobody)

		""" Non priv user should not be able to read folders owned by admin user."""
		folder_service.set_user(admin_user)
		user_home_folder = folder_service.get_user_home_folder()
		with self.assertRaises(FolderApi.UnauthorizedActionException):
			folder = folder_api.get_folder(user_home_folder)

	def testUpdateFolderByAdminUser(self):
		"""Tests if an admin can update folders in certain situations.
		"""
		admin_user = UserService().get_admin_user()
		non_priv_user = self.get_non_priv_user()
		folder_api = FolderApi(admin_user)
		folder_service = FolderService()
		root_folder = FolderService().get_root_folder()

		""" Admin should be able to update any folders except those owned by SYSTEM user. """
		folder_service.set_user(admin_user)
		user_home_folder = folder_service.get_user_home_folder()
		folder_api.update_folder(user_home_folder, title='The admin home folder')
		folder = folder_service.get_user_home_folder()
		self.assertEqual(folder.title, 'The admin home folder')

		shared_docs_folder = folder_service.get_shared_documents_folder()
		folder_api.update_folder(
			shared_docs_folder, title='The shared docs folder')
		folder = folder_service.get_shared_documents_folder()
		self.assertEqual(folder.title, 'The shared docs folder')

		nobody_user = UserService().get_nobody_user()
		folder_owned_by_nobody = FolderService().create(
			'Folder1',
			root_folder,
			nobody_user,
		)
		folder_api.update_folder(
			folder_owned_by_nobody, title="Nobody's folder")
		folder = Folder.objects.get(name='Folder1', owner=nobody_user)
		self.assertEqual(folder.title, "Nobody's folder")

		folder_service.set_user(non_priv_user)
		user_home_folder = folder_service.get_user_home_folder()
		folder_owned_by_non_priv_user = FolderService().create(
			'Folder2',
			user_home_folder,
			non_priv_user,
		)
		folder_api.update_folder(
			folder_owned_by_non_priv_user, title='Non priv user folder')
		folder = Folder.objects.get(name='Folder2', owner=non_priv_user)
		self.assertEqual(folder.title, 'Non priv user folder')

		with self.assertRaises(FolderApi.UnauthorizedActionException):
			folder_api.update_folder(
				root_folder, title='The ROOT folda!',
			)

	def testUpdateFolderBySuperUser(self):
		"""Tests if a super user can update folders in certain situations.
		"""
		non_priv_user = self.get_non_priv_user()
		admin_user = UserService().get_admin_user()
		super_user = self.get_super_user()
		everybody_user = UserService().get_everybody_user()
		folder_api = FolderApi(super_user)
		folder_service = FolderService()
		folder_service.set_user(super_user)
		root_folder = self.get_root_folder()
		nobody_user = UserService().get_nobody_user()
		system_user = UserService().get_system_user()

		""" Super user should be able to update only his own folders and folders owned by NOBODY. """
		user_home_folder = folder_service.get_user_home_folder()
		folder_owned_by_superuser = folder_service.create(
			'Folder1',
			user_home_folder,
			super_user,
		)
		folder_api.update_folder(
			folder_owned_by_superuser, title='Super user folder')
		folder = Folder.objects.get(name='Folder1')
		self.assertEqual(folder.title, 'Super user folder')

		folder_owned_by_nobody = folder_service.create(
			'Folder2',
			root_folder,
			nobody_user,
		)
		folder_api.update_folder(
			folder_owned_by_nobody, title='Super user folder')
		folder = Folder.objects.get(name='Folder2')
		self.assertEqual(folder.title, 'Super user folder')

		""" Not folder owned by admin. """
		folder_service.set_user(admin_user)
		admin_user_home_folder = folder_service.get_user_home_folder()
		with self.assertRaises(FolderApi.UnauthorizedActionException):
			folder_api.update_folder(
				admin_user_home_folder, title='Super user folder')

		""" Not folder owned by non_priv_user. """
		folder_service.set_user(non_priv_user)
		non_priv_user_home_folder = folder_service.get_user_home_folder()
		with self.assertRaises(FolderApi.UnauthorizedActionException):
			folder_api.update_folder(
				non_priv_user_home_folder, title='Super user folder',
			)
		""" Not folder owned by EVERYBODY. """
		folderOwnedByEverybody = folder_service.create(
			'Folder3',
			root_folder,
			everybody_user,
		)
		with self.assertRaises(FolderApi.UnauthorizedActionException):
			folder_api.update_folder(
				folderOwnedByEverybody,
				title='Super user folder',
			)

		""" Not folder owned by SYSTEM. """
		folder_owned_by_system = folder_service.create(
			'Folder4',
			root_folder,
			system_user,
		)
		with self.assertRaises(FolderApi.UnauthorizedActionException):
			folder_api.update_folder(
				folder_owned_by_system,
				title='Super user folder',
			)

	def get_folder_owned_by(self, user, folder_name):
		"""Returns a simple folder owned by a user and sets a folder name.
		"""
		folder_service = FolderService()
		folder_service.set_user(user)
		user_home_folder = folder_service.get_user_home_folder()
		folder = folder_service.create(
			folder_name,
			user_home_folder,
			user,
		)
		return folder

	def testDeleteByAdmin(self):
		"""Tests if an admin can delete folders in certain situations.
		"""
		admin_user = UserService().get_admin_user()

		folder_api = FolderApi(admin_user)

		""" Admin should be able to delete any folder except those owned by the system user. """
		""" Folder owned by non priv user. """

		folder = self.get_folder_owned_by(
			self.get_non_priv_user('testuser1'),
			'folder1',
		)
		folder_api.delete_folder(folder)
		folder = Folder.objects.get(name='folder1')
		self.assertEqual(folder.parent_folder.name, 'Trashcan')

		""" Folder owned by admin."""
		folder = self.get_folder_owned_by(
			admin_user,
			'folder2',
		)
		folder_api.delete_folder(folder)
		folder = Folder.objects.get(name='folder2')
		self.assertEqual(folder.parent_folder.name, 'Trashcan')
		
		""" Folder owned by nobody user. """
		folder = FolderService().create(
			'folder3',
			self.get_root_folder(),
			UserService().get_nobody_user(),
		)
		folder_api.delete_folder(folder)
		folder = Folder.objects.get(name='folder3')
		self.assertEqual(folder.parent_folder.name, 'Trashcan')

		""" Folder owned by everybody user. """
		folder = FolderService().create(
			'folder4',
			self.get_root_folder(),
			UserService().get_everybody_user(),
		)
		folder_api.delete_folder(folder)
		folder = Folder.objects.get(name='folder4')
		self.assertEqual(folder.parent_folder.name, 'Trashcan')

		""" Folder owned by system user. """
		with self.assertRaises(FolderApi.UnauthorizedActionException):
			folder_api.delete_folder(self.get_root_folder())

	def testDeleteBySuperUser(self):
		"""Tests if a super user can delete folders in certain situations.
		"""
		super_user = self.get_super_user()
		admin_user = UserService().get_admin_user()

		folder_api = FolderApi(super_user)

		""" Super user should be able to delete any folder except those owned by the system user, nobody user, everybody user and those of non priv users. """

		""" Folder owned by this user. """
		folder = self.get_folder_owned_by(
			super_user,
			'folder5',
		)
		folder_api.delete_folder(folder)
		folder = Folder.objects.get(name='folder5')
		self.assertEqual(folder.parent_folder.name, 'Trashcan')

		""" Folder owned by non priv user. """
		folder = self.get_folder_owned_by(
			self.get_non_priv_user('testuser1'),
			'folder1',
		)
		with self.assertRaises(FolderApi.UnauthorizedActionException):
			folder_api.delete_folder(folder)
		
		""" Folder owned by admin."""
		folder = self.get_folder_owned_by(
			admin_user,
			'folder2',
		)
		with self.assertRaises(FolderApi.UnauthorizedActionException):
			folder_api.delete_folder(folder)
		

		""" Folder owned by nobody user. """
		folder = FolderService().create(
			'folder3',
			self.get_root_folder(),
			UserService().get_nobody_user(),
		)

		with self.assertRaises(FolderApi.UnauthorizedActionException):
			folder_api.delete_folder(folder)
		
		""" Folder owned by everybody user. """
		folder = FolderService().create(
			'folder4',
			self.get_root_folder(),
			UserService().get_everybody_user(),
		)
		with self.assertRaises(FolderApi.UnauthorizedActionException):
			folder_api.delete_folder(folder)
		
		""" Folder owned by system user. """
		with self.assertRaises(FolderApi.UnauthorizedActionException):
			folder_api.delete_folder(self.get_root_folder())

	def testDeleteByNonPrivUser(self):
		"""Tests if a non priv user can delete folders in certain situations.
		"""
		non_priv_user = self.get_non_priv_user()
		admin_user = UserService().get_admin_user()

		folder_api = FolderApi(non_priv_user)

		""" Super user should be able to delete any folder except those owned by the system user, nobody user, everybody user and those of non priv users. """

		""" Folder owned by this user. """
		folder = self.get_folder_owned_by(
			non_priv_user,
			'folder5',
		)
		folder_api.delete_folder(folder)
		folder = Folder.objects.get(name='folder5')
		self.assertEqual(folder.parent_folder.name, 'Trashcan')
		
		""" Folder owned by admin."""
		folder = self.get_folder_owned_by(
			admin_user,
			'folder2',
		)
		with self.assertRaises(FolderApi.UnauthorizedActionException):
			folder_api.delete_folder(folder)
		

		""" Folder owned by nobody user. """
		folder = FolderService().create(
			'folder3',
			self.get_root_folder(),
			UserService().get_nobody_user(),
		)

		with self.assertRaises(FolderApi.UnauthorizedActionException):
			folder_api.delete_folder(folder)
		
		""" Folder owned by everybody user. """
		folder = FolderService().create(
			'folder4',
			self.get_root_folder(),
			UserService().get_everybody_user(),
		)
		with self.assertRaises(FolderApi.UnauthorizedActionException):
			folder_api.delete_folder(folder)
		
		""" Folder owned by system user. """
		with self.assertRaises(FolderApi.UnauthorizedActionException):
			folder_api.delete_folder(self.get_root_folder())

