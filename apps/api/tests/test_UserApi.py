"""Unit tests for UserApi class
"""

from apps.api.tests import Audit, User, UserApi, UserService, UnitasTestCaseNoMimeTypes

class UserApiTest(UnitasTestCaseNoMimeTypes):
	"""Test for apps.api.lib.user.
	"""

	def setUp(self):
		pass

	def get_non_priv_user(self, username='nonprivuser'):
		"""Return an arbitrary non priviliged user.
		""" 
		return UserService().create(
			username,
			'secret',
			'{}@localhost'.format(username),
		)

	def testInit(self):
		user = UserService().get_admin_user()
		user_api = UserApi(user)
		self.assertEqual(user_api.auth_user, user)

	def testCreateUserByAdmin(self):
		"""Tests if an admin can create users.
		"""
		admin_user = UserService().get_admin_user()
		user_api = UserApi(admin_user)
		user_api.create_user(
			'testuser',
			'secret',
			'testuser@localhost',
		)

		self.assertTrue(User.objects.get(username='testuser'))

		audit = Audit.objects.get(user=admin_user)
		self.assertTrue(audit.user, admin_user)
		self.assertTrue(
			audit.object_id, User.objects.get(username='testuser').id)
		self.assertTrue(audit.operation, 'read')

	def testCreateUserByNonPrivUser(self):
		"""Tests if non priv user can create users.
		"""
		non_priv_user = self.get_non_priv_user()
		user_api = UserApi(non_priv_user)

		with self.assertRaises(UserApi.UnauthorizedActionException):
			user_api.create_user(
				'testuser',
				'secret',
				'testuser@localhost',
			)


		with self.assertRaises(Audit.DoesNotExist):
			audit = Audit.objects.get(user=non_priv_user, object_id=5)

		self.assertTrue(Audit.objects.get(operation='security'))

	def testCreateUserBySuperUser(self):
		"""Tests if a super user can create users.
		"""
		super_user = self.get_non_priv_user()
		super_user.is_superuser=True
		super_user.save()

		user_api = UserApi(super_user)

		user_api.create_user(
			'testuser',
			'secret',
			'testuser@localhost',
		)

		self.assertTrue(User.objects.get(username='testuser'))

		audit = Audit.objects.get(user=super_user)
		self.assertTrue(audit.user, super_user)
		self.assertTrue(
			audit.object_id, User.objects.get(username='testuser').id)
		self.assertTrue(audit.operation, 'read')

	def testGetUserByAdmin(self):
		"""Tests if an admin can get users.
		"""
		admin_user = UserService().get_admin_user()
		non_priv_user = self.get_non_priv_user()

		user_api = UserApi(admin_user)
		user = user_api.get_user(non_priv_user)
		self.assertEqual(user.username, 'nonprivuser')

		user = user_api.get_user(admin_user)
		self.assertEqual(user.username, 'admin')

	def testGetUserBySuperUser(self):
		"""Tests if a super user can get users.
		"""
		admin_user = UserService().get_admin_user()
		non_priv_user = self.get_non_priv_user()
		non_priv_user.is_superuser = True
		non_priv_user.save()
		super_user = non_priv_user

		non_priv_user = self.get_non_priv_user('testuser2')

		user_api = UserApi(super_user)

		user = user_api.get_user(non_priv_user)
		self.assertEqual(user.username, 'testuser2')

		user = user_api.get_user(super_user)
		self.assertEqual(user.username, 'nonprivuser')

		with self.assertRaises(UserApi.UnauthorizedActionException):
			user = user_api.get_user(admin_user)

	def testGetUserByNonPrivUser(self):
		"""Tests if a non priv user can get users.
		"""
		non_priv_user = self.get_non_priv_user()
		admin_user = UserService().get_admin_user()
		non_priv_user2 = self.get_non_priv_user('testuser2')

		user_api = UserApi(non_priv_user)

		with self.assertRaises(UserApi.UnauthorizedActionException):
			user = user_api.get_user(admin_user)

		self.assertTrue(user_api.get_user(non_priv_user))

		with self.assertRaises(UserApi.UnauthorizedActionException):
			user = user_api.get_user(non_priv_user2)

	def testUpdateUserByAdminUser(self):
		"""Tests if an admin can update users.
		"""
		admin_user = UserService().get_admin_user()
		user_api = UserApi(admin_user)
		user_api.update_user(admin_user, email='administrator@localhost')

		user = User.objects.get(username='admin')
		self.assertEqual(user.email, 'administrator@localhost')

		non_priv_user = self.get_non_priv_user()
		user_api.update_user(non_priv_user, email='jeremy@wispy.org')

		user = User.objects.get(username='nonprivuser')
		self.assertEqual(user.email, 'jeremy@wispy.org')

	def testUpdateUserBySuperUser(self):
		"""Tests if a super user can update users.
		"""
		super_user = self.get_non_priv_user()
		super_user.is_superuser = True
		super_user.save()

		test_user = self.get_non_priv_user('testuser2')
		admin_user = UserService().get_admin_user()
		user_api = UserApi(super_user)

		""" Super user should be able to update himself."""
		user_api.update_user(super_user, email='testuser1@acme.org')
		user = User.objects.get(username='nonprivuser')
		self.assertEqual(user.email, 'testuser1@acme.org')

		""" Super user should be able to update a non-priv user."""
		user_api.update_user(test_user, email='testuser2@acme.org')
		user = User.objects.get(id=test_user.id)
		self.assertEqual(user.email, 'testuser2@acme.org')

		""" Super user should not be able to update the admin user. """
		with self.assertRaises(UserApi.UnauthorizedActionException):
			user_api.update_user(admin_user, email='admin@acme.org')

	def testUpdateUserByNonPrivUser(self):
		"""Tests if a non priv user can update users.
		"""
		super_user = self.get_non_priv_user()
		super_user.is_superuser = True
		super_user.save()

		test_user = self.get_non_priv_user('testuser2')
		test_user2 = self.get_non_priv_user('testuser3')
		admin_user = UserService().get_admin_user()
		user_api = UserApi(test_user)

		""" Non priv user should be able to update himself."""
		user_api.update_user(test_user, email='testuser2@acme.org')
		user = User.objects.get(username='testuser2')
		self.assertEqual(user.email, 'testuser2@acme.org')

		""" Non priv user should not be able to update another non-priv user."""
		with self.assertRaises(UserApi.UnauthorizedActionException):
			user_api.update_user(test_user2, email='testuser2@acme.org')
		
		""" Non user should not be able to update the admin user. """
		with self.assertRaises(UserApi.UnauthorizedActionException):
			user_api.update_user(admin_user, email='admin@acme.org')

		""" None priv user should not be able to update a super user. """
		with self.assertRaises(UserApi.UnauthorizedActionException):
			user_api.update_user(super_user, email='superuser@acme.org')

	def testDeleteUserByAdminUser(self):
		"""Tests if an admin can delete users.
		"""
		admin_user = UserService().get_admin_user()
		super_user = self.get_non_priv_user('superuser')
		super_user.is_superuser = True
		super_user.save()

		non_priv_user = self.get_non_priv_user()

		user_api = UserApi(admin_user)

		""" Admin user should be able to delete any user. """
		user_api.delete_user(super_user)
		user_api.delete_user(non_priv_user)

		with self.assertRaises(User.DoesNotExist):
			user = User.objects.get(username='superuser')

		with self.assertRaises(User.DoesNotExist):
			user = User.objects.get(username='nonprivuser')

		""" Admin user should not be able to delete himself. """
		with self.assertRaises(UserApi.UnauthorizedActionException):
			user_api.delete_user(admin_user)

	def testDeleteUserBySuperUser(self):
		"""Tests if an a super user can delete users.
		"""
		admin_user = UserService().get_admin_user()

		super_user = self.get_non_priv_user('superuser')
		super_user.is_superuser = True
		super_user.save()

		non_priv_user = self.get_non_priv_user()

		user_api = UserApi(super_user)

		""" Super user should not be able to delete any user. """
		with self.assertRaises(UserApi.UnauthorizedActionException):
			user_api.delete_user(admin_user)

		with self.assertRaises(UserApi.UnauthorizedActionException):
			user_api.delete_user(super_user)

		user_api.delete_user(non_priv_user)

	def testDeleteUserByNonPrivUser(self):
		"""Tests if a non priv user can delete users.
		"""
		admin_user = UserService().get_admin_user()

		super_user = self.get_non_priv_user('superuser')
		super_user.is_superuser = True
		super_user.save()

		non_priv_user = self.get_non_priv_user()

		user_api = UserApi(non_priv_user)

		""" Non priv user should not be able to delete any user. """
		with self.assertRaises(UserApi.UnauthorizedActionException):
			user_api.delete_user(admin_user)

		with self.assertRaises(UserApi.UnauthorizedActionException):
			user_api.delete_user(super_user)
			
		with self.assertRaises(UserApi.UnauthorizedActionException):
			user_api.delete_user(non_priv_user)