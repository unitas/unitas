#!/usr/bin/env bash

source unitas/config/serverenv.sh

echo "Stopping Solr ..."
for pid in $(ps -ef | grep solr | grep $solrPort | grep -v grep | awk '{print $2}'); 
	do kill $pid; 
done

echo "Stopping Django Dev Server ..."
for pid in $(ps -ef | grep runserver | grep $djangoDevServerPort | grep -v grep | awk '{print $2}'); 
	do kill $pid; 
done

if [ "$stopDbServer" == "true" ]
then
	$stopDbServerCmd
fi

echo "Done."