#!/usr/bin/env bash

opts=$1
find . -name '*.pyc' -delete
time ./manage.py test --failfast $opts