#!/usr/bin/env bash

# Port to run Django dev server
djangoDevServerPort="8000"

# Interface to run Django dev server
djangoDevServerInt="0.0.0.0"

# Run Solr true/false
runSolr="false"

# Solr port
solrPort="8983"

# Restart database on startup
restartDb="true"

# Run Django in daemon mode
daemonMode="false"

# Unitas log file
logFile="logs/unitas.log"

# Stop database on stopserver.sh
stopDbServer="false"

# Command to stop database server
stopDbServerCmd="sudo service postgresql stop"