"""Logging configuration file.
"""

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%d-%m-%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'file': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': 'logs/unitas.log',
            'formatter': 'verbose'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        }
    },
    'loggers': {
        'django': {
            'handlers':['file'],
            'propagate': True,
            'level':'WARNING',
        },
        'apps.api': {
            'handlers': ['file', 'console'],
            'level': 'INFO',
        },
        'apps.repository': {
            'handlers': ['file', 'console'],
            'level': 'INFO',
        },
        'apps.services': {
            'handlers': ['file', 'console'],
            'level': 'INFO',
        },
        'apps.web': {
            'handlers': ['file', 'console'],
            'level': 'INFO',
        },
        'scripts': {
            'handlers': ['file', 'console'],
            'level': 'WARNING',
        },
    }
}