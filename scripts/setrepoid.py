#!/usr/bin/env python

"""Script used to generate a uuid for the repository id.
"""
import django, os, sys, uuid

sys.path.append('.')
os.environ['DJANGO_SETTINGS_MODULE'] = 'unitas.settings'

django.setup()

from apps.repository.models import SystemSetting
import logging

logger = logging.getLogger(__name__)

setting = SystemSetting()
setting.key = 'repoId'
setting.value = uuid.uuid4()
setting.save()