#!/usr/bin/env python

import os, sys
sys.path.append('.')
os.environ['DJANGO_SETTINGS_MODULE'] = 'unitas.settings'

import django
django.setup()

from django.db.utils import IntegrityError

from apps.repository.models import MimeType
import logging

logger = logging.getLogger(__name__)


mimetype_file = 'scripts/data/mimetypes.csv'

class MimeTypeCreater(object):

	def run(self):

		fh = open(mimetype_file, 'r')
		lines = fh.readlines()
		fh.close()

		print 'Adding MimeTypes ...'
		for line in lines:
			name, designation, extension, description = line.split(':X:')

			#print name, designation, extension, description
			print '  Adding mimetype: {} [{}]'.format(
				name,
				extension,
			)
			mime_type = MimeType()
			mime_type.name = name
			mime_type.designation = designation
			mime_type.extension = extension
			mime_type.description = description
			mime_type.save()

		print 'Done adding mimetypes.'


if __name__ == '__main__':

	mime_type_creater = MimeTypeCreater()
	mime_type_creater.run()
