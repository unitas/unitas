#!/usr/bin/env python

import django, os, sys

sys.path.append('.')
os.environ['DJANGO_SETTINGS_MODULE'] = 'unitas.settings'

django.setup()

from apps.repository.models import ModelType
import logging

logger = logging.getLogger(__name__)

model_types = [
	{'name': 'folder'},
	{'name': 'document'},
]

class ModelTypeCreater(object):
	def run(self):
		for model_type in model_types:
			ModelType.objects.create(name=model_type['name'])


if __name__ == '__main__':
	model_type_creater = ModelTypeCreater()
	model_type_creater.run()