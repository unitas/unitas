#!/usr/bin/env python

import django, os, sys
os.environ['DJANGO_SETTINGS_MODULE'] = 'unitas.settings'
sys.path.append('.')
django.setup()

import logging

logger = logging.getLogger(__name__)

from django.contrib.auth.models import User
from apps.repository.models import UserProfile

system_users = [
	{
		'username': 'SYSTEM',
		'password': 'secret',
		'email': 'system@localhost',
		'is_active': False,
	},
	{
		'username': 'NOBODY',
		'password': 'secret',
		'email': 'nobody@localhost',
		'is_active': False,
	},
	{
		'username': 'EVERYBODY',
		'password': 'secret',
		'email': 'everybody@localhost',
		'is_active': False,
	},
	{
		'username': 'test_superuser',
		'password': 'secret',
		'email': 'test_superuser@localhost',
		'is_active': True,
		'is_superuser': True,
	},
	{
		'username': 'test_nonprivuser',
		'password': 'secret',
		'email': 'test_nonprivuser@localhost',
		'is_active': True,
	},
]

class UserCreater(object):

	def __init__(self):
		pass

	def run(self):
		for user in system_users:
			logger.info('Creating user: {}'.format(user['username']))
			
			# Create the user.
			u = User()
			u.username = user['username']
			u.email = user['email']
			u.set_password(user['password'])
			u.is_active = user['is_active']
			if user.has_key('is_superuser'):
				u.is_superuser = user['is_superuser']
			u.save()
			
			# Create the user profile for the user.
			up = UserProfile()
			up.user = u
			up.save()

		admin_user = User.objects.get(username='admin')
		user_profile = UserProfile()
		user_profile.user = admin_user
		user_profile.save()

if __name__ == '__main__':
	user_creater = UserCreater()
	user_creater.run()