#!/usr/bin/env bash

rm -rf pdfs/*

for each in $(ls Sample*); 
	do soffice --headless --convert-to pdf $each --outdir pdfs; 
done