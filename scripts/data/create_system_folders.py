#!/usr/bin/env python

import django, os, sys

sys.path.append('.')
os.environ['DJANGO_SETTINGS_MODULE'] = 'unitas.settings'

django.setup()

from django.contrib.auth.models import User
from django.db.models import Q
from apps.repository.models import Folder, ModelType
import logging

logger = logging.getLogger(__name__)

class SystemFolderCreater(object):

	def run(self):
		admin_user = User.objects.get(username='admin')
		system_user = User.objects.get(username='SYSTEM')
		everybody_user = User.objects.get(username='EVERYBODY')

		folder_model_type = ModelType.objects.get(name='folder')
		print('  Creating root folder ...')
		root_folder = Folder.objects.create(
			name='Repository', 
			title='Repository Root Folder', 
			description='The default repository root folder', 
			model_type=folder_model_type,
			owner=system_user,
			is_system=True,
		)

		system_folders = [
			{
				'name': 'Home',
				'title': 'Home Folder',
				'description': 'Home folder for Unitas users',
				'parent_folder': root_folder,
				'owner': system_user,
				'is_system': True,
			},
			{
				'name': 'Shared Documents',
				'title': 'Global shared documents',
				'description': 'Globally shared documents',
				'parent_folder': root_folder,
				'owner': everybody_user,
				'is_system': True,
			},
			{
				'name': 'Projects',
				'title': 'Projects Folder',
				'description': 'Unitas projects folder',
				'parent_folder': root_folder,
				'owner': system_user,
				'is_system': True,
			},
		]


		print('  Creating system folders ...')
		for folder in system_folders:
			Folder.objects.create(
				name=folder['name'], 
				title=folder['title'], 
				description=folder['description'],
				parent_folder=folder['parent_folder'],
				model_type=folder_model_type,
				owner=folder['owner'],
				is_system=folder['is_system'],
			)

		home_folder = Folder.objects.get(name='Home')

		user_list = User.objects.exclude(
			Q(username='SYSTEM')|
			Q(username='NOBODY')|
			Q(username='EVERYBODY')
		)

		print('  Creating user folders ...')
		for user in user_list:
			print ('  Creating user folder for {}'.format(user.username))
			user_home_folder = Folder.objects.create(
				name=user.username, 
				title=user.username, 
				description='Home folder for user: {}'.format(user.username),
				parent_folder=home_folder,
				model_type=folder_model_type,
				owner=user,
				is_system=True,
			)
			user_trashcan_folder = Folder.objects.create(
				name='Trashcan',
				title='{} user trashcan'.format(user.username),
				description='Trashcan folder for user: {}'.format(user.username),
				parent_folder=user_home_folder,
				model_type=folder_model_type,
				owner=user,
				is_system=True,
				is_hidden=False,
			)

if __name__ == '__main__':
	system_folder_creater = SystemFolderCreater()
	system_folder_creater.run()

