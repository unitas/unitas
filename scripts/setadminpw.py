#!/usr/bin/env python

"""Script to the admin user's password in bootstrap scripts.
"""

import django, os, sys

sys.path.append('.')
os.environ['DJANGO_SETTINGS_MODULE'] = 'unitas.settings'

django.setup()

from django.contrib.auth.models import User
import logging

logger = logging.getLogger(__name__)

admin_user = User.objects.get(username='admin')
admin_user.set_password('admin')
admin_user.save()