#!/usr/bin/env python

import os, sys

sys.path.append('.')
sys.path.append('../.')

os.environ['DJANGO_SETTINGS_MODULE'] = 'unitas.settings'

import django
django.setup()

from django.contrib.auth.models import User 
from django.contrib.auth import authenticate
from apps.repository.models import Document, Folder
from apps.api.lib.document import DocumentApi
from apps.repository.lib.service.document import DocumentService
from apps.repository.lib.service.folder import FolderService

def show_usage():

	print
	print('* upload.py - Unitas upload tool. \n* This will ingest all files and folders in the input_dir path and place them in unitas_folder.')
	print('Usage: ./upload.py [username] [password][input_dir] [unitas_folder]')
	print
	sys.exit(1)

def show_unauthorized():
	print('Not authorized to perform upload.')
	print('Exiting.')
	sys.exit(1)

def show_unitas_folder_not_exists():
	print('Unable to find Unitas folder: {}'.format(unitas_folder))
	print('Exiting.')
	sys.exit(1)

def show_input_dir_not_exists():
	print('Unable to find input directory specified: {}'.format(input_dir))
	print('Exiting')
	sys.exit(1)

try:
	username = sys.argv[1]
	password = sys.argv[2]
	input_dir = sys.argv[3]
	unitas_folder = sys.argv[4]
except IndexError:
	show_usage()

if not os.path.exists(input_dir):
	show_input_dir_not_exists()

unitas_folder_name = unitas_folder.split('/')[-1]
folder_list = Folder.objects.filter(name=unitas_folder.split('/')[-1])

root_folder = None
for folder in folder_list:
	path = folder.get_full_path(folder)
	if path == unitas_folder:
		root_folder = folder
		break

if not root_folder:
	show_unitas_folder_not_exists()

user = authenticate(username=username, password=password)

if not user:
	show_unauthorized()

folder_service = FolderService()
document_api = DocumentApi(user)

for root, dirs, files in os.walk(input_dir, topdown=False):
	for f in files:
		file_full_path = os.path.abspath(os.path.join(root, f))
		document_name = os.path.basename(file_full_path)
		parent_folder_name = os.path.basename(os.path.dirname(file_full_path))

		print 'Uploading: {}'.format(file_full_path)

		try:
			Document.objects.get(
				name=document_name, parent_folder=root_folder)
			print 'Document: {} already exists.'.format(document_name)
		except Document.DoesNotExist:
			document = document_api.create_document(
				document_name,
				root_folder,
				file_full_path,
				user,
			)
	